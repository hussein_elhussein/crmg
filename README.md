**Development:**
* **Running app directly from typescript:**
    1. run the following:<br>
    `node --inspect=9229 -r ./node_modules/ts-node/register src/main.ts`
* **seeding db:**
    1. run the following: <br>
    `node --inspect=9229 -r ./node_modules/ts-node/register src/lib/console sequelize db:seed`
<br>


**Deployment:** <br>
* **running migrations:** <br>
    1. run this command in project root: <br>
    `npm run migrate` <br>

* **run the app:**
    1. run `forever start built/main.js -l ./log/forever.log -o ./log/process.log -e ./log/process_err.log`
    2. make sure it's running: `forever list`
    
* **run the importer:**
    1. generate token for api access: `node built/lib/console auth --user=[USERNAME] --password=[PASSWORD]`
    2. run importers: `nohup node built/lib/console import >> temp/app.log 2>&1 &`
    3. check the report in `temp/importer_report.json`
    3. check the report in `temp/importer_report.json`
    

