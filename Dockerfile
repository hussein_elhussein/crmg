FROM node:10
# install sqlite:
RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y sqlite3 libsqlite3-dev

RUN mkdir /db

RUN /usr/bin/sqlite3 /db/test.db


# cache the app dependencies:
RUN npm i -g gulp
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app

COPY package*.json ./

USER node

RUN npm install

EXPOSE 5000 9229

CMD ["/bin/bash"]