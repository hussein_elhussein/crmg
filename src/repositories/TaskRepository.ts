import { BaseRepository } from '../lib/repositories/BaseRepository';
import { FindOptions, Promise as DBPromise } from 'sequelize';
import { injectable } from 'inversify';
import { IEntityValResult } from '../lib/repositories/interfaces/IEntityValResult';
import { IFieldError } from '../lib/repositories/interfaces/IEntityValError';
import { IResourceOptions } from '../lib/repositories/interfaces/IResourceOptions';
import { IRelationOption } from '../lib/repositories/interfaces/IPagOptions';
import Task from '../models/Task';

@injectable()
export class TaskRepository extends BaseRepository<Task>{

    /**
     * Find entity by ID or validate it if we are creating new one.
     *
     * @param id
     * @param attribute
     * @param entityRepo
     * @param withRelations
     */
    async findAndValidateEntity(id: string,
                                        attribute: string,
                                        entityRepo: BaseRepository<any>,
                                        withRelations?: boolean|null):Promise<IEntityValResult> {
        const idAttr = attribute + "Id";
        let entity = null;
        const result: IEntityValResult = {
            valid: false,
            value: null,
            attribute: attribute,
        };
        const notFoundErr: IFieldError = {
            status: 404,
            title: "Entity not found",
            detail: "Entity not found",
            source: {
                pointer: "data/attributes/id"
            },
            meta: {
                field: attribute,
            }
        };
        // if the body contains the entity id, find it:
        const find = async (id: any) => {
            let options:IResourceOptions|null = null;
            if(withRelations){
                const relations = this.getModelRelations(entityRepo);
                let include = <IRelationOption[]>[];
                if(relations.length){
                    for(let relation of relations){
                        let relOption: any;
                        if (relation.singular === 'User') {
                            relOption = { model: relation.model, as: relation.as};
                        } else relOption = { model: relation.model };
                        include.push(relOption);
                    }
                    options = { relations: include};
                }
            }
            let en = await entityRepo.findById(id, options).catch(e => {
                notFoundErr.detail = e.message;
                notFoundErr.title = e.message;
            });
            if (en) {
                result.valid = true;
            }
            return en;
        };
        // find the main entity if the id is specified:
        if(attribute === 'id'){
            notFoundErr.source.pointer = "data/id";
            entity = await find(id);
            if(entity){
                result.value = entity;
            }
        }
        // find a relation entity:
        else{
            result.attribute = idAttr;
            entity = await find(id);
        }

        if (!entity) {
            result.errors = [notFoundErr];
            return result;
        }
        result.value = entity;
        return result;
    }

    findById(identifier: string, options?: any): DBPromise<Task|null>{
        const op: FindOptions = {
            where: {
                id: identifier
            }
        };
        if(options && options.relations && options.relations.length){
            op.include = [];
            for(let relation of options.relations){
                if (relation.as) op.include.push(relation);
                else op.include.push(relation.model);
            }
        }
        return this.model.findOne(op);
    }
};