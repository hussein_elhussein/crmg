import { BaseRepository } from '../lib/repositories/BaseRepository';
import Person from '../models/Person';
import { injectable } from 'inversify';

@injectable()
export class PersonRepository extends BaseRepository<Person>{

}