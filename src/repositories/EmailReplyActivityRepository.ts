import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import EmailReplyActivity from '../models/EmailReplyActivity';

@injectable()
export class EmailReplyActivityRepository extends BaseRepository<EmailReplyActivity>{};