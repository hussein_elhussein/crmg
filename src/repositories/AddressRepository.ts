import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import Address from '../models/Address';

@injectable()
export class AddressRepository extends BaseRepository<Address>{}