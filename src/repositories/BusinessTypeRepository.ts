import { BaseRepository } from '../lib/repositories/BaseRepository';
import BusinessType from '../models/BusinessType';
import { injectable } from 'inversify';

@injectable()
export class BusinessTypeRepository extends BaseRepository<BusinessType>{

}