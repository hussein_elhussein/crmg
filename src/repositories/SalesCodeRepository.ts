import { BaseRepository } from '../lib/repositories/BaseRepository';
import SalesCode from '../models/SalesCode';
import { injectable } from 'inversify';

@injectable()
export class SalesCodeRepository extends BaseRepository<SalesCode>{}