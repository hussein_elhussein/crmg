import { BaseRepository } from '../lib/repositories/BaseRepository';
import LocationType from '../models/LocationType';
import { injectable } from 'inversify';

@injectable()
export class LocationTypeRepository extends BaseRepository<LocationType>{

}