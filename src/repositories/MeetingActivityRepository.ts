import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import MeetingActivity from '../models/MeetingActivity';

@injectable()
export class MeetingActivityRepository extends BaseRepository<MeetingActivity>{};