import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import PhoneCallActivity from '../models/PhoneCallActivity';

@injectable()
export class PhoneCallActivityRepository extends BaseRepository<PhoneCallActivity>{};