import { BaseRepository } from '../lib/repositories/BaseRepository';
import Company from '../models/Company';
import { injectable } from 'inversify';

@injectable()
export class CompanyRepository extends BaseRepository<Company>{

}