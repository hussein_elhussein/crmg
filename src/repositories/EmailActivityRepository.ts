import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import EmailActivity from '../models/EmailActivity';

@injectable()
export class EmailActivityRepository extends BaseRepository<EmailActivity>{};