import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import EventsActivity from '../models/EventsActivity';

@injectable()
export class EventsActivityRepository extends BaseRepository<EventsActivity>{};