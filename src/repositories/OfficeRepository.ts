import { BaseRepository } from '../lib/repositories/BaseRepository';
import OfficeCode from '../models/OfficeCode';
import { injectable } from 'inversify';

@injectable()
export class OfficeRepository extends BaseRepository<OfficeCode>{}