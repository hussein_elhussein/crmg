import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import Role from '../models/Role';

@injectable()
export class RoleRepository extends BaseRepository<Role>{}