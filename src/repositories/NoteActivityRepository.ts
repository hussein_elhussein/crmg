import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import NoteActivity from '../models/NoteActivity';

@injectable()
export class NoteActivityRepository extends BaseRepository<NoteActivity>{};