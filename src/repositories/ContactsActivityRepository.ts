import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import ContactsActivity from '../models/ContactsActivity';

@injectable()
export class ContactsActivityRepository extends BaseRepository<ContactsActivity>{};