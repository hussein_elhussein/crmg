import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import DeviceActivity from '../models/DeviceActivity';

@injectable()
export class DeviceActivityRepository extends BaseRepository<DeviceActivity>{};