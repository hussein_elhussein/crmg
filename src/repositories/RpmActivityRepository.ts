import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import RpmActivity from '../models/RpmActivity';

@injectable()
export class RpmActivityRepository extends BaseRepository<RpmActivity>{};