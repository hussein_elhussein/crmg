import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import Account from '../models/Account';

@injectable()
export class AccountRepository extends BaseRepository<Account>{}