import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import List from '../models/List';
import { Promise as DBPromise } from 'sequelize';

@injectable()
export class ListRepository extends BaseRepository<List>{

  public mailChimpLists():DBPromise<List[]>{
    return this.findAll({where: {listType: "mailchimp"}});
  }

  public mapLists():DBPromise<List[]>{
    return this.findAll({where: {listType: "map"}});
  }

  public duplicates():DBPromise<List[]>{
    return this.findAll({where: {listType: "duplicates"}});
  }

  public general():DBPromise<List[]>{
    return this.findAll({where: {listType: "general"}});
  }
}