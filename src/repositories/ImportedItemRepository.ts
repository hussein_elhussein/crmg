import { injectable } from 'inversify';
import ImportedItem from '../models/ImportedItem';
import { BaseRepository } from '../lib/repositories/BaseRepository';

@injectable()
export class ImportedItemRepository extends BaseRepository<ImportedItem>{}