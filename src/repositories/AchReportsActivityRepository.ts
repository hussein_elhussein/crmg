import { BaseRepository } from '../lib/repositories/BaseRepository';
import { injectable } from 'inversify';
import AchReportsActivity from '../models/AchReportsActivity';

@injectable()
export class AchReportsActivityRepository extends BaseRepository<AchReportsActivity>{};