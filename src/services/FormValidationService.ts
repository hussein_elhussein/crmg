import { Model } from 'sequelize-typescript';
import { IFieldValidationErr } from '../interfaces/IFieldValidationErr';
import { ValidationError } from 'sequelize';
import { injectable } from 'inversify';

@injectable()
export class FormValidationService {
    public async validate(model: Model):Promise<IFieldValidationErr[] | null>{
        let valErrors = <ValidationError>{};
        await model.validate().catch(err => valErrors = err);
        const errors: IFieldValidationErr[] = [];
        if(valErrors && valErrors.errors && valErrors.errors){
            for(let error of valErrors.errors){
                const err: IFieldValidationErr = {
                    field: error.path,
                    message: error.message,
                };
                errors.push(err);
            }
            return errors;
        }
        return null;
    }
}