import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import Person from '../models/Person';
@injectable()
export class PersonResource extends BaseResource<Person>{}