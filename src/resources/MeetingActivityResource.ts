import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import MeetingActivity from '../models/MeetingActivity';

@injectable()
export class MeetingActivityResource extends BaseResource<MeetingActivity>{};