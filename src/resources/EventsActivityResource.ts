import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import EventsActivity from '../models/EventsActivity';

@injectable()
export class EventsActivityResource extends BaseResource<EventsActivity>{};