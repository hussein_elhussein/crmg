import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import BusinessType from '../models/BusinessType';
@injectable()
export class BusinessTypeResource extends BaseResource<BusinessType>{}