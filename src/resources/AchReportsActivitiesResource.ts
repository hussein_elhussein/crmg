import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import AchReportsActivity from '../models/AchReportsActivity';

@injectable()
export class AchReportsActivityResource extends BaseResource<AchReportsActivity>{};