import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import Address from '../models/Address';

@injectable()
export class AddressResource extends BaseResource<Address>{}