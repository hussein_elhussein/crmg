import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import Account from '../models/Account';

@injectable()
export class AccountResource extends BaseResource<Account>{}