import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import Task from '../models/Task';

@injectable()
export class TaskActivityResource extends BaseResource<Task>{}