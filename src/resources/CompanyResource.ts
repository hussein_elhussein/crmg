import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import Company from '../models/Company';
@injectable()
export class CompanyResource extends BaseResource<Company>{}