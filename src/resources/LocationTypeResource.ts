import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import LocationType from '../models/LocationType';
@injectable()
export class LocationTypeResource extends BaseResource<LocationType>{}