import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import NoteActivity from '../models/NoteActivity';

@injectable()
export class NoteActivityResource extends BaseResource<NoteActivity>{};