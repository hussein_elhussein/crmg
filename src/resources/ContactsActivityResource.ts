import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import ContactsActivity from '../models/ContactsActivity';

@injectable()
export class ContactsActivityResource extends BaseResource<ContactsActivity>{};