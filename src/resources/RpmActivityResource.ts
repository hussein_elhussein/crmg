import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import RpmActivity from '../models/RpmActivity';

@injectable()
export class RpmActivityResource extends BaseResource<RpmActivity>{};