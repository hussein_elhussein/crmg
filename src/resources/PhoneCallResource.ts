import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import PhoneCallActivity from '../models/PhoneCallActivity';

@injectable()
export class PhoneCallActivityResource extends BaseResource<PhoneCallActivity>{};