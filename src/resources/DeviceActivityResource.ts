import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import DeviceActivity from '../models/DeviceActivity';

@injectable()
export class DeviceActivityResource extends BaseResource<DeviceActivity>{};