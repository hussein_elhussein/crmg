import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import EmailActivity from '../models/EmailActivity';

@injectable()
export class EmailActivityResource extends BaseResource<EmailActivity>{};