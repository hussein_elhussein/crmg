import { BaseResource } from '../lib/resources/BaseResource';
import { injectable } from 'inversify';
import EmailReplyActivity from '../models/EmailReplyActivity';

@injectable()
export class EmailReplyActivityResource extends BaseResource<EmailReplyActivity>{};