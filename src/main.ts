import { Kernel } from './lib/kernel/kernel';
import { DatabaseService } from './lib/services/DatabaseService';

(async () => {
    const kernel = new Kernel();
    const app = await kernel.boot();
    if (app) {
        /** Connect to Database */
        const db = kernel.getContainer().get(DatabaseService);
        db.init();
        const connection = await db.connect().catch(console.log);
        if (!connection) {
            throw new Error("Failed to connect to database");
        }

        const PORT = process.env.PORT || 5000;
        app.listen(PORT, () => console.log(`API listening on port ${PORT}`));
    }
    else {
        console.log('Failed to start api');
    }

})();
