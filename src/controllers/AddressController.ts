import { inject, injectable, named } from 'inversify';
import { Response,Request } from 'express';
import { BaseResourceController } from '../lib/controller/BaseResourceController';
import { ErrorHandlerService } from '../lib/services/ErrorHandlerService';
import { QueryParser } from '../lib/router/QueryParser';
import { BaseRepository } from '../lib/repositories/BaseRepository';
import Company from '../models/Company';
import Person from '../models/Person';

@injectable()
export class AddressController extends BaseResourceController{
    companyRepo: BaseRepository<Company>;
    personRepo: BaseRepository<Person>;
    constructor(@inject(ErrorHandlerService) errorHandler: ErrorHandlerService,
                @inject(QueryParser) queryParser: QueryParser,
                @inject('Repository') @named('Company') companyRepo: BaseRepository<Company>,
                @inject('Repository') @named('Person') personRepo: BaseRepository<Person>){
        super(errorHandler, queryParser);
        this.allowedRelations = ['*all*'];
        this.companyRepo = companyRepo;
        this.personRepo = personRepo;
    }

    async create(req: Request, res: Response): Promise<any>{
        let errors = null;
        if (req.body.data.relationships) {
          if (req.body.data.relationships.company) {
            req.body.data.attributes.addressableId = req.body.data.relationships.company.data.id;
            req.body.data.attributes.addressableType = 'Company';
          } else if (req.body.data.relationships.person ) {
            req.body.data.attributes.addressableId = req.body.data.relationships.person.data.id;
            req.body.data.attributes.addressableType = 'Person';
          }
        }
        let model = await this.repository.entityFromRequest(req)
            .catch(e => errors = e);
        if(errors){
            return this.sendError(errors,422);
        }
        // Save the model instance:
        const result = await this.repository.save(model, {validate: false}).catch((e: any) => errors = e);
        if(errors){
            return this.sendError(errors);
        }
        if(!result){
            const err = {
                message: 'Failed to save resource',
            };
            return this.sendError(err);
        }
        const resource = this.resourceHandler.fromModel(result);
        return this.resource(resource).send(201);
    }

    async edit(req: Request, res: Response): Promise<any>{
      let errors:any = null;
      if (!req.body.data.id) {
        req.body.data.id = req.params.id;
      }
      if (req.body.data.relationships) {
        if (req.body.data.relationships.company) {
          req.body.data.attributes.addressableId = req.body.data.relationships.company.data.id;
          req.body.data.attributes.addressableType = 'Company';
        } else if (req.body.data.relationships.person ) {
          req.body.data.attributes.addressableId = req.body.data.relationships.person.data.id;
          req.body.data.attributes.addressableType = 'Person';
        }
      }
      const model = await this.repository.entityFromRequest(req, true)
          .catch(e => errors = e);
      if(errors){
          return this.sendError(errors,422);
      }

      // update the model instance:
      const result = await this.repository.save(model,{validate: false}).catch((e:any) => errors = e);
      if(errors){
          return this.sendError(errors);
      }
      if(errors){
        return this.sendError(errors);
      }
      if(!result){
          const err = {
              message: 'Failed to edit resource',
          };
          return this.sendError(err);
      }
      const resource = this.resourceHandler.fromModel(result);
      return this.resource(resource).send(201);
  }
}
