import { inject, injectable, named } from 'inversify';
import { Response,Request } from 'express';
import { BaseResourceController } from '../lib/controller/BaseResourceController';
import { IRelationIdB } from '../lib/serializer/interfaces/IDeSerialized';
import { ErrorHandlerService } from '../lib/services/ErrorHandlerService';
import { QueryParser } from '../lib/router/QueryParser';
import { BaseRepository } from '../lib/repositories/BaseRepository';;
import BusinessType from '../models/BusinessType';
import Person from '../models/Person';
import Company from '../models/Company';
import LocationType from '../models/LocationType';

@injectable()
export class CompanyController extends BaseResourceController{
    locationTypeRepo: BaseRepository<LocationType>;
    businessTypeRepo: BaseRepository<BusinessType>;
    companyRepo: BaseRepository<Company>;
    personRepo: BaseRepository<Person>;
    constructor(@inject(ErrorHandlerService) errorHandler: ErrorHandlerService,
                @inject(QueryParser) queryParser: QueryParser,
                @inject('Repository') @named('LocationType') locationTypeRepo: BaseRepository<LocationType>,
                @inject('Repository') @named('BusinessType') businessTypeRepo: BaseRepository<BusinessType>,
                @inject('Repository') @named('Company') companyRepo: BaseRepository<Company>,
                @inject('Repository') @named('Person') personRepo: BaseRepository<Person>){
        super(errorHandler, queryParser);
        this.allowedRelations = ['*all*'];
        this.locationTypeRepo = locationTypeRepo;
        this.businessTypeRepo = businessTypeRepo;
        this.companyRepo = companyRepo;
        this.personRepo = personRepo;
    }

    async create(req: Request, res: Response): Promise<any>{
        let errors = null;
        let model = await this.repository.entityFromRequest(req)
            .catch(e => errors = e);
        if(errors){
            return this.sendError(errors,422);
        }
        // Find related entities and set relations:
        if (req.body.data.relationships) {
            if (req.body.data.relationships.business_type) { 
              const relInfo = <IRelationIdB> req.body.data.relationships.business_type.data;
              const businessType = await this.businessTypeRepo.findOne({where: {id: relInfo.id}});
              if (businessType) model.$set('businessType', businessType);
            }
            if (req.body.data.relationships.location_type) {
              const relInfo = <IRelationIdB> req.body.data.relationships.location_type.data;
              const locationType = await this.locationTypeRepo.findOne({where: {id: relInfo.id}});
              if (locationType) model.$set('locationType', locationType);
            }
            if (req.body.data.relationships.primary_contact) {
              const relInfo = <IRelationIdB> req.body.data.relationships.primary_contact.data;
              const primaryContact = await this.personRepo.findOne({where: {id: relInfo.id}});
              if (primaryContact) model.$set('primaryContact', primaryContact);
            }
            if (req.body.data.relationships.parent_company) {
              const relInfo = <IRelationIdB> req.body.data.relationships.parent_company.data;
              const parentCompany = await this.companyRepo.findOne({where: {id: relInfo.id}});
              if (parentCompany) model.$set('parentCompany', parentCompany);
            }
        }
        // Save the model instance:
        const result = await this.repository.save(model, {validate: false}).catch((e: any) => errors = e);
        if(errors){
            return this.sendError(errors);
        }
        if(!result){
            const err = {
                message: 'Failed to save resource',
            };
            return this.sendError(err);
        }
        const resource = this.resourceHandler.fromModel(result);
        return this.resource(resource).send(201);
    }

    async edit(req: Request, res: Response): Promise<any>{
      let errors:any = null;
      if (!req.body.data.id) {
        req.body.data.id = req.params.id;
      }
      const model = await this.repository.entityFromRequest(req, true)
          .catch(e => errors = e);
      if(errors){
          return this.sendError(errors,422);
      }

      // Check if relations need to be changed:
      if (req.body.data.relationships) {
        if (req.body.data.relationships.business_type) { 
          const relInfo = <IRelationIdB> req.body.data.relationships.business_type.data;
          const businessType = await this.businessTypeRepo.findOne({where: {id: relInfo.id}});
          if (businessType) model.$set('businessType', businessType);
        }
        if (req.body.data.relationships.location_type) {
          const relInfo = <IRelationIdB> req.body.data.relationships.location_type.data;
          const locationType = await this.locationTypeRepo.findOne({where: {id: relInfo.id}});
          if (locationType) model.$set('locationType', locationType);
        }
        if (req.body.data.relationships.primary_contact) {
          const relInfo = <IRelationIdB> req.body.data.relationships.primary_contact.data;
          const primaryContact = await this.personRepo.findOne({where: {id: relInfo.id}});
          if (primaryContact) model.$set('primaryContact', primaryContact);
        }
        if (req.body.data.relationships.parent_company) {
          const relInfo = <IRelationIdB> req.body.data.relationships.parent_company.data;
          const parentCompany = await this.companyRepo.findOne({where: {id: relInfo.id}});
          if (parentCompany) model.$set('parentCompany', parentCompany);
        }
    }

      // update the model instance:
      const result = await this.repository.save(model,{validate: false}).catch((e:any) => errors = e);
      if(errors){
          return this.sendError(errors);
      }
      if(errors){
        return this.sendError(errors);
      }
      if(!result){
          const err = {
              message: 'Failed to edit resource',
          };
          return this.sendError(err);
      }
      const resource = this.resourceHandler.fromModel(result);
      return this.resource(resource).send(201);
  }
}