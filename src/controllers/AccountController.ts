import { inject, injectable, named } from 'inversify';
import { Response,Request } from 'express';
import { BaseResourceController } from '../lib/controller/BaseResourceController';
import { IRelationIdB } from '../lib/serializer/interfaces/IDeSerialized';
import { ErrorHandlerService } from '../lib/services/ErrorHandlerService';
import { QueryParser } from '../lib/router/QueryParser';
import { BaseRepository } from '../lib/repositories/BaseRepository';
import OfficeCode from '../models/OfficeCode';
import SalesCode from '../models/SalesCode';
import Company from '../models/Company';
import Person from '../models/Person';

@injectable()
export class AccountController extends BaseResourceController{
    officeRepo: BaseRepository<OfficeCode>;
    salesCodeRepo: BaseRepository<SalesCode>;
    companyRepo: BaseRepository<Company>;
    personRepo: BaseRepository<Person>;
    constructor(@inject(ErrorHandlerService) errorHandler: ErrorHandlerService,
                @inject(QueryParser) queryParser: QueryParser,
                @inject('Repository') @named('Office') officeRepo: BaseRepository<OfficeCode>,
                @inject('Repository') @named('SalesCode') salesCodeRepo: BaseRepository<SalesCode>,
                @inject('Repository') @named('Company') companyRepo: BaseRepository<Company>,
                @inject('Repository') @named('Person') personRepo: BaseRepository<Person>){
        super(errorHandler, queryParser);
        this.allowedRelations = ['*all*'];
        this.officeRepo = officeRepo;
        this.salesCodeRepo = salesCodeRepo;
        this.companyRepo = companyRepo;
        this.personRepo = personRepo;
    }

    async create(req: Request, res: Response): Promise<any>{
        let errors = null;
        if (req.body.data.relationships) {
          if (req.body.data.relationships.company) {
            req.body.data.attributes.accountableId = req.body.data.relationships.company.data.id;
            req.body.data.attributes.accountableType = 'Company';
          } else if (req.body.data.relationships.person ) {
            req.body.data.attributes.accountableId = req.body.data.relationships.person.data.id;
            req.body.data.attributes.accountableType = 'Person';
          }
        }
        let model = await this.repository.entityFromRequest(req)
            .catch(e => errors = e);
        if(errors){
            return this.sendError(errors,422);
        }
        // Find related entities and set relations:
        if (req.body.data.relationships) {
            if (req.body.data.relationships.office) { 
              const relInfo = <IRelationIdB> req.body.data.relationships.office.data;
              const office = await this.officeRepo.findOne({where: {id: relInfo.id}});
              if (office) model.$set('office', office);
            }
            if (req.body.data.relationships.sales_code) {
              const relInfo = <IRelationIdB> req.body.data.relationships.sales_code.data;
              const salesCode = await this.salesCodeRepo.findOne({where: {id: relInfo.id}});
              if (salesCode) model.$set('salesCode', salesCode);
            }

        }
        // Save the model instance:
        const result = await this.repository.save(model, {validate: false}).catch((e: any) => errors = e);
        if(errors){
            return this.sendError(errors);
        }
        if(!result){
            const err = {
                message: 'Failed to save resource',
            };
            return this.sendError(err);
        }
        const resource = this.resourceHandler.fromModel(result);
        return this.resource(resource).send(201);
    }

    async edit(req: Request, res: Response): Promise<any>{
      let errors:any = null;
      if (!req.body.data.id) {
        req.body.data.id = req.params.id;
      }
      if (req.body.data.relationships) {
        if (req.body.data.relationships.company) {
          req.body.data.attributes.accountableId = req.body.data.relationships.company.data.id;
          req.body.data.attributes.accountableType = 'Company';
        } else if (req.body.data.relationships.person ) {
          req.body.data.attributes.accountableId = req.body.data.relationships.person.data.id;
          req.body.data.attributes.accountableType = 'Person';
        }
      }
      const model = await this.repository.entityFromRequest(req, true)
          .catch(e => errors = e);
      if(errors){
          return this.sendError(errors,422);
      }

      // Check if relations need to be changed:
      if (req.body.data.relationships) {
        if (req.body.data.relationships.office) { 
          const relInfo = <IRelationIdB> req.body.data.relationships.office.data;
          const office = await this.officeRepo.findOne({where: {id: relInfo.id}});
          if (office) model.$set('office', office);
        }
        if (req.body.data.relationships.sales_code) {
          const relInfo = <IRelationIdB> req.body.data.relationships.sales_code.data;
          const salesCode = await this.salesCodeRepo.findOne({where: {id: relInfo.id}});
          if (salesCode) model.$set('salesCode', salesCode);
        }
    }

      // update the model instance:
      const result = await this.repository.save(model,{validate: false}).catch((e:any) => errors = e);
      if(errors){
          return this.sendError(errors);
      }
      if(errors){
        return this.sendError(errors);
      }
      if(!result){
          const err = {
              message: 'Failed to edit resource',
          };
          return this.sendError(err);
      }
      const resource = this.resourceHandler.fromModel(result);
      return this.resource(resource).send(201);
  }
}