import { BaseResourceController } from '../lib/controller/BaseResourceController';
import { injectable } from 'inversify';

@injectable()
export class LocationTypeController extends BaseResourceController{}