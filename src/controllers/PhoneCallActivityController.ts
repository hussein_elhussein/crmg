import { injectable } from 'inversify';
import { BaseResourceController } from '../lib/controller/BaseResourceController';

@injectable()
export class PhoneCallActivityController extends BaseResourceController{};