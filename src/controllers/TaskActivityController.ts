import { inject, injectable, named } from 'inversify';
import { Response,Request } from 'express';
import { BaseResourceController } from '../lib/controller/BaseResourceController';
import { ErrorHandlerService } from '../lib/services/ErrorHandlerService';
import { QueryParser } from '../lib/router/QueryParser';
import { BaseRepository } from '../lib/repositories/BaseRepository';
import { IRelationIdB } from '../lib/serializer/interfaces/IDeSerialized';
import User from '../models/User';
import Company from '../models/Company';
import Person from '../models/Person';
@injectable()
export class TaskActivityController extends BaseResourceController{
    userRepo: BaseRepository<User>;
    companyRepo: BaseRepository<Company>;
    personRepo: BaseRepository<Person>;
    constructor(@inject(ErrorHandlerService) errorHandler: ErrorHandlerService,
                @inject(QueryParser) queryParser: QueryParser,
                @inject('Repository') @named('User') userRepo: BaseRepository<User>,
                @inject('Repository') @named('Company') companyRepo: BaseRepository<Company>,
                @inject('Repository') @named('Person') personRepo: BaseRepository<Person>){
        super(errorHandler, queryParser);
        this.allowedRelations = ['*all*'];
        this.userRepo = userRepo;
        this.companyRepo = companyRepo;
        this.personRepo = personRepo;
    }

    preProcessRequest(request: Request): Request {
        const req = request;
        if (req.body.data.attributes.userId || req.body.data.attributes.assignedTo) {
                const reqAttributes = req.body.data.attributes;
                const { userId, assignedTo, ...attributes} = reqAttributes;
                req.body.data.attributes = attributes;
                const user = {
                    data: {
                        id: userId,
                        type: "users",
                    }
                }
                const assignee = {
                    data: {
                        id: assignedTo,
                        type: "users",
                    }
                }
                if (!req.body.data.relationships) {
                    if (userId) req.body.data.relationships = { user };
                    if (assignedTo) {
                        if (!req.body.data.relationships) req.body.data.relationships = { assignee };
                        else req.body.data.relationships.assignee = { ...assignee };
                    }
                } else {
                    if (userId) req.body.data.relationships.user = { ...user };
                    if (assignedTo) req.body.data.relationships.assignee = { ...assignee };
                }
            }
        return req;
    }

    async create(req: Request, res: Response): Promise<any>{
        const request = this.preProcessRequest(req);
        let errors = null;
        let model = await this.repository.entityFromRequest(request)
            .catch(e => errors = e);
        if(errors){
            return this.sendError(errors,422);
        }
        // Find related entities and set relations:
        if (request.body.data.relationships.user) {
            const relInfo = <IRelationIdB> req.body.data.relationships.user.data;
            const user = await this.userRepo.findOne({where: {id: relInfo.id}}).catch(e => errors = e);
            if (user) model.$set('user', user);
        }
        if (request.body.data.relationships.assignee) {
            const relInfo = <IRelationIdB> req.body.data.relationships.assignee.data;
            const assignee = await this.userRepo.findOne({ where: { id: relInfo.id }}).catch(e => errors = e);
            if (assignee) model.$set('assignee', assignee);
        }
        // Save the model instance:
        const result = await this.repository.save(model, {validate: false}).catch((e: any) => errors = e);
        if(errors){
            return this.sendError(errors);
        }
        if(!result){
            const err = {
                message: 'Failed to save resource',
            };
            return this.sendError(err);
        }
        const resource = this.resourceHandler.fromModel(result);
        return this.resource(resource).send(201);
    }

    async edit(req: Request, res: Response): Promise<any>{
        const request = this.preProcessRequest(req);  
        if (!request.body.data.id) {
            request.body.data.id = req.params.id;
        }
        let errors = null;
        const model = await this.repository.entityFromRequest(request, true)
            .catch(e => errors = e);
        if(errors){
            return this.sendError(errors,422);
        }

        // Check if relations need to be changed:
        if (request.body.data.relationships.user) {
            const relInfo = <IRelationIdB> req.body.data.relationships.user.data;
            const user = await this.userRepo.findOne({where: {id: relInfo.id}}).catch(e => errors = e);
            if (user) model.$set('user', user);
        }
        if (request.body.data.relationships.assignee) {
            const relInfo = <IRelationIdB> req.body.data.relationships.assignee.data;
            const assignee = await this.userRepo.findOne({ where: { id: relInfo.id }}).catch(e => errors = e);
            if (assignee) model.$set('assignee', assignee);
        }

        // Update the model instance:
        const result = await this.repository.save(model,{validate: false}).catch((e:any) => errors = e);
        if(errors){
            return this.sendError(errors);
        }
        if(errors){
            return this.sendError(errors);
        }
        if(!result){
            const err = {
                message: 'Failed to edit resource',
            };
            return this.sendError(err);
        }
        const resource = this.resourceHandler.fromModel(result);
        return this.resource(resource).send(201);
  }
}
