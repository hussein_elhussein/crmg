import { inject, injectable, named } from 'inversify';
import { Response,Request } from 'express';
import { BaseResourceController } from '../lib/controller/BaseResourceController';
import { IRelationIdB } from '../lib/serializer/interfaces/IDeSerialized';
import { ErrorHandlerService } from '../lib/services/ErrorHandlerService';
import { QueryParser } from '../lib/router/QueryParser';
import { BaseRepository } from '../lib/repositories/BaseRepository';;
import BusinessType from '../models/BusinessType';
import SaleStatus from '../models/SaleStatus';
import PersonType from '../models/PersonType';
import User from '../models/User';

@injectable()
export class PersonController extends BaseResourceController{
    personTypeRepo: BaseRepository<PersonType>;
    businessTypeRepo: BaseRepository<BusinessType>;
    saleStatusRepo: BaseRepository<SaleStatus>;
    userRepo: BaseRepository<User>;
    constructor(@inject(ErrorHandlerService) errorHandler: ErrorHandlerService,
                @inject(QueryParser) queryParser: QueryParser,
                @inject('Repository') @named('PersonType') personTypeRepo: BaseRepository<PersonType>,
                @inject('Repository') @named('BusinessType') businessTypeRepo: BaseRepository<BusinessType>,
                @inject('Repository') @named('SaleStatus') saleStatusRepo: BaseRepository<SaleStatus>,
                @inject('Repository') @named('User') userRepo: BaseRepository<User>){
        super(errorHandler, queryParser);
        this.allowedRelations = ['*all*'];
        this.personTypeRepo = personTypeRepo;
        this.businessTypeRepo = businessTypeRepo;
        this.saleStatusRepo = saleStatusRepo;
        this.userRepo = userRepo;
    }

    async create(req: Request, res: Response): Promise<any>{
        let errors = null;
        let model = await this.repository.entityFromRequest(req)
            .catch(e => errors = e);
        if(errors){
            return this.sendError(errors,422);
        }
        // Find related entities and set relations:
        if (req.body.data.relationships) {
            if (req.body.data.relationships.business_type) { 
              const relInfo = <IRelationIdB> req.body.data.relationships.business_type.data;
              const businessType = await this.businessTypeRepo.findOne({where: {id: relInfo.id}});
              if (businessType) model.$set('businessType', businessType);
            }
            if (req.body.data.relationships.person_type) {
              const relInfo = <IRelationIdB> req.body.data.relationships.person_type.data;
              const personType = await this.personTypeRepo.findOne({where: {id: relInfo.id}});
              if (personType) model.$set('personType', personType);
            }
            if (req.body.data.relationships.sale_status) {
              const relInfo = <IRelationIdB> req.body.data.relationships.sale_status.data;
              const saleStatus = await this.saleStatusRepo.findOne({where: {id: relInfo.id}});
              if (saleStatus) model.$set('saleStatus', saleStatus);
            }
            if (req.body.data.relationships.owner) {
              const relInfo = <IRelationIdB> req.body.data.relationships.owner.data;
              const owner = await this.userRepo.findOne({where: {id: relInfo.id}});
              if (owner) model.$set('owner', owner);
            }
        }
        // Save the model instance:
        const result = await this.repository.save(model, {validate: false}).catch((e: any) => errors = e);
        if(errors){
            return this.sendError(errors);
        }
        if(!result){
            const err = {
                message: 'Failed to save resource',
            };
            return this.sendError(err);
        }
        const resource = this.resourceHandler.fromModel(result);
        return this.resource(resource).send(201);
    }

    async edit(req: Request, res: Response): Promise<any>{
      let errors:any = null;
      if (!req.body.data.id) {
        req.body.data.id = req.params.id;
      }
      const model = await this.repository.entityFromRequest(req, true)
          .catch(e => errors = e);
      if(errors){
          return this.sendError(errors,422);
      }

      // Check if relations need to be changed:
      if (req.body.data.relationships) {
        if (req.body.data.relationships.business_type) { 
          const relInfo = <IRelationIdB> req.body.data.relationships.business_type.data;
          const businessType = await this.businessTypeRepo.findOne({where: {id: relInfo.id}});
          if (businessType) model.$set('businessType', businessType);
        }
        if (req.body.data.relationships.person_type) {
          const relInfo = <IRelationIdB> req.body.data.relationships.person_type.data;
          const personType = await this.personTypeRepo.findOne({where: {id: relInfo.id}});
          if (personType) model.$set('personType', personType);
        }
        if (req.body.data.relationships.sale_status) {
          const relInfo = <IRelationIdB> req.body.data.relationships.sale_status.data;
          const saleStatus = await this.saleStatusRepo.findOne({where: {id: relInfo.id}});
          if (saleStatus) model.$set('saleStatus', saleStatus);
        }
        if (req.body.data.relationships.owner) {
          const relInfo = <IRelationIdB> req.body.data.relationships.owner.data;
          const owner = await this.userRepo.findOne({where: {id: relInfo.id}});
          if (owner) model.$set('owner', owner);
        }
    }

      // update the model instance:
      const result = await this.repository.save(model,{validate: false}).catch((e:any) => errors = e);
      if(errors){
          return this.sendError(errors);
      }
      if(errors){
        return this.sendError(errors);
      }
      if(!result){
          const err = {
              message: 'Failed to edit resource',
          };
          return this.sendError(err);
      }
      const resource = this.resourceHandler.fromModel(result);
      return this.resource(resource).send(201);
  }
}