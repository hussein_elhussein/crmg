import { config } from 'dotenv';
import { resolve } from "path";
import {AuthService} from '../services/AuthService';
import { inject, injectable} from 'inversify';
import { Request, Response } from 'express';
import { IToken } from '../interfaces/IToken';
import User from '../models/User';
import { IHttpError } from '../interfaces/IHttpError';
import { FormValidationService } from '../services/FormValidationService';
import { BaseController } from '../lib/controller/BaseController';
import { ErrorHandlerService } from '../lib/services/ErrorHandlerService';
import { QueryParser } from '../lib/router/QueryParser';
config({ path: resolve(__dirname, "../.env") });

@injectable()
export class AuthController extends BaseController{
    authSer: AuthService;
    formValService: FormValidationService;
    constructor(@inject(ErrorHandlerService) errorHandler: ErrorHandlerService,
                @inject(QueryParser) queryParser: QueryParser,
                @inject(AuthService) authSer: AuthService,
                @inject(FormValidationService) formValService: FormValidationService){
        super(errorHandler, queryParser);
        this.authSer = authSer;
        this.formValService = formValService;
    }
    async login(req: any,res: any){
        const username = req.body.username;
        const password = req.body.password;
        const token = <IToken> await this.authSer.login(username,password);
        if(token){
            return this.sendResponse(token);
        }
        return this.sendResponse({error: "Invalid username or password"},403);
    }

    async register(req: Request, res: Response){
        let user = new User();
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.email = req.body.email;
        user.password = req.body.password;
        const errors = await this.formValService.validate(user);
        if(errors){
            return res.status(422).json({errors});
        }
        const password = req.body.password;
        user.password = password;
        user = <User> await user.save().catch(err => this.errorHandler.handle(err));
        if(!user){
            const err: IHttpError = {
                message: "Couldn't save user"
            };
            return res.status(500).json(err);
        }
        const token = <IToken> await this.authSer.login(user.email, password);
        if(token){
            return res.json(token);
        }
        const err: IHttpError = {
            message: "Couldn't log you in"
        };
        return res.status(500).json(err);
    }
}