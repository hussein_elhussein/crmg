import { Table, Column, DataType, TableOptions, ForeignKey, IsNumeric, BelongsToMany } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required } from '../lib/validation';

const options: TableOptions = {
    tableName: "commodity_types",
    modelName:"CommodityType",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class CommodityType extends BaseModel<CommodityType>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
