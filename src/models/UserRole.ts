import { Table, Column, DataType, TableOptions,  ForeignKey } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';
import User from './User';
import Role from './Role';

const options: TableOptions = {
    tableName: "user_role",
    modelName:"UserRole",
    timestamps: false,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class UserRole extends BaseModel<UserRole>{

    @Required()
    @ForeignKey(() => User)
    @Column
    userId: string;

    @Required()
    @ForeignKey(() => Role)
    @Column
    roleId: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
