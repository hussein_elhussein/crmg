import { Table, Column, DataType, BelongsTo, TableOptions, ForeignKey } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Person from './Person';
import Company from './Company';
const options: TableOptions = {
    tableName: "employees",
    modelName:"Employee",
    timestamps: false,
    paranoid: true,
    underscored: true,
};

@Table(options)
export default class Employee extends BaseModel<Employee>{
    
    @Required()
    @ForeignKey(() => Person)
    @IsString()
    @Column(DataType.UUID)
    public personId: string;

    @BelongsTo(() => Person)
    person: Person;

    @Required()
    @ForeignKey(() => Company)
    @IsString()
    @Column(DataType.UUID)
    public companyId: string;

    @BelongsTo(() => Company)
    company: Company;

    @Column(DataType.DATE)
    public deletedAt: string;
}
