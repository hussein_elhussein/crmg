import {
    Table,
    Column,
    DataType,
    TableOptions,
    ForeignKey,
    BelongsTo,
} from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required } from '../lib/validation';
import CommodityType from './CommodityType';
import Person from './Person';

const options: TableOptions = {
    tableName: "commodities",
    modelName:"Commodity",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class Commodity extends BaseModel<Commodity>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public value: string;

    @Required()
    @ForeignKey( () => CommodityType)
    @Column(DataType.UUID)
    commodityTypeId: string;

    @BelongsTo(() => CommodityType, {foreignKey: {allowNull: false}})
    public commodityType: CommodityType;

    @ForeignKey( () => Person)
    @Column(DataType.UUID)
    contactLeadId: string;

    @BelongsTo(() => Person, {foreignKey: {allowNull: true}})
    public contactLead: Person;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
