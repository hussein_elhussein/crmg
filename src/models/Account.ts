import {
    Table,
    Column,
    DataType,
    TableOptions,
    ForeignKey,
    BelongsTo,
    BelongsToMany, HasMany,
} from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsInt, IsString, Required, IsFloat, IsBoolean } from '../lib/validation';
import OfficeCode from './OfficeCode';
import SalesCode from './SalesCode';
import { injectable } from 'inversify';
import CompanyType from './CompanyType';
import User from './User';
import AccountOfficeCode from './AccountOfficeCode';
import AccountSalesCode from './AccountSalesCode';
import AccountCashAdvisor from './AccountCashAdvisor';
import TradingNumber from './TradingNumber';
import AccountNumber from './AccountNumber';
import RoutingNumber from './RoutingNumber';
import Broker from './Broker';
import Referral from './Referral';
import SecuringAgent from './SecuringAgent';
import InsuranceProvider from './InsuranceProvider';
import MilkHandler from './MilkHandler';
import AccountType from './AccountType';
import MarginPush from './MarginPush';

const options: TableOptions = {
    tableName: "accounts",
    modelName:"Account",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
@injectable()
export default class Account extends BaseModel<Account>{

    @Required()
    @Column(DataType.UUID)
    public accountableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public accountableType: string;

    @HasMany(() => TradingNumber)
    public tradingNumbers: TradingNumber[];

    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @HasMany(() => RoutingNumber)
    public routingNumbers: RoutingNumber[];

    @HasMany(() => AccountNumber)
    public accountNumbers: AccountNumber[];

    @IsString()
    @Column(DataType.TEXT)
    public transactionNote: string;

    @BelongsToMany(() => OfficeCode, () => AccountOfficeCode)
    officeCodes: OfficeCode[];

    @BelongsToMany(() => SalesCode, () => AccountSalesCode)
    public salesCodes: SalesCode[];

    @BelongsToMany(() => User, {
        through: {
            model: () => AccountCashAdvisor,
        },
        foreignKey: "accountId",
        otherKey: "cashAdvisorId"
    })
    public cashAdvisors: User[];

    @ForeignKey(() => Broker)
    @IsString()
    @Column(DataType.UUID)
    public brokerId: string;

    @BelongsTo(() => Broker, {foreignKey: {allowNull: true}})
    public broker: Broker;

    @IsFloat()
    @Column(DataType.FLOAT)
    public contactRate: string;

    @ForeignKey(() => Referral)
    @IsString()
    @Column(DataType.UUID)
    public referralId: string;

    @BelongsTo(() => Referral, {foreignKey: {name: "referralId", field: "referral_id", allowNull: true}})
    public referredBy: Referral;

    @ForeignKey(() => SecuringAgent)
    @IsString()
    @Column(DataType.UUID)
    public securingAgentId: string;

    @BelongsTo(() => SecuringAgent, {foreignKey: {allowNull: true}})
    public securingAgent: SecuringAgent;

    @ForeignKey(() => InsuranceProvider)
    @IsString()
    @Column(DataType.UUID)
    public insuranceProviderId: string;

    @BelongsTo(() => InsuranceProvider, {foreignKey: {allowNull: true}})
    public insuranceProvider: InsuranceProvider;

    @ForeignKey(() => MilkHandler)
    @IsString()
    @Column(DataType.UUID)
    public milkHandlerId: string;

    @BelongsTo(() => MilkHandler, {foreignKey: {allowNull: true}})
    public milkHandler: MilkHandler;

    @ForeignKey(() => AccountType)
    @IsString()
    @Column(DataType.UUID)
    public accountTypeId: string;

    @BelongsTo(() => AccountType, {foreignKey: {allowNull: true}})
    public accountType: AccountType;

    @ForeignKey(() => MarginPush)
    @IsString()
    @Column(DataType.UUID)
    public marginPushId: string;

    @BelongsTo(() => MarginPush, {foreignKey: {allowNull: true}})
    public marginPush: MarginPush;
    
    @ForeignKey(() => CompanyType)
    @IsString()
    @Column(DataType.STRING)
    public companyTypeId: string;

    @BelongsTo(() => CompanyType, {foreignKey: {allowNull: true}})
    companyType: CompanyType;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public hasSecurityAgreement: boolean;

    @IsString()
    @Column(DataType.TEXT({length: 'long'}))
    public securityAgreement: string;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public hedge: boolean;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
