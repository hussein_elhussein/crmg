import {
    BeforeSave,
    BelongsToMany,
    Column,
    DataType,
    HasMany,
    Table,
    TableOptions,
} from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required, IsBoolean } from '../lib/validation';
import Person from './Person';
import { Kernel } from '../lib/kernel/kernel';
import { AuthService } from '../services/AuthService';
import { ErrorHandlerService } from '../lib/services/ErrorHandlerService';
import { UserRepository } from '../repositories/UserRepository';
import PhoneCallActivity from './PhoneCallActivity';
import EventsActivity from './EventsActivity';
import EmailReplyActivity from './EmailReplyActivity';
import AchReportsActivity from './AchReportsActivity';
import DeviceActivity from './DeviceActivity';
import RpmActivity from './RpmActivity';
import ContactsActivity from './ContactsActivity';
import EmailActivity from './EmailActivity';
import MeetingActivity from './MeetingActivity';
import NoteActivity from './NoteActivity';
import UserRole from './UserRole';
import List from './List';
import Task from './Task';
import Role from './Role';
import Account from './Account';
import AccountCashAdvisor from './AccountCashAdvisor';
declare var kernel: Kernel;
const options: TableOptions = {
    tableName: "users",
    modelName: "User",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class User extends BaseModel<User>{
    protected static messages: any = {
        email: "Please enter a valid E-mail address",
        emailExists: "E-mail is already exist",
        password: "Please enter a valid password",
    };
    public hiddenAttributes: Array<string> = ['deletedAt','password'];

    @IsString()
    @Column(DataType.STRING)
    public username: string;

    @IsString()
    @Column(DataType.STRING)
    public firstName: string;

    @IsString()
    @Column(DataType.STRING)
    public lastName: string;

    @HasMany(() => Person)
    people: Person[];

    @HasMany(() => Task)
    tasks: Task[];

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public cashAdvisor: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public active: boolean;

    @BelongsToMany(() => Role, () => UserRole)
    roles: Role[];

    @HasMany(() => List, {
        foreignKey: "ownerId",
        constraints: false,
    })
    lists: List[];

    @HasMany(() => PhoneCallActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    phoneCallActivities: PhoneCallActivity[];

    @HasMany(() => EventsActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    eventsActivities: EventsActivity[];

    @HasMany(() => EmailReplyActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    emailReplyActivities: EmailReplyActivity[];

    @HasMany(() => AchReportsActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    achReportsActivities: AchReportsActivity[];

    @HasMany(() => DeviceActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    deviceActivities: DeviceActivity[];

    @HasMany(() => RpmActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    rpmActivities: RpmActivity[];

    @HasMany(() => ContactsActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    contactsActivities: ContactsActivity[];

    @HasMany(() => EmailActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    emailActivities: EmailActivity[];

    @HasMany(() => MeetingActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    meetingActivities: MeetingActivity[];

    @HasMany(() => NoteActivity, {
        foreignKey: "userId",
        constraints: false,
    })
    noteActivities: NoteActivity[];

    @BelongsToMany(() => Account, {
        through: {
            model: () => AccountCashAdvisor,
        },
        foreignKey: "cash_advisor_id",
        otherKey: "account_id"
    })
    public accounts: Account[];
    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Required()
    @Column({
        type: DataType.STRING,
        validate: {
            isEmail: { msg: User.messages.email},
            notNull: { msg: User.messages.email},
            isUnique: User.isUnique
        },
        allowNull: false,
        unique: {name: "email", msg: User.messages.emailExists},
    })
    public email: string;

    @Required()
    @Column({
        type: DataType.TEXT,
        validate: {
            notEmpty: { msg: User.messages.password},
            notNull: { msg: User.messages.password }
        },
        allowNull: false,
    })
    get password(): string{
        return this.getDataValue('password');
    }

    set password(password: string){
        this.setDataValue('password',password);
    }
    public static async isUnique(email: string): Promise<boolean>{
        const changed = (this as any).changed('email');
        if (changed) {
            const container = kernel.getContainer();
            const errHandler = container.resolve(ErrorHandlerService);
            const userRepo = container.resolve(UserRepository);
            const exists = await userRepo.findByEmail(email).catch(e => errHandler.handle(e));
            if (exists) {
                throw new Error(User.messages.emailExists);
            }
        }
        return true;
    }

    @BeforeSave
    static async hashPassword(instance: User, options: any){
        const container = kernel.getContainer();
        const errHandler = container.resolve(ErrorHandlerService);
        const authSer = container.resolve(AuthService);
        const prevHash = instance.previous('password');
        if(!prevHash){
            // on new user, prev is empty:
            const hashed = await authSer.hashPassword(instance.password).catch(e => errHandler.handle(e));
            if(hashed){
                instance.password = hashed;
            }
            return null;
        }
        const match = await authSer.comparePassword(instance.password, prevHash).catch(e  => errHandler.handle(e));
        // if the password doesn't match, it means we need to change it:
        if (!match) {
            const hashed = await authSer.hashPassword(instance.password).catch(e => errHandler.handle(e));
            if(hashed){
                instance.password = hashed;
            }
        }
        else {
            // this is the same password so ignore it:
            instance.changed('password', false);
        }
    }
}
