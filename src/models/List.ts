import {
    Table,
    Column,
    DataType,
    ForeignKey,
    BelongsTo,
    HasMany,
    BelongsToMany,
    HasOne,
    TableOptions,
} from 'sequelize-typescript';
import User from './User';
import { IsString, Required, IsBoolean } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import ListContact from './ListContact';
import ListFilter from './ListFilter';
import Company from './Company';
import Person from './Person';
import { ContactType, literals } from '../types/modelEnums';
import Address from './Address';
import CompletedContact from './CompletedContact';

const options:TableOptions = {
    tableName: "lists",
    modelName:"List",
    paranoid: true,
    timestamps: true,
    underscored: true,
};

@Table(options)
export default class List extends BaseModel<List>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public listType: string;

    @Required()
    @IsString()
    @Column({type: DataType.ENUM, values: literals.contactType})
    public contactType: ContactType;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public searchNew: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public exclude: boolean;

    @Column(DataType.BOOLEAN)
    public sync?: boolean;

    @Column(DataType.TEXT)
    public mailchimpListId?: string;

    @HasMany(() => ListFilter, {
        foreignKey: "listId",
        constraints: false,
    })
    listFilters: ListFilter[];

    @IsString()
    @Column(DataType.STRING)
    public updateFilter: string;

    @IsString()
    @Column(DataType.STRING)
    public countFilter: string;

    @IsString()
    @Column(DataType.STRING)
    public removeFilter: string;

    @BelongsToMany(() => Person,{
        through: {
            model: () => ListContact,
            unique: false,
            scope: {
                listContactableType: "Person"
            }
        },
        foreignKey: "listId",
        otherKey: "listContactableId",
        constraints: false,
    })
    people: Person[];

    @BelongsToMany(() => Company,{
        through: {
            model: () => ListContact,
            unique: false,
            scope: {
                listContactableType: "Company"
            }
        },
        foreignKey: "listId",
        otherKey: "listContactableId",
        constraints: false,
    })
    companies: Company[];

    @BelongsToMany(() => Person,{
        through: {
            model: () => CompletedContact,
            unique: false,
            scope: {
                completableType: "Person"
            }
        },
        foreignKey: "listId",
        otherKey: "completableId",
        constraints: false,
    })
    completedPersonContacts: Person[];

    @BelongsToMany(() => Company,{
        through: {
            model: () => CompletedContact,
            unique: false,
            scope: {
                completableType: "Company"
            }
        },
        foreignKey: "listId",
        otherKey: "completableId",
        constraints: false,
    })
    completedCompanyContacts: Company[];

    @HasOne(() => Address, {
        foreignKey: "addressableId",
        constraints: false,
        scope: {
            addressableType: "List",
        }
    })
    public address: Address;

    @ForeignKey(() => User)
    @Column(DataType.UUID)
    public ownerId: string;

    @BelongsTo(() => User, { foreignKey: { allowNull: true, name: "ownerId" }})
    owner: User;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}