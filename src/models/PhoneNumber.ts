import { Table, Column, DataType } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import { TableOptions } from '../lib/models/interfaces/TableOptions';
import { DefaultRepository } from '../lib/repositories/DefaultRepository';
import { PhoneNumberType, literals } from '../types/modelEnums'
const options:TableOptions = {
    tableName: "phone_numbers",
    modelName:"PhoneNumber",
    paranoid: true,
    timestamps: true,
    underscored: true,
    repository: DefaultRepository,
};

@Table(options)
export default class PhoneNumber extends BaseModel<PhoneNumber>{

    @Required()
    @IsString()
    @Column(DataType.UUID)
    public phoneNumerableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public phoneNumerableType: string;
    
    @Required()
    @IsString()
    @Column(DataType.STRING)
    public number: string;

    @Required()
    @IsString()
    @Column({type: DataType.ENUM, values: literals.phoneNumberType})
    public type: PhoneNumberType;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
