import { Table, Column, DataType, TableOptions, HasMany } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Person from './Person';
const options: TableOptions = {
    tableName: "lead_sources",
    modelName:"LeadSource",
    timestamps: false,
    paranoid: true,
    underscored: true,
};

@Table(options)
export default class LeadSource extends BaseModel<LeadSource>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @HasMany(() => Person)
    people: Person[];

    @IsString()
    @Column(DataType.TEXT)
    public details: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}