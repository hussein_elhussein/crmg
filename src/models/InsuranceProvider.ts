import { Table, Column, DataType, HasMany, TableOptions } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Account from './Account';
const options:TableOptions = {
    tableName: "insurance_providers",
    modelName:"InsuranceProvider",
    paranoid: true,
    timestamps: true,
    underscored: true,
};
@Table(options)
export default class InsuranceProvider extends BaseModel<InsuranceProvider>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @HasMany(() => Account)
    public accounts: Account[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
