import { Table, Column, DataType, TableOptions, Default } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required, IsBoolean } from '../lib/validation';
import { AddressType, literals, PostalAddressStatus } from '../types/modelEnums';

const options: TableOptions = {
    tableName: "addresses",
    modelName:"Address",
    timestamps: true,
    paranoid: true,
    underscored: true,
};

@Table(options)
export default class Address extends BaseModel<Address>{

    @Required()
    @IsString()
    @Column(DataType.UUID)
    public addressableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public addressableType: string;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.addressType})
    public type: AddressType;

    @Default(false)
    @Column(DataType.BOOLEAN)
    public primary: boolean;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public country: string;

    @Column(DataType.STRING)
    public address: string;

    @IsString()
    @Column(DataType.STRING)
    public addressTwo: string;

    @Column(DataType.STRING)
    public city: string;

    @IsString()
    @Column(DataType.STRING)
    public state: string;

    @Column(DataType.STRING)
    public postalCode: string;

    @Column({type: DataType.INTEGER, values: literals.postalAddressStatus})
    public verificationStatus: PostalAddressStatus;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
