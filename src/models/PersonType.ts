import { Table, Column, DataType, HasMany, TableOptions } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Person from './Person';
const options: TableOptions = {
    tableName: "person_types",
    modelName:"PersonType",
    timestamps: true,
    paranoid: true,
    underscored: true,
};

@Table(options)
export default class PersonType extends BaseModel<PersonType>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @HasMany(() => Person)
    people: Person[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
