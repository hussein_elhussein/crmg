import { Table, Column, DataType, TableOptions,  ForeignKey } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';
import Account from './Account';
import SalesCode from './SalesCode';

const options: TableOptions = {
    tableName: "account_sales_code",
    modelName:"AccountSalesCode",
    timestamps: false,
    paranoid: false,
    underscored: true,
};
@Table(options)
export default class AccountSalesCode extends BaseModel<AccountSalesCode>{

    @Required()
    @ForeignKey(() => Account)
    @Column
    accountId: string;

    @Required()
    @ForeignKey(() => SalesCode)
    @Column
    salesCodeId: string;
}
