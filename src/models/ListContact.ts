import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { IsString, IsInt, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import List from './List';

const options: TableOptions = {
    tableName: "list_contacts",
    modelName:"ListContact",
    timestamps: false,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class ListContact extends BaseModel<ListContact>{

    @Required()
    @IsString()
    @ForeignKey(() => List)
    @Column(DataType.UUID)
    public listId: string;

    @BelongsTo(() => List, {foreignKey: {allowNull: false}})
    list: List;

    @Required()
    @IsString()
    @Column(DataType.UUID)
    public listContactableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public listContactableType: string;

    @IsInt()
    @Column(DataType.INTEGER)
    public index: number;

    @Column(DataType.DATE)
    public deletedAt: string;
}
