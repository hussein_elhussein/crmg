import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, IsBoolean, IsDate, IsInt } from '../lib/validation';
import Person from './Person';
import Document from './Document';
import { OnboardStatus, AccountType, literals } from '../types/modelEnums';

const options: TableOptions = {
    tableName: "onboard_documents",
    modelName: "OnboardDocument",
    timestamps: true,
    paranoid: true,
    underscored: true,
};

@Table(options)
export default class OnboardDocument extends BaseModel<OnboardDocument>{

    @ForeignKey(() => Person)
    @Column(DataType.STRING)
    public personId: string;

    @BelongsTo(() => Person, { foreignKey: { allowNull: true }})
    person: Person;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public powerOfAttorney: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public straitsAccountPaperwork: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public achAuthorizationForm: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public additionalRiskDisclosure: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public partnershipAgreement: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public byLaws: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public driversLicense: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public balanceSheet: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public articlesOfOrganization: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public rpmPaperwork: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public operatingAgreement: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public informationSheet: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public tradingExperience: boolean;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.accountType})
    public accountType: AccountType;

    @IsInt()
    @Column({type: DataType.ENUM, values: literals.onboardStatus})
    public status: OnboardStatus;

    @IsString()
    @Column(DataType.TEXT({ length: 'long'}))
    public statusNotes: string;

    @IsDate()
    @Column(DataType.DATE)
    public lastUpdateTime: string;

    @IsString()
    @Column(DataType.STRING)
    public tradingAccountId: string;

    @HasMany(() => Document, {
        foreignKey: "onboardDocumentId",
        constraints: false,
    })
    documents: Document[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
