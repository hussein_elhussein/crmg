import { Table, Column, DataType, TableOptions, HasMany, BelongsToMany } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required } from '../lib/validation';
import Account from './Account';
import AccountSalesCode from './AccountSalesCode';

const options: TableOptions = {
    tableName: "sales_codes",
    modelName:"SalesCode",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class SalesCode extends BaseModel<SalesCode>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @BelongsToMany(() => Account, () => AccountSalesCode)
    public accounts: Account[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
