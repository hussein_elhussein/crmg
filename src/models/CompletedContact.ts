import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { IsString, IsInt, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import List from './List';

const options: TableOptions = {
    tableName: "completed_contacts",
    modelName:"CompletedContact",
    timestamps: false,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class CompletedContact extends BaseModel<CompletedContact>{

    @Required()
    @IsString()
    @ForeignKey(() => List)
    @Column(DataType.UUID)
    public listId: string;

    @BelongsTo(() => List, {foreignKey: {allowNull: false}})
    list: List;

    @Required()
    @IsString()
    @Column(DataType.UUID)
    public completableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public completableType: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
