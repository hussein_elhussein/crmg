import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';
import ImporterReport from './ImporterReport';

const options: TableOptions = {
    tableName: "importer_failed_items",
    modelName:"ImporterFailedItem",
    timestamps: false,
    underscored: true,
};
@Table(options)
export default class ImporterFailedItem extends BaseModel<ImporterFailedItem>{

    @ForeignKey(() => ImporterReport)
    @Column(DataType.UUID)
    importerReportId: string;

    @BelongsTo(() => ImporterReport, {foreignKey: {allowNull: false}})
    importerReport: ImporterReport;

    @Column(DataType.STRING)
    remoteId: string;

    @Required()
    @Column(DataType.STRING)
    path: string;

    @Required()
    @Column(DataType.STRING)
    message: string;

    @Column(DataType.TEXT)
    error: string|null;
}
