import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';
import Tag from './Tag';

const options: TableOptions = {
    tableName: "tagged",
    modelName:"Tagged",
    timestamps: false,
    underscored: true,
};
@Table(options)
export default class Tagged extends BaseModel<Tagged>{

    @ForeignKey(() => Tag)
    @Column(DataType.UUID)
    public tagId: string;

    @BelongsTo(() => Tag,{foreignKey: {allowNull: false}})
    tag: Tag;

    @Required()
    @Column(DataType.UUID)
    public taggableId: string;

    @Required()
    @Column(DataType.STRING)
    public taggableType: string;
}
