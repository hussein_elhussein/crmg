import { Table, Column, DataType, TableOptions } from 'sequelize-typescript';
import { IsString, Required} from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';

const options:TableOptions = {
    tableName: "notes",
    modelName:"Note",
    paranoid: true,
    timestamps: false,
    underscored: true,
};

@Table(options)
export default class Note extends BaseModel<Note>{

    @Required()
    @IsString()
    @Column(DataType.TEXT)
    public body: string;

    @Required()
    @IsString()
    @Column(DataType.UUID)
    public notableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public notableType: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}