import { Table, Column, DataType, ForeignKey, TableOptions, BelongsTo } from 'sequelize-typescript';
import { IsString, IsDate, Required, IsInt, IsBoolean } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import User from './User';
import { literals, Priority, SendBefore } from '../types/modelEnums';

const options:TableOptions = {
    tableName: "tasks",
    modelName:"Task",
    paranoid: true,
    timestamps: true,
    underscored: true,
};
@Table(options)
export default class Task extends BaseModel<Task>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public type: String;

    @Required()
    @IsString()
    @Column(DataType.UUID)
    public taskableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public taskableType: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public title: string;

    @Required()
    @IsDate()
    @Column(DataType.DATE)
    public date: string;

    @IsString()
    @Column(DataType.TEXT)
    public notes: string;

    @Required()
    @IsString()
    @Column({type: DataType.ENUM, values: literals.priority})
    public priority: Priority;

    @Required()
    @ForeignKey(() => User)
    @IsString()
    @Column(DataType.UUID)
    public assignedTo: string;

    @BelongsTo(() => User, { foreignKey: { allowNull: false, name: "assignedTo" }})
    assignee: User;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public sendRemind: boolean;

    @IsInt()
    @Column({type: DataType.ENUM, values: literals.sendBefore})
    public sendBefore: SendBefore;

    @ForeignKey(() => User)
    @Column(DataType.UUID)
    public userId: string;

    @BelongsTo(() => User, { foreignKey: { allowNull: true }})
    user: User;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public complete: boolean;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
