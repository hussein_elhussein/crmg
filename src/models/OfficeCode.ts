import { Table, Column, DataType, TableOptions, HasMany, BelongsToMany } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required } from '../lib/validation';
import Account from './Account';
import AccountOfficeCode from './AccountOfficeCode';

const options: TableOptions = {
    tableName: "office_codes",
    modelName:"OfficeCode",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class OfficeCode extends BaseModel<OfficeCode>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @BelongsToMany(() => Account, () => AccountOfficeCode)
    public accounts: Account[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
