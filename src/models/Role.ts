import { Table, Column, DataType, TableOptions, ForeignKey, IsNumeric, BelongsToMany } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required } from '../lib/validation';
import User from './User';
import UserRole from './UserRole';

const options: TableOptions = {
    tableName: "roles",
    modelName:"Role",
    timestamps: false,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class Role extends BaseModel<Role>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @BelongsToMany(() => User, () => UserRole)
    users: User[];

    @Column(DataType.DATE)
    public deletedAt: string;
}
