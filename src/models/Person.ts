import {
    Table,
    Column,
    DataType,
    ForeignKey,
    BelongsTo,
    TableOptions,
    HasMany,
    BelongsToMany,
    HasOne, AfterSave,
} from 'sequelize-typescript';
import { IsDate, IsString, IsBoolean, IsUrl, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import PersonType from './PersonType';
import SaleStatus from './SaleStatus';
import BusinessType from './BusinessType';
import User from './User';
import Account from './Account';
import Address from './Address';
import SocialLink from './SocialLink';
import Company from './Company';
import LeadSource from './LeadSource';
import RelatedPerson from './RelatedPerson';
import Note from './Note';
import PhoneNumber from './PhoneNumber';
import EmailReplyActivity from './EmailReplyActivity';
import AchReportsActivity from './AchReportsActivity';
import DeviceActivity from './DeviceActivity';
import RpmActivity from './RpmActivity';
import ContactsActivity from './ContactsActivity';
import EmailActivity from './EmailActivity';
import MeetingActivity from './MeetingActivity';
import NoteActivity from './NoteActivity';
import Email from './Email';
import Task from './Task';
import Tag from './Tag';
import Tagged from './Tagged';
import PhoneCallActivity from './PhoneCallActivity';
import EventsActivity from './EventsActivity';
import NextContact from './NextContact';
import { Prefix, Suffix, StatementType, Gender, literals } from '../types/modelEnums';
import OnboardDocument from './OnboardDocument';
const options: TableOptions = {
    tableName: "people",
    modelName:"Person",
    timestamps:true,
    paranoid:true,
    underscored: true
};

@Table(options)
export default class Person extends BaseModel<Person>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public firstName: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public lastName: string;

    @IsString()
    @Column(DataType.STRING)
    public middleName: string;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.prefix})
    public prefix: Prefix;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.suffix})
    public suffix: Suffix;

    @IsString()
    @Column(DataType.STRING)
    public credentials: string;

    @Required()
    @ForeignKey(() => PersonType)
    @Column(DataType.UUID)
    public personTypeId: string;

    @BelongsTo(() => PersonType,{foreignKey: {allowNull: false}})
    personType: PersonType;

    @IsDate()
    @Column(DataType.DATEONLY)
    public accountOpenedDate: string;

    @ForeignKey(() => SaleStatus)
    @Column(DataType.STRING)
    public saleStatusId: string;

    @BelongsTo(() => SaleStatus, {foreignKey: {allowNull: true}})
    saleStatus: SaleStatus;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.statementType})
    public statementType: StatementType;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public doNotCall: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public powerOfAttorney: boolean;

    @IsDate()
    @Column(DataType.DATEONLY)
    public birthDate: string;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.gender})
    public gender: Gender;

    @IsUrl()
    @Column(DataType.STRING)
    public website: string;

    @IsString()
    @Column(DataType.STRING)
    public jobTitle: string;

    @ForeignKey(() => BusinessType)
    @Column(DataType.UUID)
    public businessTypeId: string;

    @BelongsTo(() => BusinessType,{foreignKey: {allowNull: true}})
    businessType: BusinessType;

    @ForeignKey(() => User)
    @Column(DataType.UUID)
    public ownerId: string;

    @BelongsTo(() => User,{foreignKey: {allowNull: true}})
    owner: User;

    @HasOne(() => Company, {foreignKey: {name: "primaryContactId", field: "primary_contact_id"}})
    company: Company;

    @HasMany(() => Account, {
        foreignKey: "accountableId",
        constraints: false,
        scope: {
            accountableType: "Person",
        },
    })
    accounts: Account[];

    @HasMany(() => Address, {
        foreignKey: "addressableId",
        constraints: false,
        scope: {
            addressableType: "Person",
        },
    })
    addresses: Address[];

    @HasMany(() => Email, {
        foreignKey: "personId",
        constraints: false,
    })
    emails: Email[];

    @HasMany(() => OnboardDocument, {
        foreignKey: "personId",
        constraints: false,
    })
    onboardDocuments: OnboardDocument[];


    @HasMany(() => NextContact, {
        foreignKey: "nextContactableId",
        constraints: false,
        scope: {
            nextContactableType: "Person",
        },
    })
    nextContacts: NextContact[];


    @HasMany(() => Note, {
        foreignKey: "notableId",
        constraints: false,
        scope: {
            notableType: "Person",
        },
    })
    notes: Note[];

    @HasMany(() => PhoneNumber, {
        foreignKey: "phoneNumerableId",
        constraints: false,
        scope: {
            phoneNumerableType: "Person",
        },
    })
    phones: PhoneNumber[];

    @HasMany(() => SocialLink, {
        foreignKey: "socialLinkableId",
        constraints: false,
        scope: {
            socialLinkableType: "Person",
        },
    })
    socialLinks: SocialLink[];

    @ForeignKey(() => LeadSource)
    @Column(DataType.UUID)
    public leadSourceId: string;

    @BelongsTo(() => LeadSource,{foreignKey: {allowNull: true}})
    leadSource: LeadSource;

    @HasMany(() => AchReportsActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    achReportsActivities: AchReportsActivity[];

    @HasMany(() => ContactsActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    contactsActivities: ContactsActivity[];

    @HasMany(() => DeviceActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    deviceActivities: DeviceActivity[];

    @HasMany(() => EmailActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    emailActivities: EmailActivity[];

    @HasMany(() => EmailReplyActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    emailReplyActivities: EmailReplyActivity[];

    @HasMany(() => EventsActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    eventsActivities: EventsActivity[];

    @HasMany(() => MeetingActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    meetingActivities: MeetingActivity[];

    @HasMany(() => NoteActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    noteActivities: NoteActivity[];

    @HasMany(() => PhoneCallActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    phoneCallActivities: PhoneCallActivity[];

    @HasMany(() => RpmActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Person",
        }
    })
    rpmActivities: RpmActivity[];

    @HasMany(() => Task,{
        foreignKey: "taskableId",
        constraints: false,
        scope: {
            taskableType: "Person",
        }
    })
    tasks: Task[];

    @HasMany(() => RelatedPerson, {
        foreignKey: "personId",
        constraints: false,
    })
    relatedPeople: RelatedPerson[];

    @BelongsToMany(() => Person, {
        through: {
            model: () => RelatedPerson,
            unique: false,
        },
        foreignKey: "relativeId",
        as: "relative",
        constraints: false,
    })
    relatives: Person[];

    @BelongsToMany(() => Tag,{
        through: {
            model: () => Tagged,
            unique: false,
            scope: {
                taggableType: "Person",
            }
        },
        foreignKey: "taggableId",
        constraints: false,
    })
    tags: Tag[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;

}
