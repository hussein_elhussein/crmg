import { Table, Column, DataType, ForeignKey, TableOptions, BelongsTo } from 'sequelize-typescript';
import { IsString, IsDate, Required, IsInt, IsBoolean } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import { literals, SendBefore } from '../types/modelEnums';
import User from './User';

const options:TableOptions = {
    tableName: "meeting_activities",
    modelName:"MeetingActivity",
    paranoid: true,
    timestamps: true,
    underscored: true,
};
@Table(options)
export default class MeetingActivity extends BaseModel<MeetingActivity>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public type: String;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public activitiableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public activitiableType: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public title: string;

    @Required()
    @IsDate()
    @Column(DataType.DATE)
    public date: string;

    @IsString()
    @Column(DataType.TEXT)
    public notes: string;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public sendRemind: boolean;

    @IsInt()
    @Column({type: DataType.ENUM, values: literals.sendBefore})
    public sendBefore: SendBefore;

    @ForeignKey(() => User)
    @Column(DataType.UUID)
    public userId: string;

    @BelongsTo(() => User, { foreignKey: { allowNull: true }})
    user: User;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}