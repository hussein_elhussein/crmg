import { Table, Column, DataType, TableOptions, HasMany } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required } from '../lib/validation';
import { Location, literals } from '../types/modelEnums';   
import Company from '../models/Company';
const options: TableOptions = {
    tableName: "location_types",
    modelName:"LocationType",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class LocationType extends BaseModel<LocationType>{

    @Required()
    @IsString()
    @Column({ type: DataType.ENUM, values: literals.location })
    public name: Location;

    // @HasMany(() => Company, {
    //     foreignKey: "locationTypeId",
    //     constraints: false,
    // })
    // companies: Company[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
