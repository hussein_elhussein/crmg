import { Table, Column, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import { TableOptions } from '../lib/models/interfaces/TableOptions';
import { DefaultRepository } from '../lib/repositories/DefaultRepository';
import Account from './Account';
const options:TableOptions = {
    tableName: "account_numbers",
    modelName:"AccountNumber",
    paranoid: true,
    timestamps: true,
    underscored: true,
    repository: DefaultRepository
};

@Table(options)
export default class AccountNumber extends BaseModel<AccountNumber>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public number: string;

    @Required()
    @ForeignKey(() => Account)
    @Column
    accountId: string;

    @BelongsTo(() => Account)
    account: Account;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
