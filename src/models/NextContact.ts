import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { IsString, IsInt, IsBoolean, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import { literals, SendBefore } from '../types/modelEnums';
import User from './User';

const options:TableOptions = {
    tableName: "next_contacts",
    modelName:"NextContact",
    paranoid: true,
    timestamps: true,
    underscored: true,
};
@Table(options)
export default class NextContact extends BaseModel<NextContact>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public title: string;

    @IsString()
    @Column(DataType.TEXT)
    public notes: string;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public sendRemind: boolean;

    @IsInt()
    @Column({type: DataType.ENUM, values: literals.sendBefore})
    public sendBefore: SendBefore;

    @IsString()
    @Column(DataType.STRING)
    public type: string;

    @Required()
    @IsString()
    @Column(DataType.UUID)
    public nextContactableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public nextContactableType: string;

    @ForeignKey(() => User)
    @Column(DataType.UUID)
    public userId: string;

    @BelongsTo(() => User, { foreignKey: { allowNull: true }})
    user: User;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public complete: boolean;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}