import { Table, Column, DataType, BelongsTo, TableOptions, ForeignKey } from 'sequelize-typescript';
import { IsString } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Person from './Person';
import RelationshipsType from './RelationshipsType';
const options: TableOptions = {
    tableName: "related_people",
    modelName:"RelatedPerson",
    timestamps: false,
    paranoid: true,
    underscored: true,
};

@Table(options)
export default class RelatedPerson extends BaseModel<RelatedPerson>{

    @ForeignKey(() => Person)
    @IsString()
    @Column(DataType.STRING)
    public personId: string;

    @BelongsTo(() => Person, "personId")
    person: Person;

    @ForeignKey(() => Person)
    @IsString()
    @Column(DataType.STRING)
    public relativeId: string;

    @BelongsTo(() => Person, "relativeId")
    relative: Person;

    @ForeignKey(() => RelationshipsType)
    @IsString()
    @Column(DataType.STRING)
    public relationshipsTypeId: string;

    @BelongsTo(() => RelationshipsType, {foreignKey: {allowNull: true}})
    relationshipsType: RelationshipsType;

    @Column(DataType.DATE)
    public deletedAt: string;
}