import { Table, Column, DataType, BelongsToMany, TableOptions } from 'sequelize-typescript';
import { IsString, Required} from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Person from './Person';
import RelatedPerson from './RelatedPerson';

const options:TableOptions = {
    tableName: "relationships_types",
    modelName:"RelationshipsType",
    paranoid: true,
    timestamps: false,
    underscored: true,
};

@Table(options)
export default class RelationshipsType extends BaseModel<RelationshipsType>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @BelongsToMany(() => Person, () => RelatedPerson)
    people: Person[];

    @Column(DataType.DATE)
    public deletedAt: string;
}