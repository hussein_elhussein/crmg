import { Table, Column, DataType } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import { TableOptions } from '../lib/models/interfaces/TableOptions';
import { DefaultRepository } from '../lib/repositories/DefaultRepository';
const options:TableOptions = {
    tableName: "tags",
    modelName:"Tag",
    paranoid: true,
    timestamps: true,
    underscored: true,
    repository: DefaultRepository
};

@Table(options)
export default class Tag extends BaseModel<Tag>{

    @IsString()
    @Column(DataType.STRING)
    public colorHex: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
