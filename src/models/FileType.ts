import { Table, Column, DataType, TableOptions } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';

const options:TableOptions = {
    tableName: "file_types",
    modelName:"FileType",
    paranoid: true,
    timestamps: true,
    underscored: true,
};
@Table(options)
export default class FileType extends BaseModel<FileType>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
