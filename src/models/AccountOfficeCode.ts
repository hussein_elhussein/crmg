import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';
import Account from './Account';
import OfficeCode from './OfficeCode';

const options: TableOptions = {
    tableName: "account_office_code",
    modelName:"AccountOfficeCode",
    timestamps: false,
    paranoid: false,
    underscored: true,
};
@Table(options)
export default class AccountOfficeCode extends BaseModel<AccountOfficeCode>{

    @Required()
    @ForeignKey(() => Account)
    @Column
    accountId: string;

    @BelongsTo(() => Account)
    account: Account;

    @Required()
    @ForeignKey(() => OfficeCode)
    @Column
    officeCodeId: string;

    @BelongsTo(() => OfficeCode)
    officeCode: OfficeCode;
}
