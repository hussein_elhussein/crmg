import { Table, Column, DataType, TableOptions } from 'sequelize-typescript';
import { IsString, Required, IsUrl } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import { SocialLinkType, literals } from '../types/modelEnums'

const options:TableOptions = {
    tableName: "social_links",
    modelName:"SocialLink",
    paranoid: true,
    timestamps: true,
    underscored: true,
};

@Table(options)
export default class SocialLink extends BaseModel<SocialLink>{
    
    @Required()
    @IsString()
    @Column(DataType.UUID)
    public socialLinkableId: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public socialLinkableType: string;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.socialLinkType})
    public socialLinkType: SocialLinkType;

    @Required()
    @IsUrl()
    @Column(DataType.STRING)
    public url: string;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}