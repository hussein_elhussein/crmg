import { Table, Column, DataType, HasMany, TableOptions } from 'sequelize-typescript';
import Company from './Company';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Person from './Person';
const options:TableOptions = {
    tableName: "business_types",
    modelName:"BusinessType",
    paranoid: true,
    timestamps: true,
    underscored: true,
};
@Table(options)
export default class BusinessType extends BaseModel<BusinessType>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @HasMany(() => Person, {
        foreignKey: "businessTypeId",
        constraints: false,
    })
    people: Person[];

    @HasMany(() => Company, {
        foreignKey: "businessTypeId",
        constraints: false,
    })
    companies: Company[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
