import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { IsString, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import List from './List';
const options:TableOptions = {
    tableName: "list_filters",
    modelName:"ListFilter",
    paranoid: true,
    timestamps: true,
    underscored: true,
};
@Table(options)
export default class ListFilter extends BaseModel<ListFilter>{

    @IsString()
    @Column(DataType.STRING)
    public type: string;

    @IsString()
    @Column(DataType.STRING)
    public property: string;

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public operator: string;

    @IsString()
    @Column(DataType.TEXT({length: 'long'}))
    public values: string;

    @Required()
    @ForeignKey(() => List)
    @IsString()
    @Column(DataType.UUID)
    public listId: string;

    @BelongsTo(() => List)
    list: List;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
