import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { IsString, Required, IsUrl } from '../lib/validation';
import { FileType, literals } from '../types/modelEnums';
import OnboardDocument from './OnboardDocument';

const options: TableOptions = {
    tableName: "documents",
    modelName: "Document",
    timestamps: true,
    paranoid: true,
    underscored: true,
};

@Table(options)
export default class Document extends BaseModel<Document>{

    @Required()
    @ForeignKey(() => OnboardDocument)
    @IsString()
    @Column(DataType.UUID)
    public onboardDocumentId: string;

    @BelongsTo(() => OnboardDocument, {foreignKey: {allowNull: false}})
    onboardDocument: OnboardDocument;

    @Required()
    @IsString()
    @Column({type: DataType.ENUM, values: literals.fileType})
    public fileType: FileType;

    @Required()
    @Column(DataType.STRING)
    public file: string;

    @IsString()
    @Column(DataType.STRING)
    public status: string;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
