import { Table, Column, DataType, TableOptions, HasMany } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';
import ImporterFailedItem from './ImporterFailedItem';

const options: TableOptions = {
    tableName: "importer_reports",
    modelName:"ImporterReport",
    timestamps: false,
    underscored: true,
};
@Table(options)
export default class ImporterReport extends BaseModel<ImporterReport>{

    @Required()
    @Column(DataType.STRING)
    name: string;

    @Column(DataType.INTEGER)
    importedItems: number;

    @Column(DataType.INTEGER)
    updatedItems: number;

    @Column(DataType.INTEGER)
    total: number;

    @HasMany(() => ImporterFailedItem)
    failed: ImporterFailedItem[];

    @Required()
    @Column(DataType.BOOLEAN)
    completed: boolean;
}
