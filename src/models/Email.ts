import { Table, Column, DataType, ForeignKey, TableOptions, BelongsTo } from 'sequelize-typescript';
import { IsString, IsEmail, IsBoolean, Required } from '../lib/validation';
import { BaseModel } from '../lib/models/BaseModel';
import Person from './Person';
import { EmailType, literals } from '../types/modelEnums';
const options:TableOptions = {
    tableName: "emails",
    modelName:"Email",
    paranoid: true,
    timestamps: true,
    underscored: true,
};

@Table(options)
export default class Email extends BaseModel<Email>{

    @Required()
    @ForeignKey(() => Person)
    @IsString()
    @Column(DataType.UUID)
    public personId: string;

    @BelongsTo(() => Person)
    person: Person;

    @IsEmail()
    @Column(DataType.STRING)
    public address: string;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public primary: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public unsubscribe: boolean;

    @IsBoolean()
    @Column(DataType.BOOLEAN)
    public ccTransactionReport: boolean;

    @IsString()
    @Column({type: DataType.ENUM, values: literals.emailType})
    public type: EmailType;

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
