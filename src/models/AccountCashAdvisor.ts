import { Table, Column, DataType, TableOptions, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';
import Account from './Account';
import User from './User';

const options: TableOptions = {
    tableName: "account_cash_advisor",
    modelName:"AccountCashAdvisor",
    timestamps: false,
    paranoid: false,
    underscored: true,
};
@Table(options)
export default class AccountCashAdvisor extends BaseModel<AccountCashAdvisor>{

    @Required()
    @ForeignKey(() => Account)
    @Column
    accountId: string;

    @BelongsTo(() => Account)
    account: Account;

    @Required()
    @ForeignKey(() => User)
    @Column
    cashAdvisorId: string;

    @BelongsTo(() => User)
    cashAdvisor: User;
}
