import {
    Table,
    Column,
    DataType,
    BelongsTo,
    ForeignKey,
    TableOptions,
    HasMany, BelongsToMany,
} from 'sequelize-typescript';
import BusinessType from './BusinessType';
import { BaseModel } from '../lib/models/BaseModel';
import LocationType from './LocationType';
import Person from './Person';
import { IsDate, IsFloat, IsInt, IsString, IsUrl, Required } from '../lib/validation';
import Address from './Address';
import Tag from './Tag';
import Tagged from './Tagged';
import PhoneNumber from './PhoneNumber';
import Note from './Note';
import User from './User';
import NextContact from './NextContact';
import Account from './Account';
import SocialLink from './SocialLink';
import PhoneCallActivity from './PhoneCallActivity';
import EventsActivity from './EventsActivity';
import EmailReplyActivity from './EmailReplyActivity';
import AchReportsActivity from './AchReportsActivity';
import DeviceActivity from './DeviceActivity';
import RpmActivity from './RpmActivity';
import ContactsActivity from './ContactsActivity';
import EmailActivity from './EmailActivity';
import MeetingActivity from './MeetingActivity';
import NoteActivity from './NoteActivity';
import Task from './Task';

const options: TableOptions = {
    tableName: "companies",
    modelName:"Company",
    timestamps: true,
    paranoid: true,
    underscored: true,
};
@Table(options)
export default class Company extends BaseModel<Company>{

    @Required()
    @IsString()
    @Column(DataType.STRING)
    public name: string;

    @IsUrl()
    @Column(DataType.STRING)
    public website: string;

    @ForeignKey(() => BusinessType)
    @Column(DataType.UUID)
    public businessTypeId: string;

    @BelongsTo(() => BusinessType,{foreignKey: {allowNull: true}})
    businessType: BusinessType;

    @ForeignKey(() => LocationType)
    @Column(DataType.UUID)
    public locationTypeId: string;

    @BelongsTo(() => LocationType, {foreignKey: {allowNull: true}})
    locationType: LocationType;

    @IsFloat()
    @Column(DataType.FLOAT)
    public annualRevenue: string;

    @IsDate()
    @Column(DataType.DATE)
    public accountOpenedDate: string;

    @IsInt()
    @Column(DataType.INTEGER)
    public numberOfEmployees: number;

    @ForeignKey(() => Company)
    @Column(DataType.STRING)
    public parentCompanyId: string;

    @BelongsTo(() => Company, {foreignKey: {allowNull: true}})
    parentCompany: Company;

    @ForeignKey(() => Person)
    @Column(DataType.UUID)
    public primaryContactId: string;

    @BelongsTo(() => Person,{foreignKey: {allowNull: true}})
    primaryContact: Person;

    @HasMany(() => Account, {
        foreignKey: "accountableId",
        constraints: false,
        scope: {
            accountableType: "Company",
        },
    })
    accounts: Account[];

    @HasMany(() => Address, {
        foreignKey: "addressableId",
        constraints: false,
        scope: {
            addressableType: "Company",
        },
    })
    addresses: Address[];

    @HasMany(() => Note, {
        foreignKey: "notableId",
        constraints: false,
        scope: {
            notableType: "Company",
        },
    })
    notes: Note[];

    @HasMany(() => PhoneNumber, {
        foreignKey: "phoneNumerableId",
        constraints: false,
        scope: {
            phoneNumerableType: "Company",
        },
    })
    phones: PhoneNumber[];

    @HasMany(() => SocialLink, {
        foreignKey: "socialLinkableId",
        constraints: false,
        scope: {
            socialLinkableType: "Company",
        },
    })
    socialLinks: SocialLink[];

    @HasMany(() => AchReportsActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    achReportsActivities: AchReportsActivity[];

    @HasMany(() => ContactsActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    contactsActivities: ContactsActivity[];

    @HasMany(() => NextContact,{
        foreignKey: "nextContactableId",
        constraints: false,
        scope: {
            nextContactableType: "Company",
        }
    })
    nextContacts: NextContact[];

    @HasMany(() => DeviceActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    deviceActivities: DeviceActivity[];

    @HasMany(() => EmailActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    emailActivities: EmailActivity[];

    @HasMany(() => EmailReplyActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    emailReplyActivities: EmailReplyActivity[];

    @HasMany(() => EventsActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    eventsActivities: EventsActivity[];

    @HasMany(() => MeetingActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    meetingActivities: MeetingActivity[];

    @HasMany(() => NoteActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    noteActivities: NoteActivity[];

    @HasMany(() => PhoneCallActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    phoneCallActivities: PhoneCallActivity[];

    @HasMany(() => RpmActivity,{
        foreignKey: "activitiableId",
        constraints: false,
        scope: {
            activitiableType: "Company",
        }
    })
    rpmActivities: RpmActivity[];

    @HasMany(() => Task,{
        foreignKey: "taskableId",
        constraints: false,
        scope: {
            taskableType: "Company",
        }
    })
    tasks: Task[];

    @BelongsToMany(() => User,{
        through: {
            model: () => NextContact,
            unique: false,
            scope: {
                nextContactableType: "Company",
            }
        },
        foreignKey: "nextContactableId",
        constraints: false,
    })
    users: User[];

    @BelongsToMany(() => Tag,{
        through: {
            model: () => Tagged,
            unique: false,
            scope: {
                taggableType: "Company",
            }
        },
        foreignKey: "taggableId",
        constraints: false,
    })
    tags: Tag[];

    @Column(DataType.DATE)
    public createdAt: string;

    @Column(DataType.DATE)
    public updatedAt: string;

    @Column(DataType.DATE)
    public deletedAt: string;
}
