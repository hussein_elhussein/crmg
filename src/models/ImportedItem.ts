import { Table, Column, DataType, TableOptions } from 'sequelize-typescript';
import { BaseModel } from '../lib/models/BaseModel';
import { Required } from '../lib/validation';

const options: TableOptions = {
    tableName: "imported_items",
    modelName:"ImportedItem",
    timestamps: false,
    underscored: true,
};
@Table(options)
export default class ImportedItem extends BaseModel<ImportedItem>{

    @Column(DataType.UUID)
    itemId: string;

    @Column(DataType.STRING)
    public uniqueId: string;

    @Required()
    @Column(DataType.STRING)
    public tableName: string;

    @Required()
    @Column(DataType.BOOLEAN)
    public imported: boolean;

    @Column(DataType.BOOLEAN)
    public updated: boolean;
}
