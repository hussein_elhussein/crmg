import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import PersonType from '../../models/PersonType';

@injectable()
export class PersonTypeSeeder extends BaseSeeder<PersonType>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 3;
	}

	getModel(): StaticModel<PersonType>{
		return PersonType;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<PersonType[]> {
		return await Factory.generate(PersonType, 1);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('person_types',{});
	}

}
