import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import Person from '../../models/Person';

@injectable()
export class PersonSeeder extends BaseSeeder<Person>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 4;
	}

	getModel(): StaticModel<Person>{
		return Person;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<Person[]> {
		return await Factory.generate(Person, 1);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('people',{});
	}

}
