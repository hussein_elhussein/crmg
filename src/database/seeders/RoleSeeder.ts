import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import Role from '../../models/Role';

@injectable()
export class RoleSeeder extends BaseSeeder<Role>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 1;
	}

	getModel(): StaticModel<Role>{
		return Role;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<Role[]> {
		return await Factory.generate(Role,1);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('roles',{});
	}

}
