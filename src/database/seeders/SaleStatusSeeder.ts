import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import SaleStatus from '../../models/SaleStatus';

@injectable()
export class SaleStatusSeeder extends BaseSeeder<SaleStatus>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 3;
	}

	getModel(): StaticModel<SaleStatus>{
		return SaleStatus;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<SaleStatus[]> {
		return await Factory.generate(SaleStatus, 1);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('sales_code',{});
	}

}
