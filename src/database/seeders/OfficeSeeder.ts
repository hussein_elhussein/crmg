import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import OfficeCode from '../../models/OfficeCode';

@injectable()
export class OfficeSeeder extends BaseSeeder<OfficeCode>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 3;
	}

	getModel(): StaticModel<OfficeCode>{
		return OfficeCode;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<OfficeCode[]> {
		return await Factory.generate(OfficeCode, 2);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('offices',{});
	}

}
