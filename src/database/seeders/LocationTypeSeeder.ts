import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import LocationType from '../../models/LocationType';

@injectable()
export class LocationTypeSeeder extends BaseSeeder<LocationType>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 3;
	}

	getModel(): StaticModel<LocationType>{
		return LocationType;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<LocationType[]> {
		return await Factory.generate(LocationType, 1);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('location_types',{});
	}

}
