import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import User from '../../models/User';

@injectable()
export class UserSeeder extends BaseSeeder<User>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 2;
	}

	getModel(): StaticModel<User>{
		return User;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<User[]> {
		return await Factory.generate(User,2);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('users',{});
	}

}
