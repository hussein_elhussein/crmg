import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import SalesCode from '../../models/SalesCode';

@injectable()
export class SalesCodeSeeder extends BaseSeeder<SalesCode>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 3;
	}

	getModel(): StaticModel<SalesCode>{
		return SalesCode;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<SalesCode[]> {
		return await Factory.generate(SalesCode, 2);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('sales_codes',{});
	}

}
