import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import Company from '../../models/Company';

@injectable()
export class CompanySeeder extends BaseSeeder<Company>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 5;
	}

	getModel(): StaticModel<Company>{
		return Company;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<Company[]> {
		return await Factory.generate(Company,1);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('roles',{});
	}

}
