import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import Tag from '../../models/Tag';

@injectable()
export class TagSeeder extends BaseSeeder<Tag>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 3;
	}

	getModel(): StaticModel<Tag>{
		return Tag;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<Tag[]> {
		return await Factory.generate(Tag,1);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('tags',{});
	}

}
