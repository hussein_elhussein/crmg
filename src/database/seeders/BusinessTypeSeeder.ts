import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import BusinessType from '../../models/BusinessType';

@injectable()
export class BusinessTypeSeeder extends BaseSeeder<BusinessType>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 3;
	}

	getModel(): StaticModel<BusinessType>{
		return BusinessType;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<BusinessType[]> {
		return await Factory.generate(BusinessType, 2);
	}

	async down(): Promise<any> {
		await this.queryInterface.bulkDelete('business_types',{});
	}

}
