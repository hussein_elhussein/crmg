import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  await query.addColumn('imported_items', 'imported', {
    type: DataType.BOOLEAN,
    allowNull: true,
  });
  await query.addColumn('imported_items', 'updated', {
    type: DataType.BOOLEAN,
    allowNull: true,
  });
}

export async function down(query: QueryInterface) {
  await query.removeColumn('imported_items','imported');
  await query.removeColumn('imported_items','updated');
}