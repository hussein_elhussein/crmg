import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable("related_people", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
        },
        person_id: {
			type: DataType.UUID,
			allowNull: true,
			references: {
				model: "people",
				key: "id",
			},
        },
        relative_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
				model: "people",
				key: "id",
			},
        },
        relationships_type_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "relationships_types",
                key: "id",
            },
        },
		deleted_at: {
            allowNull: true,
            type: DataType.DATE
        },
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("related_people");
}