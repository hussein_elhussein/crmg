import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	/*
      Add altering commands here.

      Example:
      await query.createTable('users', { id: DataType.INTEGER });
      console.log('Table users created!');
    */
	await query.createTable("addresses", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		addressable_id: {
			type: DataType.UUID,
			allowNull: false,
		},
		addressable_type: {
			type: DataType.STRING,
			allowNull: false,
		},
		type: {
			type: DataType.STRING,
			allowNull: true,
		},
		primary: {
			type: DataType.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
		country: {
			type: DataType.STRING,
			allowNull: false,
		},
		address: {
			type: DataType.STRING,
			allowNull: true,
		},
		address_two: {
			type: DataType.STRING,
			allowNull: true,
		},
		city: {
			type: DataType.STRING,
			allowNull: true,
		},
		state: {
			type: DataType.STRING,
			allowNull: true,
		},
		postal_code: {
			type: DataType.STRING,
			allowNull: true,
		},
		verification_status: {
			type: DataType.INTEGER,
			allowNull: true,
		},
		created_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		updated_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE,
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("addresses");
}