import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  await query.createTable('roles', {
    id: {
      type: sequelize.UUID,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataType.STRING
    },
    deleted_at: {
      allowNull: true,
      type: DataType.DATE
    },

  });

}

export async function down(query: QueryInterface) {
  await query.dropTable('roles');
}