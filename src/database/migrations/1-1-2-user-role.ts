import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  await query.createTable('user_role', {
    id: {
      type: sequelize.UUID,
      defaultValue: sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: 'users',
        key: 'id',
      },
      onDelete: "SET NULL",
    },
    role_id: {
      type: DataType.UUID,
      references: {
        model: 'roles',
        key: 'id',
      },
      onDelete: "SET NULL",
    },
    deleted_at: {
      allowNull: true,
      type: DataType.DATE
    },

  });

}

export async function down(query: QueryInterface) {
  await query.dropTable('user_role');
}