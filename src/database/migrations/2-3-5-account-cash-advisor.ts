import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("account_cash_advisor", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        account_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: "accounts",
                key: "id",
            },
        },
        cash_advisor_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: "users",
                key: "id",
            },
        },
    });
  
}

export async function down(query: QueryInterface) {
    await query.dropTable("account_cash_advisor");
}