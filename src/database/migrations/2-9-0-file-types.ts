import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("file_types", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        name: {
            type: DataType.STRING,
            allowNull: false,
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });

}

export async function down(query: QueryInterface) {
    await query.dropTable("file_types");
}