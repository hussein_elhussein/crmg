import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
  /*
    Add altering commands here.

    Example:
    await query.createTable('users', { id: DataType.INTEGER });
    console.log('Table users created!');
  */
  await query.createTable("people", {
    id: {
      type: DataType.UUID,
      allowNull: false,
      primaryKey: true,
    },
    first_name: {
      type: DataType.STRING,
      allowNull: false,
    },
    last_name: {
      type: DataType.STRING,
      allowNull: false,
    },
    middle_name: {
      type: DataType.STRING,
      allowNull: true,
    },
    prefix: {
      type: DataType.STRING,
      allowNull: true,
    },
    suffix: {
      type: DataType.STRING,
      allowNull: true
    },
    credentials: {
      type: DataType.STRING,
      allowNull: true,
    },
    person_type_id: {
      type: DataType.UUID,
      allowNull: false,
      references: {
        model: "person_types",
        key: "id",
      },
    },
    account_opened_date: {
      type: DataType.DATEONLY,
      allowNull: true,
    },
    sale_status_id: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: "sale_status",
        key: "id",
      },
      onDelete: "SET NULL",
    },
    statement_type: {
      type: DataType.STRING,
      allowNull: true,
    },
    do_not_call: {
      type: DataType.BOOLEAN,
      allowNull: true,
    },
    power_of_attorney: {
      type: DataType.BOOLEAN,
      allowNull: true,
    },
    birth_date: {
      type: DataType.DATEONLY,
      allowNull: true,
    },
    gender: {
      type: DataType.STRING,
      allowNull: true
    },
    website: {
      type: DataType.STRING,
      allowNull: true,
    },
    job_title: {
      type: DataType.STRING,
      allowNull: true,
    },
    business_type_id: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: "business_types",
        key: "id",
      },
      onDelete: "SET NULL",
    },
    lead_source_id: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: "lead_sources",
        key: "id",
      },
      onDelete: "SET NULL",
    },
    owner_id: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: "users",
        key: "id",
      },
      onDelete: "SET NULL",
    },
    created_at: {
      allowNull: false,
      type: DataType.DATE
    },
    updated_at: {
      allowNull: false,
      type: DataType.DATE
    },
    deleted_at: {
      allowNull: true,
      type: DataType.DATE
    },
  });

}

export async function down(query: QueryInterface) {
  await query.dropTable("people");
}