import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
  await query.createTable("next_contacts", {
    id: {
      type: DataType.UUID,
      allowNull: false,
      primaryKey: true,
    },
    title: {
      type: DataType.STRING,
      allowNull: false,
    },
    notes: {
      type: DataType.TEXT,
      allowNull: true,
    },
    send_remind: {
      type: DataType.BOOLEAN,
      allowNull: true,
    },
    send_before: {
      type: DataType.INTEGER,
      allowNull: true,
    },
    type: {
      type: DataType.STRING,
      allowNull: true,
    },
    next_contactable_id: {
      type: DataType.UUID,
      allowNull: false,
    },
    next_contactable_type: {
      type: DataType.STRING,
      allowNull: false,
    },
    user_id: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: 'users',
        key: 'id',
      },
      onDelete: 'SET NULL',
    },
    complete: {
      type: DataType.BOOLEAN,
      allowNull: true,
    },
    created_at: {
      allowNull: false,
      type: DataType.DATE,
    },
    updated_at: {
      allowNull: true,
      type: DataType.DATE,
    },
    deleted_at: {
      allowNull: true,
      type: DataType.DATE,
    },
  });

}

export async function down(query: QueryInterface) {
  await query.dropTable("next_contacts");
}