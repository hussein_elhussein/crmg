import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("documents", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        onboard_document_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: 'onboard_documents',
                key: 'id'
            }
        },
        file_type: {
            type: DataType.STRING,
            allowNull: false,
        },
        file: {
            type: DataType.STRING,
            allowNull: false,
        },
        status: {
            type: DataType.STRING,
            allowNull: true,
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });

}

export async function down(query: QueryInterface) {
    await query.dropTable("documents");
}