import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("onboard_documents", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        person_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: 'people',
                key: 'id',
            },
        },
        power_of_attorney: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        straits_account_paperwork: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        ach_authorization_form: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        additional_risk_disclosure: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        partnership_agreement: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        by_laws: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        drivers_license: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        balance_sheet: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        articles_of_organization: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        rpm_paperwork: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        operating_agreement: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        information_sheet: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        trading_experience: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        account_type: {
            type: DataType.STRING,
            allowNull: true,
        },
        status: {
            type: DataType.STRING,
            allowNull: true,
        },
        status_notes: {
            type: DataType.TEXT({ length: 'long' }),
            allowNull: true,
        },
        last_update_time: {
            type: DataType.DATE,
            allowNull: true,
        },
        trading_account_id: {
            type: DataType.STRING,
            allowNull: true,
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });

}

export async function down(query: QueryInterface) {
    await query.dropTable("onboard_documents");
}