import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("account_numbers", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        number: {
            type: DataType.STRING,
            allowNull: false,
        },
        account_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: "accounts",
                key: "id",
            },
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: true,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });
  
}

export async function down(query: QueryInterface) {
    await query.dropTable("account_numbers");
}