import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("commodities", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        value: {
            allowNull: false,
            type: DataType.STRING,
        },
        commodity_type_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: 'commodity_types',
                key: 'id'
            }
        },
        contact_lead_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: 'people',
                key: 'id'
            }
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });

}

export async function down(query: QueryInterface) {
    await query.dropTable("commodities");
}