import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable('list_contacts', {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		list_contactable_id: {
			type: DataType.UUID,
			allowNull: false,
		},
		list_contactable_type: {
			type: DataType.STRING,
			allowNull: false,
		},
		list_id: {
			type: DataType.UUID,
			allowNull: false,
			references: {
				model: 'lists',
				key: 'id',
			},
		},
		index: {
			type: DataType.INTEGER,
			allowNull: true,
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE,
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable('list_contacts');
}