import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';
import { literals } from '../../types/modelEnums';

export async function up(query: QueryInterface) {
    await query.createTable("lists", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        name: {
            type: DataType.STRING,
            allowNull: false,
        },
        list_type: {
            type: DataType.STRING,
            allowNull: false,
        },
        contact_type: {
            type: DataType.ENUM,
            values: literals.contactType,
            allowNull: true,
        },
        exclude: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        search_new: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        sync: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        mailchimp_list_id: {
            type: DataType.STRING,
            allowNull: true,
        },
        update_filter: {
            type: DataType.STRING,
            allowNull: true,
        },
        count_filter: {
            type: DataType.STRING,
            allowNull: true,
        },
        remove_filter: {
            type: DataType.STRING,
            allowNull: true,
        },
        owner_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "users",
                key: "id",
            },
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });

}

export async function down(query: QueryInterface) {
    await query.dropTable("lists");
}