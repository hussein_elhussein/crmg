import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable('completed_contacts', {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		completable_id: {
			type: DataType.UUID,
			allowNull: false,
		},
		completable_type: {
			type: DataType.STRING,
			allowNull: false,
		},
		list_id: {
			type: DataType.UUID,
			allowNull: false,
			references: {
				model: 'lists',
				key: 'id',
			},
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE,
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable('completed_contacts');
}