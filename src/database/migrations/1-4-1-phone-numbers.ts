import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface, sequelize: any) {
	await query.createTable("phone_numbers", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		phone_numerable_id: {
			type: DataType.UUID,
			allowNull: false,
		},
		phone_numerable_type: {
			type: DataType.STRING,
			allowNull: false,
		},
		number: {
			type: DataType.STRING,
			allowNull: false,
		},
		type: {
			type: DataType.STRING,
			allowNull: false,
		},
		created_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		updated_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE,
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("phone_numbers");
}