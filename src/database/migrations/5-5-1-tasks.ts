import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export async function up(query: QueryInterface) {
  await query.createTable('tasks', {
    id: {
      type: DataType.UUID,
      allowNull: false,
      primaryKey: true,
    },
    type: {
      type: DataType.STRING,
      allowNull: false,
    },
    taskable_id: {
      type: DataType.UUID,
      allowNull: false,
    },
    taskable_type: {
      type: DataType.STRING,
      allowNull: false,
    },
    title: {
      type: DataType.STRING,
      allowNull: false,
    },
    date: {
      type: DataType.DATE,
      allowNull: false,
    },
    notes: {
      type: DataType.TEXT,
      allowNull: true,
    },
    priority: {
      type: DataType.STRING,
      allowNull: false,
    },
    assigned_to: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: 'users',
        key: 'id',
      },
      onDelete: 'SET NULL',
    },
    send_remind: {
      type: DataType.BOOLEAN,
      allowNull: true,
    },
    send_before: {
      type: DataType.INTEGER,
      allowNull: true,
    },
    user_id: {
      type: DataType.UUID,
      allowNull: true,
      references: {
        model: 'users',
        key: 'id',
      },
      onDelete: 'SET NULL',
    },
    complete: {
      type: DataType.BOOLEAN,
      allowNull: true,
    },
    created_at: {
      allowNull: false,
      type: DataType.DATE,
    },
    updated_at: {
      allowNull: false,
      type: DataType.DATE,
    },
    deleted_at: {
      allowNull: true,
      type: DataType.DATE,
    },
  });

}

export async function down(query: QueryInterface) {
  await query.dropTable('tasks');
}