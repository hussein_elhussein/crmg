import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable("ach_reports_activities", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
        },
        type: {
            type: DataType.STRING,
            allowNull: false,
        },
        activitiable_id: {
			type: DataType.UUID,
			allowNull: false,
        },
        activitiable_type: {
            type: DataType.STRING,
            allowNull: false,
        },
        title: {
            type: DataType.STRING,
            allowNull: false,
        },
        date: {
            type: DataType.DATE,
            allowNull: false,
        },
        notes: {
            type: DataType.TEXT,
            allowNull: true,
        },
        report_type: {
            type: DataType.STRING,
            allowNull: false,
        },
        report_date: {
            type: DataType.DATE,
            allowNull: false,
        },
        transaction_type: {
            type: DataType.STRING,
            allowNull: true,
        },
        user_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "users",
                key: "id",
            },
            onDelete: "SET NULL",
        },
		created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("ach_reports_activities");
}