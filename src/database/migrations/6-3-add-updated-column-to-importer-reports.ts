import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  await query.addColumn('importer_reports', 'updated_items', {
    type: DataType.INTEGER,
    allowNull: true,
  });
}

export async function down(query: QueryInterface) {
  await query.removeColumn('importer_reports','updated_items');
}