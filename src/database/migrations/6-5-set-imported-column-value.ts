import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  const result = await query.bulkUpdate('imported_items', {imported: true}, {
    imported: null
  });
}

export async function down(query: QueryInterface) {
  const stop = null;
}