import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable("notes", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
        },
        body: {
            type: DataType.TEXT,
            allowNull: false,
        },
        notable_id: {
			type: DataType.UUID,
			allowNull: false,
        },
        notable_type: {
            type: DataType.STRING,
            allowNull: false,
        },
		deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("notes");
}