import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  await query.createTable('importer_failed_items', {
    id: {
      type: sequelize.UUID,
      allowNull: false,
      primaryKey: true
    },
    importer_report_id: {
      type: DataType.UUID,
      allowNull: false,
      references: {
        model: 'importer_reports',
        key: 'id',
      },
      onDelete: "CASCADE",
    },
    remote_id: {
      type: DataType.STRING,
      allowNull: true
    },
    path: {
      type: DataType.STRING,
      allowNull: false
    },
    message: {
      type: DataType.STRING,
      allowNull: false
    },
    error: {
      type: DataType.TEXT,
      allowNull: false
    },

  });
}

export async function down(query: QueryInterface) {
  await query.dropTable('importer_failed_items');
}