import { Op } from 'Sequelize';
import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
import { ImportedItem } from '../../lib/database/importers/interfaces/ImportedItem';
import { Kernel } from '../../lib/kernel/kernel';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import List from '../../models/List';
declare var kernel: Kernel;
export async function up(query: QueryInterface, sequelize: any) {
  await query.addColumn('imported_items', 'meta', {
    type: DataType.JSON,
    allowNull: true,
  });
  // set meta value:
  // 1. get the imported general lists:
  const repository = <BaseRepository<List>> kernel.getContainer().get('ListRepository');
  const generalLists = await repository.findAll({where: {listType: "general"}});
  const ids = [];
  for(let item of generalLists) {
    ids.push(item.id);
  }
  const imported_items_repo = <BaseRepository<ImportedItem>> kernel.getContainer().get('ImportedItemRepository');
  const meta = {
    listType: "general",
  };
  // 2. set the meta value:
  const result = await imported_items_repo.update({meta: meta}, {
    where: {
      item_id: {
        [Op.in]: ids,
      }
    }
  });
}

export async function down(query: QueryInterface) {
  await query.removeColumn('imported_items', 'meta');
}