import { QueryInterface } from 'sequelize';
import { DataType, AllowNull } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  await query.createTable('users', {
    id: {
      type: sequelize.UUID,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataType.STRING,
      allowNull: true,
    },
    first_name: {
      type: DataType.STRING,
      allowNull: true,
    },
    last_name: {
      type: DataType.STRING,
      allowNull: true,
    },
    email: {
      type: DataType.STRING,
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataType.TEXT,
      allowNull: false,
    },
    cash_advisor: {
      type: DataType.BOOLEAN,
    },
    active: {
      type: DataType.BOOLEAN,
    },
    created_at: {
      allowNull: false,
      type: DataType.DATE
    },
    updated_at: {
      allowNull: false,
      type: DataType.DATE
    },
    deleted_at: {
      allowNull: true,
      type: DataType.DATE
    },

  });
  await query.addIndex('users',['email'],{
    name: "email",
    type: "UNIQUE",
  });
  await query.addIndex('users',['username'],{
    name: "username",
    type: "FULLTEXT",
  });

}

export async function down(query: QueryInterface) {
  await query.dropTable('users');
}