import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable("emails", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		person_id: {
			type: DataType.UUID,
			allowNull: true,
			references: {
				model: "people",
				key: "id",
			},
		},
        address: {
			type: DataType.STRING,
			allowNull: true,
		},
        primary: {
			type: DataType.BOOLEAN,
			allowNull: true,
        },
        unsubscribe: {
			type: DataType.BOOLEAN,
			allowNull: true,
		},
        cc_transaction_report: {
			type: DataType.BOOLEAN,
			allowNull: true,
		},
        type: {
			type: DataType.STRING,
			allowNull: true,
        },
        created_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		updated_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE,
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("emails");
}