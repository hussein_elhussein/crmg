import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	/*
      Add altering commands here.

      Example:
      await query.createTable('users', { id: DataType.INTEGER });
      console.log('Table users created!');
    */
	await query.createTable('tagged', {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		tag_id: {
			type: DataType.UUID,
			allowNull: true,
			references: {
				model: 'tags',
				key: 'id',
			},
			onDelete: "CASCADE",
		},
		taggable_id: {
			type: DataType.UUID,
			allowNull: false,
		},
		taggable_type: {
			type: DataType.STRING,
			allowNull: false,
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable('tagged');
}