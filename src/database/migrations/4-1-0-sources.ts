import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable("sources", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
        },
        name: {
            type: DataType.STRING,
            allowNull: false,
        },
		deleted_at: {
            allowNull: true,
            type: DataType.DATE
        },
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("sources");
}