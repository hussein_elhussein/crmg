import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("list_filters", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        list_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: "lists",
                key: "id",
            },
        },
        type: {
            type: DataType.STRING,
            allowNull: true,
        },
        property: {
            type: DataType.STRING,
            allowNull: true,
        },
        operator: {
            type: DataType.STRING,
            allowNull: false,
        },
        values: {
            type: DataType.TEXT({length: 'long'}),
            allowNull: true,
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });
}

export async function down(query: QueryInterface) {
    await query.dropTable("list_filters");
}