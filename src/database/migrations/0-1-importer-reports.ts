import { QueryInterface } from 'sequelize';
import { DataType } from 'sequelize-typescript';
export async function up(query: QueryInterface, sequelize: any) {
  await query.createTable('importer_reports', {
    id: {
      type: sequelize.UUID,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataType.STRING,
      allowNull: false,
    },
    imported_items: {
      type: DataType.INTEGER,
      allowNull: true,
    },
    total: {
      type: DataType.INTEGER,
      allowNull: true,
    },
    completed: {
      type: DataType.BOOLEAN,
      allowNull: false,
    }
  });
}

export async function down(query: QueryInterface) {
  await query.dropTable('importer_reports');
}