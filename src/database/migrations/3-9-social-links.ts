import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface, sequelize: any) {
	await query.createTable("social_links", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		social_linkable_id: {
			type: DataType.UUID,
            allowNull: false,
		},
		social_linkable_type: {
			type: DataType.STRING,
			allowNull: false,
		},
		social_link_type: {
			type: DataType.STRING,
			allowNull: true,
		},
		url: {
			type: DataType.STRING,
			allowNull: false,
		},
		created_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		updated_at: {
			allowNull: false,
			type: DataType.DATE,
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE,
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("social_links");
}