import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	/*
      Add altering commands here.

      Example:
      await query.createTable('users', { id: DataType.INTEGER });
      console.log('Table users created!');
    */
	await query.createTable('tags', {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
		color_hex: {
			type: DataType.STRING,
			allowNull: true,
		},
		name: {
			type: DataType.STRING,
			allowNull: false,
		},
		created_at: {
			allowNull: false,
			type: DataType.DATE
		},
		updated_at: {
			allowNull: false,
			type: DataType.DATE
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE
		},
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable('tags');
}