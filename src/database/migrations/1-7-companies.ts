import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface, sequelize: any) {
	/*
      Add altering commands here.

      Example:
      await query.createTable('users', { id: DataType.INTEGER });
      console.log('Table users created!');
    */
	await query.createTable('companies', {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true
		},
		name: {
			type: DataType.STRING,
			allowNull: false,
		},
		website: {
			type: DataType.STRING,
			allowNull: true,
		},
		business_type_id: {
			type: DataType.UUID,
			allowNull: true,
			references: {
				model: 'business_types',
				key: 'id',
			},
			onDelete: "SET NULL",
		},

		location_type_id: {
			type: DataType.UUID,
			allowNull: true,
			references: {
				model: 'location_types',
				key: 'id',
			},
			onDelete: "SET NULL",
		},
		annual_revenue: {
			type: DataType.FLOAT,
			allowNull: true,
		},
		account_opened_date: {
			type: DataType.DATE,
			allowNull: true,
		},
		number_of_employees: {
			type: DataType.INTEGER,
			allowNull: true,
		},

		parent_company_id: {
			type: DataType.UUID,
			allowNull: true,
			references: {
				model: 'companies',
				key: 'id',
			},
			onDelete: "SET NULL",
		},

		primary_contact_id: {
			type: DataType.UUID,
			allowNull: true,
			references: {
				model: 'people',
				key: 'id',
			},
			onDelete: "SET NULL",
		},

		created_at: {
			allowNull: false,
			type: DataType.DATE
		},
		updated_at: {
			allowNull: false,
			type: DataType.DATE
		},
		deleted_at: {
			allowNull: true,
			type: DataType.DATE
		},

	});

}

export async function down(query: QueryInterface) {
	await query.dropTable('companies');
}