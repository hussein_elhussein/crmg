import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
	await query.createTable("employees", {
		id: {
			type: DataType.UUID,
			allowNull: false,
			primaryKey: true,
		},
        person_id: {
			type: DataType.UUID,
			allowNull: false,
			references: {
				model: "people",
				key: "id",
			},
        },
        company_id: {
			type: DataType.UUID,
			allowNull: false,
			references: {
				model: "companies",
				key: "id",
			},
        },
		deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
	});

}

export async function down(query: QueryInterface) {
	await query.dropTable("employees");
}