import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("account_sales_code", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        account_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: "accounts",
                key: "id",
            },
        },
        sales_code_id: {
            type: DataType.UUID,
            allowNull: false,
            references: {
                model: "sales_codes",
                key: "id",
            },
        },
    });
  
}

export async function down(query: QueryInterface) {
    await query.dropTable("account_sales_code");
}