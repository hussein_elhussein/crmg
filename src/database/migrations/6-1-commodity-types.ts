import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("commodity_types", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        name: {
            allowNull: false,
            type: DataType.STRING,
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });

}

export async function down(query: QueryInterface) {
    await query.dropTable("commodity_types");
}