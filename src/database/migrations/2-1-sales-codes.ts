import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable('sales_codes', {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        name: {
            type: DataType.STRING,
            allowNull: false,
        },

        created_at: {
            allowNull: false,
            type: DataType.DATE
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE
        },
    });

}

export async function down(query: QueryInterface) {
    await query.dropTable('sales_codes');
}