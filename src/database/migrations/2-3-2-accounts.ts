import { QueryInterface } from 'sequelize';
import {DataType} from 'sequelize-typescript';

export async function up(query: QueryInterface) {
    await query.createTable("accounts", {
        id: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        accountable_id: {
            type: DataType.UUID,
            allowNull: false,
        },
        accountable_type: {
            type: DataType.STRING,
            allowNull: false,
        },
        name: {
            type: DataType.STRING,
            allowNull: true,
        },
        transaction_note: {
            type: DataType.TEXT,
            allowNull: true,
        },
        contact_rate: {
            type: DataType.FLOAT,
            allowNull: true,
        },
        broker_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "brokers",
                key: "id"
            }
        },
        referral_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "referrals",
                key: "id"
            }
        },
        securing_agent_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "securing_agents",
                key: "id"
            }
        },
        insurance_provider_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "insurance_providers",
                key: "id"
            }
        },
        milk_handler_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "milk_handlers",
                key: "id"
            }
        },
        account_type_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "account_types",
                key: "id"
            }
        },
        margin_push_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "margin_pushes",
                key: "id"
            }
        },
        company_type_id: {
            type: DataType.UUID,
            allowNull: true,
            references: {
                model: "company_types",
                key: "id"
            }
        },
        has_security_agreement: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        security_agreement: {
            type: DataType.TEXT({length: "long"}),
            allowNull: true,
        },
        hedge: {
            type: DataType.BOOLEAN,
            allowNull: true,
        },
        created_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        updated_at: {
            allowNull: false,
            type: DataType.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: DataType.DATE,
        },
    });
  
}

export async function down(query: QueryInterface) {
    await query.dropTable("accounts");
}