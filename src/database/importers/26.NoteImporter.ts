import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import Note from '../../models/Note';

@injectable()
export class NoteImporter extends BaseImporter<Note>{

  shouldRun(): boolean {
    return false;
  }

  hasData(): boolean {
    return true;
  }

  getOrder(): number {
    return 26;
  }

  protected getRequiredProperties(): string[] {
    return ['body', 'notableId', 'notableType'];
  }

  getPath(): string {
    return '/note';
  }

  getMapping(): any{
    return {
      body: "body",
      notableId: "notableId",
      notableType: "notableType",
    };
  }

  public async save(data: IDeSerialized, existing?:Note):Promise<Note>{
    const values = this.getValues(data);
    let note: Note;
    if (existing) {
      note = existing;
    }
    else {
      note = new Note();
    }
    for(let attr of Object.keys(values)){
      (note as any).set(attr, values[attr]);
    }
    await note.save();
    return note;
  }
}