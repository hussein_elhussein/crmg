import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import Email from '../../models/Email';

@injectable()
export class EmailImporter extends BaseImporter<Email>{

  shouldRun(): boolean {
    return false;
  }

  hasData(): boolean {
    return true;
  }

  getOrder(): number {
    return 19;
  }

  protected getRequiredProperties(): string[] {
    return ['address', 'personId'];
  }

  getPath(): string {
    return '/email';
  }

  getMapping(): any{
    return {
      address: "email",
      type: "type",
      unsubscribe: "unsubscribe",
      primary: "primary",
      ccTransactionReport: "cc_transaction_report",
      personId: "personId",
    };
  }

  valueFor(property: string, value: any): any {
    if (!value) {
      return super.valueFor(property, value);
    }
    if (property === 'primary' || property === 'ccTransactionReport') {
      if (Array.isArray(value)) {
        return value[0];
      }
    }
    if (property === 'address') {
      const v = value.match(/(\w|\d)+@(\w|\d)+\.(\w|\d)+/);
      if (v && v.length) {
        return v[0];
      }
      return null;
    }
    return super.valueFor(property, value);
  }

  public async save(data: IDeSerialized, existing?: Email):Promise<Email>{
    const values = this.getValues(data);
    values.address = values.address.trim();
    let email: Email;
    if (existing) {
      email = existing;
    }
    else {
      email = new Email();
    }
    for(let attr of Object.keys(values)){
      (email as any).set(attr, values[attr]);
    }
    let err = null;
    await email.save().catch(e => err = e);
    if(err){
      throw err;
    }
    return email;
  }
}