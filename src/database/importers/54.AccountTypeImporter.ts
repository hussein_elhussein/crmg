import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import AccountType from '../../models/AccountType';

@injectable()
export class AccountTypeImporter extends BaseImporter<AccountType>{
  validateInstance = false;
  getOrder(): number {
    return 17;
  }

  getIdProperty(): string {
    return 'label';
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/account_types';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}