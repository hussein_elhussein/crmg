import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import EventsActivity from '../../models/EventsActivity';

@injectable()
export class EventsActivityImporter extends BaseImporter<EventsActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 30;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/events_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      sendRemind: "send_remind",
      sendBefore: "send_before",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:EventsActivity, params?: any):Promise<EventsActivity>{
    data.created = this.parseDate(data.created);
    const values = this.getValues(data);
    let eventsActivity: EventsActivity;
    if (existing) {
      eventsActivity = existing;
    }
    else {
      eventsActivity = new EventsActivity();
    }
    for(let attr of Object.keys(values)){
      (eventsActivity as any).set(attr, values[attr]);
    }
    eventsActivity.type = 'EventsActivity';
    if(params){
      eventsActivity.activitiableId = params['activitiableId'];
      eventsActivity.activitiableType = params['activitiableType'];
    }
    let err = null;
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, eventsActivity, 'user');
    }
    const options: SaveOptions = {validate: false};
    await eventsActivity.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return eventsActivity;
  }
}