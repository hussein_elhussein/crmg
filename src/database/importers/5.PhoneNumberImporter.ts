import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import PhoneNumber from '../../models/PhoneNumber';

@injectable()
export class PhoneNumberImporter extends BaseImporter<PhoneNumber>{

  shouldRun(): boolean {
    return false;
  }

  hasData(): boolean {
    return true;
  }

  getOrder(): number {
    return 5;
  }

  protected getRequiredProperties(): string[] {
    return ['number', 'phoneNumerableId', 'phoneNumerableType'];
  }

  getPath(): string {
    return '/phone_number';
  }

  getMapping(): any{
    return {
      number: "phone",
      type: "type",
      phoneNumerableId: "phoneNumerableId",
      phoneNumerableType: "phoneNumerableType",
    };
  }

  public async save(data: IDeSerialized, existing?:PhoneNumber):Promise<PhoneNumber>{
    const values = this.getValues(data);
    let phoneNumber: PhoneNumber;
    if (existing) {
      phoneNumber = existing;
    }
    else {
      phoneNumber = new PhoneNumber();
    }
    for(let attr of Object.keys(values)){
      (phoneNumber as any).set(attr, values[attr]);
    }
    let err = null;
    await phoneNumber.save().catch(e => err = e);
    if(err){
      throw err;
    }
    return phoneNumber;
  }

}