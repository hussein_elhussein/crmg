import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import OfficeCode from '../../models/OfficeCode';

@injectable()
export class OfficeCodeImporter extends BaseImporter<OfficeCode>{
  getOrder(): number {
    return 11;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/office_code';
  }

  getMapping(): any{
    return {
      name: 'label',
    };
  }
}