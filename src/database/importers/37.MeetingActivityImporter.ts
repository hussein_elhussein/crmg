import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import MeetingActivity from '../../models/MeetingActivity';

@injectable()
export class MeetingActivityImporter extends BaseImporter<MeetingActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 37;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/meeting_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      sendeRemind: "send_remind",
      sendBefore: "send_before",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:MeetingActivity, params?: any):Promise<MeetingActivity>{
    data.created = this.parseDate(data.created);
    const values = this.getValues(data);
    let meetingActivity: MeetingActivity;
    if (existing) {
      meetingActivity = existing;
    }
    else {
      meetingActivity = new MeetingActivity();
    }
    for(let attr of Object.keys(values)){
      (meetingActivity as any).set(attr, values[attr]);
    }
    meetingActivity.type = 'MeetingActivity';
    if(params){
      meetingActivity.activitiableId = params['activitiableId'];
      meetingActivity.activitiableType = params['activitiableType'];
    }
    let err = null;
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, meetingActivity, 'user');
    }
    const options: SaveOptions = {validate: false};
    await meetingActivity.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return meetingActivity;
  }
}