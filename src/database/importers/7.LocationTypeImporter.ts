import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import LocationType from '../../models/LocationType';

@injectable()
export class LocationTypeImporter extends BaseImporter<LocationType>{
  getOrder(): number {
    return 7;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/location_type';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }
}