import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import EmailActivity from '../../models/EmailActivity';

@injectable()
export class EmailActivityImporter extends BaseImporter<EmailActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 36;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/email_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      sendeRemind: "send_remind",
      sendBefore: "send_before",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:EmailActivity, params?: any):Promise<EmailActivity>{
    const values = this.getValues(data);
    values.createdAt = this.parseDate(values.createdAt);
    let emailActivity: EmailActivity;
    if (existing) {
      emailActivity = existing;
    }
    else {
      emailActivity = new EmailActivity();
    }
    for(let attr of Object.keys(values)){
      (emailActivity as any).set(attr, values[attr]);
    }
    emailActivity.type = "EmailActivity";
    if(params){
      emailActivity.activitiableId = params['activitiableId'];
      emailActivity.activitiableType = params['activitiableType'];
    }
    if(data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, emailActivity, 'user');
    }
    let err = null;
    const options: SaveOptions = {validate: false};
    await emailActivity.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return emailActivity;
  }
}