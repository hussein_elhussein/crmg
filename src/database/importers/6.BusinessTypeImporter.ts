import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import BusinessType from '../../models/BusinessType';

@injectable()
export class BusinessTypeImporter extends BaseImporter<BusinessType>{
  getOrder(): number {
    return 6;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/business_type';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}