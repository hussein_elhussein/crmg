import { inject, injectable, named } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { UserImporter } from './2.UserImporter';
import { ListFilterImporter } from './22.ListFilterImporter';
import List from '../../models/List';
import Company from '../../models/Company';
import Person from '../../models/Person';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import ListContact from '../../models/ListContact';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import { PersonImporter } from './10.PersonImporter';
import { CompanyImporter } from './9.CompanyImporter';
import { ListHelper } from './helpers/ListHelper';

@injectable()
export class MailChimpListImporter extends BaseImporter<List>{
  constructor(@inject(UserImporter) protected user_importer: UserImporter,
              @inject(ListFilterImporter) protected list_filter_importer: ListFilterImporter,
              @inject(PersonImporter) protected person_importer: PersonImporter,
              @inject(CompanyImporter) protected company_importer: CompanyImporter,
              @inject('Repository') @named('ListContact') protected list_contact_repo: BaseRepository<ListContact>,
              @inject('Repository') @named('Company') protected company_repo: BaseRepository<Company>,
              @inject('Repository') @named('Person') protected person_repo: BaseRepository<Person>,
              @inject('Repository') @named('List') protected list_repo: BaseRepository<List>){
      super();
  }

  async init(withModel: boolean = true): Promise<any> {
    await super.init(false);
    this.model = List;
  }

  getOrder(): number {
    return 23;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  async countImported(): Promise<number | null> {
    const count = await this.list_repo.count({
      where: {
        list_type: "mailchimp",
      }
    });
    return count;
  }

  getPath(): string {
    return '/mailchimp_list';
  }

  getMapping(): any{
    return {
      name: "label",
      contactType: "type",
      exclude: "exclude",
      searchNew: "search_new",
      sync: "sync",
      mailchimpId: "mailchimp_list_id",
      updateFilter: "filter.update_filter",
      countFilter: "filter.count_filter",
      removeFilter: "filter.remove_filter",
    };
  }

  public async save(data: IDeSerialized, existing?: List, params?: any):Promise<List>{
    const values = this.getValues(data);
    if (values.hasOwnProperty('exclude')) {
      values['exclude'] = values['exclude'] === "1";
    }

    if (values.hasOwnProperty('searchNew')) {
      values['searchNew'] = values['searchNew'] === "1";
    }
    if (values.hasOwnProperty('sync')) {
      values['sync'] = values['sync'] === "1";
    }
    values.contactType = values.contactType.charAt(0).toUpperCase() + values.contactType.slice(1);
    let list: List;
    if (existing) {
      list = existing;
    }
    else {
      list = new List();
    }
    for(let attr of Object.keys(values)){
      (list as any).set(attr, values[attr]);
    }
    list.listType = "mailchimp";
    const helper = new ListHelper();
    await this.preSave(data, list, params);
    await helper.preSaveImportRelations(this, data, list);
    const createdList = await list.save();
    await this.postSave(data, list, params);
    await helper.postSaveImportRelations(this, data, createdList);
    return list;
  }
}