import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import FileType from '../../models/FileType';

@injectable()
export class FileTypeImporter extends BaseImporter<FileType>{
  getOrder(): number {
    return 57;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/storage_file_types';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}