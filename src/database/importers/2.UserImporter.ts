import { inject, injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import User from '../../models/User';
import UserRole from '../../models/UserRole';
import { RoleRepository } from '../../repositories/RoleRepository';
import Role from '../../models/Role';
import { RoleImporter } from './1.RoleImporter';
import { PersonTypeImporter } from './3.PersonTypeImporter';

@injectable()
export class UserImporter extends BaseImporter<User>{
  role_repo: RoleRepository;
  constructor(@inject(RoleRepository) role_repo: RoleRepository,
              @inject(RoleImporter) protected roleImporter: RoleImporter){
    super();
    this.role_repo = role_repo;
  }
  getOrder(): number {
    return 2;
  }

  getAdditionalIds(): string[] {
    return ['username','email'];
  }

  protected getRequiredProperties(): string[] {
    return ['username','email','active'];
  }

  getPath(): string {
    return '/users';
  }

  getMapping(): any{
    return {
      username: "username",
      firstName: "first_name",
      lastName: "last_name",
      active: "active",
      email: "email",
      roles: "roles",
    };
  }

  async save(data: any, existing?: User): Promise<User> {
    const values = this.getValues(data);
    const roles = values['roles'];
    values['active'] = parseInt(values['active']);
    let instance: User;
    if (existing) {
      instance = existing;
    }
    else {
      instance = new this.model(values);
    }
    if(!existing){
      instance.password = "test";
    }
    let err = null;
    const user = await instance.save({validate: false}).catch(e => err = e);
    if(err){
      throw err;
    }
    for(let id of roles){
      const role = <Role> await this.roleImporter.findImportedItem(id, data, Role).catch(e => err = e);
      if(err){
        throw err;
      }
      if(!role){
        continue;
      }
      const user_role = new UserRole();
      user_role.roleId = role.id;
      user_role.userId = user.id;
      await user_role.save().catch(e => err = e);
      if(err){
        throw err;
      }
    }
    return user;
  }

}