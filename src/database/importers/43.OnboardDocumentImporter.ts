import { injectable, inject } from 'inversify';
import { SaveOptions } from 'sequelize';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized, IRelationIdB } from '../../lib/serializer/interfaces/IDeSerialized';
import { ImporterManager } from '../../lib/database/importers/ImporterManager';
import { PersonImporter } from './10.PersonImporter';
import OnboardDocument from '../../models/OnboardDocument';
import Person from '../../models/Person';
@injectable()
export class OnboardDocumentImporter extends BaseImporter<OnboardDocument>{
    person_importer: PersonImporter;
    constructor(@inject(PersonImporter) person_importer: PersonImporter){
      super();
      this.person_importer = person_importer;
    }
 
  getOrder(): number {
    return 43;
  }

  protected getRequiredProperties(): string[] {
    return [];
  }

  getPath(): string {
    return '/contact_document_base';
  }

  getMapping(): any{
    return {
      powerOfAttorney: 'power_of_attorney',
      straitsAccountPaperwork: 'straits_account_paperwork',
      achAuthorizationForm: 'ach_authorization_form',
      additionalRiskDisclosure: 'additional_risk_disclosure',
      partnershipAgreement: 'partnership_agreement',
      byLaws: 'by_laws',
      driversLicense: 'drivers_license',
      balanceSheet: 'balance_sheet',
      articlesOfOrganization: 'articles_of_organization',
      rpmPaperwork: 'rpm_paperwork',
      operatingAgreement: 'operating_agreement',
      informationSheet: 'crmg_information_sheet',
      tradingExperience: 'trading_experience',
      accountType: 'account_type',
      statusNotes: 'status_notes',
      lastUpdateTime: 'last_update_time',
      tradingAccountid: 'base_trading_accountm',
    };
  }

  async save(data: IDeSerialized, existing?:OnboardDocument):Promise<OnboardDocument>{
    const values = this.getValues(data);
    values.lastUpdateTime = this.parseDate(values.lastUpdateTime);
    let onboardDoc: OnboardDocument;
    if (existing) {
      onboardDoc = existing;
    }
    else {
      onboardDoc = new OnboardDocument();
    }
    for(let attr of Object.keys(values)){
      (onboardDoc as any).set(attr, values[attr]);
    }
    let err = null;
    if (data.relationships && data.relationships.contact_lead) {
        const relInfo = <IRelationIdB> data.relationships.contact_lead.data;
        await this.importRelation(relInfo, this.person_importer, onboardDoc, 'person');
    }
    if (data.upload_completed) onboardDoc.status = 1;
    if (data.is_hide) onboardDoc.status = 2;
    else onboardDoc.status = 0;
    const options: SaveOptions = {validate: false};

    await onboardDoc.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return onboardDoc;
  }
}