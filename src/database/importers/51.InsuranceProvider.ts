import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import InsuranceProvider from '../../models/InsuranceProvider';

@injectable()
export class InsuranceProviderImporter extends BaseImporter<InsuranceProvider>{
  getOrder(): number {
    return 17;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/insurance_providers';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}