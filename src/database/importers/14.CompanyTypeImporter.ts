import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import CompanyType from '../../models/CompanyType';

@injectable()
export class CompanyTypeImporter extends BaseImporter<CompanyType>{
  getOrder(): number {
    return 14;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/company_types';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }
}