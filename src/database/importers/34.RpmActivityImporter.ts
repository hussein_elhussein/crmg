import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import RpmActivity from '../../models/RpmActivity';

@injectable()
export class RpmActivityImporter extends BaseImporter<RpmActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 34;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/rpm_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      rpmYear: "rpm_year",
      rpmAccountNumber: "rpm_account_number",
      rpmTab: "rpm_tab",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:RpmActivity, params?:any):Promise<RpmActivity>{
    const values = this.getValues(data);
    values.createdAt = this.parseDate(values.createdAt);
    let rpmActivity: RpmActivity;
    if (existing) {
      rpmActivity = existing;
    }
    else {
      rpmActivity = new RpmActivity();
    }
    for(let attr of Object.keys(values)){
      (rpmActivity as any).set(attr, values[attr]);
    }
    rpmActivity.type = "RpmActivtiy";
    if(params){
      rpmActivity.activitiableId = params['activitiableId'];
      rpmActivity.activitiableType = params['activitiableType'];
    }
    let err = null;
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, rpmActivity, 'user');
    }
    const options: SaveOptions = {validate: false};
    await rpmActivity.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return rpmActivity;
  }
}