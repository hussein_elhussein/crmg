import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import DeviceActivity from '../../models/DeviceActivity';

@injectable()
export class DeviceActivityImporter extends BaseImporter<DeviceActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 33;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/device_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      device: "device",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:DeviceActivity, params?: any):Promise<DeviceActivity>{
    const values = this.getValues(data);
    values.createdAt = this.parseDate(values.createdAt);
    let deviceActivity: DeviceActivity;
    if (existing) {
      deviceActivity = existing;
    }
    else {
      deviceActivity = new DeviceActivity();
    }
    for(let attr of Object.keys(values)){
      (deviceActivity as any).set(attr, values[attr]);
    }
    deviceActivity.type = 'DeviceActivity';
    if(params){
      deviceActivity.activitiableId = params['activitiableId'];
      deviceActivity.activitiableType = params['activitiableType'];
    }
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, deviceActivity, 'user');
    }
    await deviceActivity.save();
    return deviceActivity;
  }
}