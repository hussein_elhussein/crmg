import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import SecuringAgent from '../../models/SecuringAgent';

@injectable()
export class SecuringAgentImporter extends BaseImporter<SecuringAgent>{
  getOrder(): number {
    return 17;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/securing_agents';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}