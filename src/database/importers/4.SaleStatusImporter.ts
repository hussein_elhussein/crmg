import { inject, injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import SaleStatus from '../../models/SaleStatus';

@injectable()
export class SaleStatusImporter extends BaseImporter<SaleStatus>{

  getOrder(): number {
    return 4;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/sale_status';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}