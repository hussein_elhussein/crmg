import { inject, injectable, named } from 'inversify';
import { Op } from 'Sequelize';
import { FindOptions, WhereOptions } from 'sequelize';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized, IRelationIdB } from '../../lib/serializer/interfaces/IDeSerialized';
import { ImporterManager } from '../../lib/database/importers/ImporterManager';
import Email from '../../models/Email';
import { SocialLinkImporter } from './28.SocialLinkImporter';
import { EmailImporter } from './19.EmailImporter';
import { NoteImporter } from './26.NoteImporter';
import { PhoneNumberImporter } from './5.PhoneNumberImporter';
import { SaleStatusImporter } from './4.SaleStatusImporter';
import { UserImporter } from './2.UserImporter';
import { AccountImporter } from './15.AccountImporter';
import { AddressImporter } from './16.AddressImporter';
import { BusinessTypeImporter } from './6.BusinessTypeImporter';
import { PersonTypeImporter } from './3.PersonTypeImporter';
import { LeadSourceImporter } from './8.LeadSourceImporter';
import { CompanyImporter } from './9.CompanyImporter';
import { RelatedPersonImporter } from './27.RelatedPersonImporter';
import Person from '../../models/Person';
import Tag from '../../models/Tag';
import { Kernel } from '../../lib/kernel/kernel';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import { ActivitiesImporter } from './47.ActivitiesImporter';
import { TagImporter } from './17.TagImporter';
declare var kernel: Kernel;

@injectable()
export class PersonImporter extends BaseImporter<Person>{
  constructor(@inject(PersonTypeImporter) protected person_type_importer: PersonTypeImporter,
              @inject(SaleStatusImporter) protected sale_status_importer: SaleStatusImporter,
              @inject(BusinessTypeImporter) protected business_type_importer: BusinessTypeImporter,
              @inject(UserImporter) protected user_importer: UserImporter,
              @inject(LeadSourceImporter) protected lead_source_importer: LeadSourceImporter,
              @inject(AccountImporter) protected account_importer: AccountImporter,
              @inject(AddressImporter) protected address_importer: AddressImporter,
              @inject(SocialLinkImporter) protected social_link_importer: SocialLinkImporter,
              @inject(EmailImporter) protected email_importer: EmailImporter,
              @inject(NoteImporter) protected note_importer: NoteImporter,
              @inject(PhoneNumberImporter) protected phone_number_importer: PhoneNumberImporter,
              @inject(RelatedPersonImporter) protected related_person_importer: RelatedPersonImporter,
              @inject(ActivitiesImporter) protected activitiesImporter: ActivitiesImporter,
              @inject(TagImporter) protected tagImporter: TagImporter,
              @inject('Repository') @named('Tag') protected tag_repo: BaseRepository<Tag>,
              ){
    super();
  }

  getOrder(): number {
    return 10;
  }
  getAdditionalIds(): string[] {
    return ['firstName', 'lastName'];
  }

  loadQueryOptions(data: IDeSerialized): FindOptions | null {
    if(!Object.keys(data).includes('label')){
      return null;
    }
    const values = data['label'].split(' ');
    const options = <FindOptions> {
      where: {
        [Op.and]: {
          firstName: values[0],
          lastName: values[1],
        }
      }
    }
    return options;
  }

  protected getRequiredProperties(): string[] {
    return ['firstName','lastName'];
  }

  getPath(): string {
    return '/person';
  }

  getMapping(): any{
    return {
      notes: 'notes',
      prefix: 'contact.name.safe.title',
      firstName: 'contact.name.safe.given',
      lastName: 'contact.name.safe.family',
      middleName: 'contact.name.safe.middle',
      suffix: 'contact.name.safe.generational',
      credentials: 'contact.name.safe.credentials',
      accountOpenedDate: 'contact.account_opened_date',
      statementType: 'contact.statement_type',
      doNotCall: 'contact.do_not_call',
      powerOfAttorney: 'contact.powerOfAttorney',
      birthDate: 'contact.birth_date',
      gender: 'contact.gender',
      website: 'contact.website',
      jobTitle: 'contact.job_title',
    };
  }

  async save(data: IDeSerialized, existing?: Person, params?: any): Promise<Person> {
    const values = this.getValues(data);
    values.accountOpenedDate = this.parseDate(values.accountOpenedDate);
    values.birthDate = this.parseDate(values.birthDate);
    if (values.gender?.length) {
      values.gender = values.gender[0].toLowerCase() === 'm' ? "Male" : "Female";
    }
    if (values.website && typeof values.website === 'object') {
      values.website = values.website.url;
    }

    let person: Person;
    if (existing) {
      person = existing;
    }
    else {
      person = new Person();
    }
    for (let attr of Object.keys(values)) {
      (person as any).set(attr, values[attr]);
    }
    let err = null;
    await this.preSave(data, person, params).catch(e => err = e);
    if(err){
      throw err;
    }
    const createdPerson = await person.save({validate: false}).catch(e => err = e);
    if(err){
      throw err;
    }
    await this.postSave(data, createdPerson, params).catch(e => err = e);
    if(err){
      throw err;
    }
    return createdPerson;
  }

  async preSaveImportRelations(data:IDeSerialized, instance: Person, params?: any){
    if (data.contact.type) {
      const relData = {
        id: data.contact.type,
        type: data.contact.type,
      };
      const personType = await this.toRelation(relData, this.person_type_importer, null, {}, false, false, params);
      if (personType) {
        await instance.$set('personType', personType[0]);
      }
    }
    if (data.contact.sales_status) {
      const relData = { id: data.contact.sales_status };
      await this.importRelation(relData, this.sale_status_importer, instance, 'saleStatus', false, params);
    }
    if (data.contact.business_type) {
      const relData = { id: data.contact.business_type };
      await this.importRelation(relData, this.business_type_importer, instance, 'businessType', false, params);
    }
    if (data.lead_source && !Array.isArray(data.lead_source)) {
      const relData = { id: data.lead_source.id };
      await this.importRelation(relData, this.lead_source_importer, instance, 'leadSource', false, params);
    }
    if(data.relationships){
      if(data.relationships.user) {
        const relInfo = <IRelationIdB> data.relationships.user.data;
        await this.importRelation(relInfo, this.user_importer, instance, 'owner', false, params);
      }
    }
  }

  async postSaveImportRelations(data:IDeSerialized, instance: Person, params?: any){
    if (data.relationships) {
      if (data.relationships.tags) {
        const relInfo:any = data.relationships.tags.data;
        const tags = await this.importRelation(relInfo, this.tagImporter, null, null, true, params);
        if(tags){
          await instance.$set('tags', tags);
        }
      }
      if (data.relationships.labels) {
        const relInfo = <any> data.relationships.labels.data;
        const token = ImporterManager.getToken().access_token;
        for (let label of relInfo) {
          const base_path = kernel.getConfig().import_api;
          const path =  base_path + `/label/` + label.id;
          const labelData = await ImporterManager.getData(path, token, this);
          const labelAttributes = labelData.data.attributes;
          const foundTag = await this.tag_repo.findOne({ where: { name: labelAttributes.label }});
          if (foundTag) {
            let attachedTags = <Tag[]> await instance.$get('tags');
            let tagAttached = false;
            for(let attachedTag of attachedTags){
              if(attachedTag.name === foundTag.name){
                tagAttached = true;
                break;
              }
            }
            if(!tagAttached){
              await instance.$add('tags', foundTag);
            }
          }
          else {
            const tag = new Tag();
            tag.name = labelAttributes.label;
            if (labelAttributes.color) tag.colorHex = labelAttributes.color;
            const result = await this.tag_repo.save(tag);
            if (result){
              await instance.$add('tags', result);
            }
          }
        }
      }
      this.activitiesImporter.setInstance(instance);
      await this.activitiesImporter.save(data);
      if (data.relationships.company) {
        const companies = <IRelationIdB[]>data.relationships.company.data;
        const companyImporter = kernel.getContainer().get(CompanyImporter);
        const relInfo = <IRelationIdB[]> companies;
        await this.importRelation(relInfo, companyImporter, instance, 'company', false, params);
      }
    }
    if (data.account && !Array.isArray(data.account)) {
      const relInfo = {
        accountableId: instance.id,
        accountableType: "Person"
      };
      await this.toRelation(data.account, this.account_importer, instance.id, relInfo, false, true, params);
    }
    if (data.addresses) {
      const relInfo = {
        addressableId: instance.id,
        addressableType: "Person"
      };
      await this.toRelation(data.addresses, this.address_importer, instance.id, relInfo, true, true, params);
    }
    if (data.social_links) {
      const relInfo = {
        socialLinkableId: instance.id,
        socialLinkableType: "Person"
      };
      await this.toRelation(data.social_links, this.social_link_importer, instance.id, relInfo, true, true, params);
    }
    if (data.emails) {
      const relInfo = {
        personId: instance.id,
      };
      await this.toRelation(data.emails, this.email_importer, instance.id, relInfo, true, true, params);
    }
    if (data.phones) {
      const relInfo = {
        phoneNumerableId: instance.id,
        phoneNumerableType: "Person",
      };
      await this.toRelation(data.phones, this.phone_number_importer, instance.id, relInfo, true, true, params);
    }
    if (data.notes) {
      const relInfo = {
        notableId: instance.id,
        notableType: "Person",
      };
      let note = null;
      if(typeof data.notes === "string") {
        note = {
          body: data.notes,
        };
      }
      else {
        note = {
          body: data.notes.value,
        };
      }
      await this.toRelation(note, this.note_importer, instance.id,relInfo, false, true, params);
    }
    await this.importRelatedPeople(data, instance, params);
  }

  protected async importRelatedPeople(data:IDeSerialized, instance: Person, params?: any){
    if (!data.related_people) {
      return;
    }
    let related_people = <IRelationIdB[]> data.related_people;
    if (!related_people.length) {
      return;
    }
    const relInfo = {
      personId: instance.id,
    };
    await this.toRelation(related_people, this.related_person_importer, instance.id, relInfo, true, false, params);
  }
}
