import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import TradingNumber from '../../models/TradingNumber';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';

@injectable()
export class TradingNumberImporter extends BaseImporter<TradingNumber>{

  hasData(): boolean {
    return true;
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 44;
  }

  protected getRequiredProperties(): string[] {
    return ['number','accountId'];
  }

  getPath(): string {
    return '/';
  }

  getMapping(): any{
    return {
      number: "number",
      accountId: "accountId"
    };
  }

  async save(data: IDeSerialized, existing?: TradingNumber | null, params?: any): Promise<TradingNumber> {
    let instance: TradingNumber;
    if(existing){
      instance = existing;
    }else{
      instance = new TradingNumber();
    }
    const values = this.getValues(data);
    for(let attr of Object.keys(values)){
      (instance as any).set(attr, values[attr]);
    }
    await instance.save();
    return instance;
  }

}