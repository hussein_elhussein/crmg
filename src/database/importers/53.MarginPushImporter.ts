import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import MarginPush from '../../models/MarginPush';

@injectable()
export class MarginPushImporter extends BaseImporter<MarginPush>{
  validateInstance = false;
  getOrder(): number {
    return 17;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/margin_pushes';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}