import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import AccountNumber from '../../models/AccountNumber';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';

@injectable()
export class AccountNumberImporter extends BaseImporter<AccountNumber>{

  hasData(): boolean {
    return true;
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 46;
  }

  protected getRequiredProperties(): string[] {
    return ['number','accountId'];
  }

  getPath(): string {
    return '/';
  }

  getMapping(): any{
    return {
      number: "number",
      accountId: "accountId"
    };
  }

  async save(data: IDeSerialized, existing?: AccountNumber | null, params?: any): Promise<AccountNumber> {
    let instance: AccountNumber;
    if(existing){
      instance = existing;
    }else{
      instance = new AccountNumber();
    }
    const values = this.getValues(data);
    for(let attr of Object.keys(values)){
      (instance as any).set(attr, values[attr]);
    }
    await instance.save();
    return instance;
  }

}