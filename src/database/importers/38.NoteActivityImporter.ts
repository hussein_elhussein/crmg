import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import NoteActivity from '../../models/NoteActivity';

@injectable()
export class NoteActivityImporter extends BaseImporter<NoteActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 38;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/note_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      sendeRemind: "send_remind",
      sendBefore: "send_before",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:NoteActivity, params?:any):Promise<NoteActivity>{
    const values = this.getValues(data);
    values.createdAt = this.parseDate(values.createdAt);
    let noteActivity: NoteActivity;
    if (existing) {
      noteActivity = existing;
    }
    else {
      noteActivity = new NoteActivity();
    }
    for(let attr of Object.keys(values)){
      (noteActivity as any).set(attr, values[attr]);
    }
    noteActivity.type = 'NoteActivity';
    if(params){
      noteActivity.activitiableId = params['activitiableId'];
      noteActivity.activitiableType = params['activitiableType'];
    }
    let err = null;
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, noteActivity, 'user');
    }
    const options: SaveOptions = {validate: false};
    await noteActivity.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return noteActivity;
  }
}