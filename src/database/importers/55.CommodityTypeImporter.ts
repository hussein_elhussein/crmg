import { inject, injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import CommodityType from '../../models/CommodityType';

@injectable()
export class CommodityTypeImporter extends BaseImporter<CommodityType>{

  getOrder(): number {
    return 55;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/commodity_types';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}