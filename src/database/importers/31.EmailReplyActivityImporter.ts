import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import EmailReplyActivity from '../../models/EmailReplyActivity';

@injectable()
export class EmailReplyActivityImporter extends BaseImporter<EmailReplyActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 31;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/email_reply_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:EmailReplyActivity, params?: any):Promise<EmailReplyActivity>{
    const values = this.getValues(data);
    values.createdAt = this.parseDate(values.createdAt);
    let emailReplyActivity: EmailReplyActivity;
    if (existing) {
      emailReplyActivity = existing;
    }
    else {
      emailReplyActivity = new EmailReplyActivity();
    }
    for(let attr of Object.keys(values)){
      (emailReplyActivity as any).set(attr, values[attr]);
    }
    emailReplyActivity.type = 'EmailReplyActivity';
    if(params){
      emailReplyActivity.activitiableId = params['activitiableId'];
      emailReplyActivity.activitiableType = params['activitiableType'];
    }
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, emailReplyActivity, 'user');
    }
    await emailReplyActivity.save();
    return emailReplyActivity;
  }
}