import { inject, injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { RelationshipsTypeImporter } from './26.RelationshipsTypeImporter';
import { PersonImporter } from './10.PersonImporter';
import RelatedPerson from '../../models/RelatedPerson';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { Kernel } from '../../lib/kernel/kernel';
declare var kernel: Kernel;
@injectable()
export class RelatedPersonImporter extends BaseImporter<RelatedPerson>{
  relationships_type_importer: RelationshipsTypeImporter;
  person_importer: PersonImporter;
  constructor(@inject(RelationshipsTypeImporter) relationships_type_importer: RelationshipsTypeImporter){
    super();
    this.relationships_type_importer = relationships_type_importer;
  }

  shouldRun(): boolean {
    return false;
  }

  hasData(): boolean {
    return true;
  }

  getOrder(): number {
    return 27;
  }

  protected getRequiredProperties(): string[] {
    return ['personId'];
  }

  getPath(): string {
    return '/related_person';
  }

  getMapping(): any{
    return {
      type: "type.value",
      personId: "personId",
    };
  }

  protected async additionalValidation(data: any, attr: string): Promise<boolean> {
    return super.additionalValidation(data, attr);
  }

  public async save(data: IDeSerialized, existing?: RelatedPerson, params?: any):Promise<RelatedPerson>{
    const values = this.getValues(data);
    let relatedPerson: RelatedPerson;
    if (existing) {
      relatedPerson = existing;
    }
    else {
      relatedPerson = new RelatedPerson();
    }
    for(let attr of Object.keys(values)){
      (relatedPerson as any).set(attr, values[attr]);
    }
    let err = null;
    await this.preSave(data, relatedPerson, params);
    await relatedPerson.save().catch(e => err = e);
    if(err){
      throw err;
    }
    await this.postSave(data, relatedPerson, params)
    return relatedPerson;
  }

  async postSaveImportRelations(data: any, instance: RelatedPerson, params?: any){
    if (data.id) {
      this.person_importer = kernel.getContainer().get(PersonImporter);
      await this.importRelation(data, this.person_importer, instance, 'relative', false, params);
    }
    if (data.type) {
      const relData = {name: data.type.safe_value};
      const relTypes = await this.toRelation(relData, this.relationships_type_importer, instance.id, null, false, true, params);
      await instance.$set('relationshipsType', relTypes[0]);
    }
  }
}