import { inject, injectable, named } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized, IRelationIdB } from '../../lib/serializer/interfaces/IDeSerialized';
import { ImporterManager } from '../../lib/database/importers/ImporterManager';
import { BusinessTypeImporter } from './6.BusinessTypeImporter';
import { LocationTypeImporter } from './7.LocationTypeImporter';
import { PhoneNumberImporter } from './5.PhoneNumberImporter';
import { AddressImporter } from './16.AddressImporter';
import { PersonImporter } from './10.PersonImporter';
import { NoteImporter } from './26.NoteImporter';
import { AccountImporter } from './15.AccountImporter';
import Company from '../../models/Company';
import { Kernel } from '../../lib/kernel/kernel';
import Tag from '../../models/Tag';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import { ActivitiesImporter } from './47.ActivitiesImporter';
import { TagImporter } from './17.TagImporter';
declare var kernel: Kernel;

@injectable()
export class CompanyImporter extends BaseImporter<Company>{
  constructor(@inject(LocationTypeImporter) protected locationTypeImporter:LocationTypeImporter,
              @inject(BusinessTypeImporter) protected businessTypeImporter:BusinessTypeImporter,
              @inject(NoteImporter) protected noteImporter: NoteImporter,
              @inject(PhoneNumberImporter) protected phoneNumberImporter: PhoneNumberImporter,
              @inject(AddressImporter) protected addressImporter: AddressImporter,
              @inject(AccountImporter) protected accountImporter: AccountImporter,
              @inject(ActivitiesImporter) protected activitiesImporter: ActivitiesImporter,
              @inject(TagImporter) protected tagImporter: TagImporter,
              @inject('Repository') @named('Tag') protected tagRepo: BaseRepository<Tag>,
              ){
    super();
  }

  getOrder(): number {
    return 9;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/company';
  }

  getMapping(): any{
    return {
      name: "name",
      website: "website.url",
      annualRevenue: "annual_revenue",
      accountOpenedDate: "account_opened_date",
      numberOfEmployees: "number_of_employees",
      notes: "notes",
    };
  }

  async save(data: IDeSerialized, existing?: Company, params?: any): Promise<Company> {
    const values = this.getValues(data);
    let company: Company;
    if (existing) {
      company = existing;
    }
    else {
      company = new Company();
    }
    for(let attr of Object.keys(values)){
      (company as any).set(attr, values[attr]);
    }
    let err = null;
    await this.preSave(data, company, params).catch(e => err = e);
    if(err){
      throw err;
    }
    const createdCompany = await company.save({validate: false}).catch(e => err = e);
    if(err){
      throw err;
    }
    await this.postSave(data, createdCompany, params).catch(e => err = e);
    if(err){
      throw err;
    }
    return createdCompany;
  }
  async preSaveImportRelations(data: IDeSerialized, instance: Company, params?: any){
    if (data.primaryContact) {
      await instance.$set('primaryContact', data.primaryContact)
    }
    if (!data.relationships) {
      return;
    }
    if (data.relationships.primary_contact) {
      const personImporter = kernel.getContainer().get(PersonImporter);
      const relInfo = <IRelationIdB> data.relationships.primary_contact.data;
      await this.importRelation(relInfo, personImporter, instance, 'primaryContact',false, params);
    }
    if (data.relationships.business_type) {
      const relInfo = <IRelationIdB> data.relationships.business_type.data;
      await this.importRelation(relInfo, this.businessTypeImporter, instance, 'businessType', false, params);
    }
    if (data.relationships.location_type) {
      const relInfo = <IRelationIdB> data.relationships.location_type.data;
      await this.importRelation(relInfo, this.locationTypeImporter, instance, 'locationType', false, params);
    }
    if (data.relationships.parent_company) {
      const relInfo = <IRelationIdB> data.relationships.parent_company.data;
      await this.importRelation(relInfo, this, instance, 'parentCompany', false, params);
    }

  }

  async postSaveImportRelations(data: IDeSerialized, instance: Company, params?: any){
    if (data.relationships) {
      if (data.relationships.tags) {
        const relInfo:any = data.relationships.tags.data;
        const tags = await this.importRelation(relInfo, this.tagImporter, null, null, true, params);
        if(tags){
          await instance.$set('tags', tags);
        }
      }
      if (data.relationships.labels) {
        const relInfo = <any> data.relationships.labels.data;
        const token = ImporterManager.getToken().access_token;
        for (let label of relInfo) {
          const base_path = kernel.getConfig().import_api;
          const path =  base_path + `/label/` + label.id;
          const labelData = await ImporterManager.getData(path, token, this);
          const labelAttributes = labelData.data.attributes;
          const foundTag = await this.tagRepo.findOne({ where: { name: labelAttributes.label }});
          if (foundTag) {
            let attachedTags = <Tag[]> await instance.$get('tags');
            let tagAttached = false;
            for(let attachedTag of attachedTags){
              if(attachedTag.name === foundTag.name){
                tagAttached = true;
                break;
              }
            }
            if(!tagAttached){
              await instance.$add('tags', foundTag);
            }
          }
          else {
            const tag = new Tag();
            tag.name = labelAttributes.label;
            if (labelAttributes.color) tag.colorHex = labelAttributes.color;
            const result = await this.tagRepo.save(tag);
            if (result){
              await instance.$add('tags', result);
            }
          }
        }
      }
    }
    if (data.account_number && Array.isArray(data.account_number) && data.account_number.length) {
      const relInfo = {
        accountableId: instance.id,
        accountableType: "Company"
      };
      await this.toRelation(data.account_number, this.accountImporter, instance.id, relInfo, false, true, params);
    }
    if (data.notes) {
      const relInfo = {
        notableId: instance.id,
        notableType: "Company",
      };
      let note = null;
      if(typeof data.notes === "string") {
        note = {
          body: data.notes,
        };
      }
      else {
        note = {
          body: data.notes.value,
        };
      }
      await this.toRelation(note, this.noteImporter, instance.id,relInfo, false, true, params);
    }
    if (data.phones) {
      for(let phone of data.phones){
        if(!phone.type){
          phone.type = "Work";
        }
      }
      const addAttr = {
        phoneNumerableId: instance.id,
        phoneNumerableType: "Company",
      };
      await this.toRelation(data.phones, this.phoneNumberImporter, instance.id, addAttr, true, true, params);
    }
    if (data.addresses) {
      const addAttr = {
        addressableId: instance.id,
        addressableType: "Company",
      };
      await this.toRelation(data.addresses, this.addressImporter, instance.id, addAttr, true, true, params);
    }
    this.activitiesImporter.setInstance(instance);
    await this.activitiesImporter.save(data);
  }
}
// TODO: 
// Apply mailchimp to list 
// Add to tests Person and Company
// completed_contacts  is relationship to Person, but it's not in the relationships  array
// also on the model side of List, the relationship listContact should be listContacts ,
// and should relate directly to Person, without listContactable
// but you should need a list_contact  table, because the relationship type is Many to Many
// do the same for completed_contacts
// Create migration/importer/model for OnboardDocument
// Crate migration/importer/model for Document
// model attributes need to match like the mapping document person => contactLead
