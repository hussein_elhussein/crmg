import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import SocialLink from '../../models/SocialLink';

@injectable()
export class SocialLinkImporter extends BaseImporter<SocialLink>{

  shouldRun(): boolean {
    return false;
  }

  hasData(): boolean {
    return true;
  }

  getOrder(): number {
    return 28;
  }

  protected getRequiredProperties(): string[] {
    return ['url', 'socialLinkableId', 'socialLinkableType'];
  }

  getPath(): string {
    return '/social_link';
  }

  getMapping(): any{
    return {
      socialLinkType: "service",
      url: "url",
      socialLinkableId: "socialLinkableId",
      socialLinkableType: "socialLinkableType",
    };
  }

  public async save(data: IDeSerialized, existing?:SocialLink):Promise<SocialLink>{
    const values = this.getValues(data);
    let socialLink: SocialLink;
    if (existing) {
      socialLink = existing;
    }
    else {
      socialLink = new SocialLink();
    }
    for(let attr of Object.keys(values)){
      (socialLink as any).set(attr, values[attr]);
    }
    let err = null;
    await socialLink.save({validate: false}).catch(e => err = e);
    if(err){
      throw err;
    }
    return socialLink;
  }

}