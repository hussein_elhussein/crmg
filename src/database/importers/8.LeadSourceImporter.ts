import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import LeadSource from '../../models/LeadSource';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';

@injectable()
export class LeadSourceImporter extends BaseImporter<LeadSource>{

  getOrder(): number {
    return 8;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/lead_sources';
  }

  getMapping(): any{
    return {
      name: "label",
      details: "details"
    };
  }

  public async save(data: IDeSerialized, existing?: LeadSource, params?: any):Promise<LeadSource>{
    const values = this.getValues(data);
    let leadSource: LeadSource;
    if (existing) {
      leadSource = existing;
    }
    else {
      leadSource = new LeadSource();
    }
    for(let attr of Object.keys(values)){
      (leadSource as any).set(attr, values[attr]);
    }
    let err = null;
    if(params && params.hasOwnProperty('details')){
      leadSource.details = params.details;
    }
    await leadSource.save().catch(e => err = e);
    if(err){
      throw err;
    }
    return leadSource;
  }
}