import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import Role from '../../models/Role';

@injectable()
export class RoleImporter extends BaseImporter<Role>{
  getOrder(): number {
    return 1;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/roles';
  }

  getMapping(): any{
    return {
      name: "name",
    };
  }

}