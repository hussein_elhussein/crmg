import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized, IRelationIdB } from '../../lib/serializer/interfaces/IDeSerialized';
import { OnboardDocumentImporter } from './43.OnboardDocumentImporter';
import Document from '../../models/Document';
@injectable()
export class DocumentImporter extends BaseImporter<Document>{
    onboard_document_importer: OnboardDocumentImporter;
    constructor(@inject(OnboardDocumentImporter) onboard_document_importer: OnboardDocumentImporter){
      super();
      this.onboard_document_importer = onboard_document_importer;
    }
 
  getOrder(): number {
    return 42;
  }

  protected getRequiredProperties(): string[] {
    return ['fileType','file'];
  }

  getPath(): string {
    return '/contact_document_files';
  }

  getMapping(): any{
    return {
      fileType: 'contact_file_type',
      file: 'document_file.uri',
      status: 'document_status',
    };
  }

  async save(data: IDeSerialized, existing?:Document):Promise<Document>{
    const values = this.getValues(data);
    let document: Document;
    if (existing) {
      document = existing;
    }
    else {
      document = new Document();
    }
    for(let attr of Object.keys(values)){
      (document as any).set(attr, values[attr]);
    }
    if (data.relationships && data.relationships.document_base) {
        const relInfo = <IRelationIdB> data.relationships.document_base.data;
        await this.importRelation(relInfo, this.onboard_document_importer, document, 'onboardDocument', false)
    }
    await document.save();
    return document;
  }
}