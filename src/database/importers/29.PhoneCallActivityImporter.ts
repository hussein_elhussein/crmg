import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import PhoneCallActivity from '../../models/PhoneCallActivity';

@injectable()
export class PhoneCallActivityImporter extends BaseImporter<PhoneCallActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }


  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 29;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/phone_call_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      callType: "call_type",
      date: "date.value",
      notes: "notes.value",
      sendRemind: "send_remind",
      sendBefore: "send_before",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:PhoneCallActivity, params?: any):Promise<PhoneCallActivity>{
    data.created = this.parseDate(data.created);
    const values = this.getValues(data);
    let phoneCallActivity: PhoneCallActivity;
    if (existing) {
      phoneCallActivity = existing;
    }
    else {
      phoneCallActivity = new PhoneCallActivity();
    }
    for(let attr of Object.keys(values)){
      (phoneCallActivity as any).set(attr, values[attr]);
    }
    phoneCallActivity.type = "PhoneCallActivity";
    if(params){
      phoneCallActivity.activitiableId = params['activitiableId'];
      phoneCallActivity.activitiableType = params['activitiableType'];
    }
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, phoneCallActivity, 'user');
    }
    await phoneCallActivity.save();
    return phoneCallActivity;
  }
}