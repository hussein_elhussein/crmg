import { inject, injectable, named } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { ImporterManager } from '../../lib/database/importers/ImporterManager';
import { OfficeCodeImporter } from './11.OfficeCodeImporter';
import { SalesCodeImporter } from './12.SalesCodeImporter';
import Account from '../../models/Account';
import { CompanyTypeImporter } from './14.CompanyTypeImporter';
import { Kernel } from '../../lib/kernel/kernel';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import User from '../../models/User';
import { TradingNumberImporter } from './44.TradingNumberImporter';
import { RoutingNumberImporter } from './45.RoutingNumberImporter';
import { AccountNumberImporter } from './46.AccountNumberImporter';
import { BrokerImporter } from './48.BrokerImporter';
import { ReferralImporter } from './49.ReferralImporter';
import { SecuringAgentImporter } from './50.SecuringAgentImporter';
import { InsuranceProviderImporter } from './51.InsuranceProvider';
import { MilkHandlerImporter } from './52.MilkHandlerImporter';
import { MarginPushImporter } from './53.MarginPushImporter';
import { AccountTypeImporter } from './54.AccountTypeImporter';
import { TradingTypeImporter } from './55.TradingTypeImporter';
declare var kernel: Kernel;

@injectable()
export class AccountImporter extends BaseImporter<Account>{
  constructor(@inject(OfficeCodeImporter) protected office_importer: OfficeCodeImporter,
              @inject(SalesCodeImporter) protected sales_code_importer: SalesCodeImporter,
              @inject(CompanyTypeImporter) protected company_type_importer: CompanyTypeImporter,
              @inject(TradingNumberImporter) protected trading_number_importer: TradingNumberImporter,
              @inject(RoutingNumberImporter) protected routing_number_importer: RoutingNumberImporter,
              @inject(AccountNumberImporter) protected account_number_importer: AccountNumberImporter,
              @inject(BrokerImporter) protected brokerImporter: BrokerImporter,
              @inject(ReferralImporter) protected referralImporter: ReferralImporter,
              @inject(SecuringAgentImporter) protected securingAgentImporter: SecuringAgentImporter,
              @inject(InsuranceProviderImporter) protected insuranceProviderImporter: InsuranceProviderImporter,
              @inject(MilkHandlerImporter) protected milkHandlerImporter: MilkHandlerImporter,
              @inject(MarginPushImporter) protected marginPushImporter: MarginPushImporter,
              @inject(AccountTypeImporter) protected accountTypeImporter: AccountTypeImporter,
              @inject(TradingTypeImporter) protected tradingTypeImporter: TradingTypeImporter,
              @inject('Repository') @named('User') protected userRepo: BaseRepository<User>
  ){
    super();
  }

  shouldRun(): boolean {
    return false;
  }

  hasData(): boolean {
    return true;
  }

  getOrder(): number {
    return 15;
  }

  protected getRequiredProperties(): string[] {
    return ['accountableId','accountableType'];
  }

  getPath(): string {
    return '/account';
  }

  getMapping(): any{
    return {
      name: 'details.value',
      transactionNote: 'transaction_note',
      contactRate: 'contact_rate',
      hasSecurityAgreement: 'has_security_agreement',
      securityAgreement: 'security_agreement',
      hedge: 'contact_hedge',
      accountableId: "accountableId",
      accountableType: "accountableType"
    };
  }

  public getValues(data: IDeSerialized): any{
    const mapping = this.getMapping();
    const obj:any = {};
    for(let objKey of Object.keys(mapping)){
      const props = mapping[objKey].split('.');
      let val = null;
      for(let prop of props){
        val = val? val[prop]: Array.isArray(data[prop]) ? data[prop][0] : data[prop];
      }
      obj[objKey] = val;
    }
    for(let prop of Object.keys(obj)) {
      obj[prop] = this.valueFor(prop, obj[prop]);
    }
    return obj;
  }

  valueFor(property: string, value: any): any {
    if (property === 'contactRate') {
      if(!value){
        return null;
      }
      const v = value.match(/[0-9]+|([.][0-9]+)/);
      if (v && v.length) {
        return v[0];
      }
      return null;
    }
    if(property === "securityAgreement") {
      if(!value){
        return null;
      }
    }
    return super.valueFor(property, value);
  }

  async save(data: IDeSerialized, existing?: Account):Promise<Account>{
    let account: Account;
    const values = this.getValues(data);
    if (existing) {
      account = existing;
    }
    else {
      account = new Account();
    }

    for(let attr of Object.keys(values)){
      (account as any).set(attr, values[attr]);
    }
    await this.preSave(data, account);
    let err = null;
    await account.save({validate: false}).catch(e => err = e);
    if(err){
      const stop = null;
    }
    await this.postSave(data, account);
    return account;
  }

  async preSaveImportRelations(data: IDeSerialized, instance: Account) {
    // trading_type
    if (data.broker) {
      const relInfo = {id: data.broker};
      await this.importRelation(relInfo, this.brokerImporter, instance, 'broker');
    }
    if (data.referred_by) {
      const relInfo = {id: data.referred_by};
      await this.importRelation(relInfo, this.referralImporter, instance, 'referredBy');
    }
    if (data.securing_agent) {
      const relInfo = {id: data.securing_agent};
      await this.importRelation(relInfo, this.securingAgentImporter, instance, 'securingAgent');
    }
    if (data.insurance_provider) {
      const relInfo = {id: data.insurance_provider};
      await this.importRelation(relInfo, this.insuranceProviderImporter, instance, 'insuranceProvider');
    }
    if (data.milk_handler) {
      const relInfo = {id: data.milk_handler};
      await this.importRelation(relInfo, this.milkHandlerImporter, instance, 'milkHandler');
    }
    if (data.margin_push) {
      const relInfo = {id: data.margin_push};
      await this.importRelation(relInfo, this.marginPushImporter, instance, 'marginPush');
    }
    if (data.trading_type) {
      const relInfo = {id: data.trading_type};
      await this.importRelation(relInfo, this.tradingTypeImporter, instance, 'accountType');
    }
    if (data.contact_account_type) {
      const relInfo = {id: data.contact_account_type};
      await this.importRelation(relInfo, this.accountTypeImporter, instance, 'accountType');
    }
    if (data.company_type) {
      const relInfo = {id: data.company_type};
      await this.importRelation(relInfo, this.company_type_importer, instance, 'companyType');
    }
  }
  async postSaveImportRelations(data:IDeSerialized, instance: Account){
    if(!data.details){
      return;
    }
    const offices = [];
    const salesCodes = [];
    const tradingNumbers = [];
    const routingNumbers = [];
    const accountNumbers = [];
    for(let i = 0; i < data.details.length; i++){
      if(data.details[i].hasOwnProperty('office')){
        offices.push({id: data.details[i].office});
      }
      if(data.details[i].hasOwnProperty('sales')){
        salesCodes.push({id: data.details[i].sales});
      }
      if(data.trading_number && data.trading_number[i]){
        const tradingNumber = {
          accountId: instance.id,
          number: data.trading_number[i],
        };
        tradingNumbers.push(tradingNumber);

      }
      if(data.routing_number && data.routing_number[i]){
        const routingNumber = {
          accountId: instance.id,
          number: data.routing_number[i].text,
        };
        routingNumbers.push(routingNumber);

      }
      if(data.account_number && data.account_number[i]){
        const accountNumber = {
          accountId: instance.id,
          number: data.account_number[i].text,
        };
        accountNumbers.push(accountNumber);
      }
    }
    await this.importRelation(offices, this.office_importer, instance, 'officeCodes', true);
    await this.importRelation(salesCodes, this.sales_code_importer, instance, 'salesCodes', true);
    await this.toRelation(tradingNumbers, this.trading_number_importer, instance.id);
    await this.toRelation(routingNumbers, this.routing_number_importer, instance.id);
    await this.toRelation(accountNumbers, this.account_number_importer, instance.id);
    await this.importCashAdvisor(data, instance);
  }
  async importCashAdvisor(data:IDeSerialized, instance: Account){
    const cashAdvisors = [];
    for(let i=0; i < data.details.length; i++){
      if(data.details[i].cash_advisor && data.details[i].cash_advisor !== "0"){
        let path = kernel.getConfig().import_api + "/cash_advisor/" + data.details[i].cash_advisor;
        const token = ImporterManager.getToken();
        let err = null;
        const item = await ImporterManager.getData(path, token.access_token, this).catch(e => err = e);
        if(err){
          throw err;
        }
        if(item){
          const user_name = item.data.attributes.label.split(' ');
          if(user_name){
            const options:any = {
              first_name: user_name[0],
            };
            if (user_name.length >1) {
              options.last_name = user_name[1];
            }
            const user = await this.userRepo.findOne({ where: options });
            if(user){
              cashAdvisors.push(user);
            }
          }
        }
      }
    }
    if(cashAdvisors.length){
      let err = null;
      await instance.$set('cashAdvisors', cashAdvisors).catch(e => err = e);
      if(err){
        throw err;
      }
    }
  }
}