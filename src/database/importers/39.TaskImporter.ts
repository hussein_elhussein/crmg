import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import Task from '../../models/Task';

@injectable()
export class TaskImporter extends BaseImporter<Task>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 39;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/task_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      priority: "priority",
      sendeRemind: "send_remind",
      sendBefore: "send_before",
      complete: "complete",
      createdAt: "created",
    };
  }

  public async save(data: IDeSerialized, existing?:Task, params?: any):Promise<Task>{
    data.created = this.parseDate(data.created);
    const values = this.getValues(data);
    let taskActivity: Task;
    if (existing) {
      taskActivity = existing;
    }
    else {
      taskActivity = new Task();
    }
    for(let attr of Object.keys(values)){
      (taskActivity as any).set(attr, values[attr]);
    }
    taskActivity.type = "Task";
    if(params){
      taskActivity.taskableId = params['taskableId'];
      taskActivity.taskableType = params['taskableType'];
    }
    let err = null;
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, taskActivity, 'user');
    }
    if (data.assigned_to) {
      const relInfo = {id: data.assigned_to};
      await this.importRelation(relInfo, this.user_importer, taskActivity, 'assignee');
    }
    const options: SaveOptions = {validate: false};
    await taskActivity.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return taskActivity;
  }
}