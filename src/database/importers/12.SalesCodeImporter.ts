import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import SalesCode from '../../models/SalesCode';

@injectable()
export class SalesCodeImporter extends BaseImporter<SalesCode>{
  getOrder(): number {
    return 12;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/sales_codes';
  }

  getMapping(): any{
    return {
      name: 'label',
    };
  }

}