import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import NextContact from '../../models/NextContact';

@injectable()
export class NextContactImporter extends BaseImporter<NextContact>{
  constructor(@inject(UserImporter) protected user_importer: UserImporter){
    super();
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 40;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/next_contact_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      notes: "notes.value",
      type: "type",
      complete: "complete",
      createdAt: "date.value"
    };
  }

  public async save(data: IDeSerialized, existing?:NextContact, params?: any):Promise<NextContact>{
    const values = this.getValues(data);
    let instance: NextContact;
    if (existing) {
      instance = existing;
    }
    else {
      instance = new NextContact();
    }
    for(let attr of Object.keys(values)){
      (instance as any).set(attr, values[attr]);
    }
    instance.type = 'NextContactActivity';
    if(params){
      instance.nextContactableId = params['nextContactableId'];
      instance.nextContactableType = params['nextContactableType'];
    }
    let err = null;
    await this.preSave(data, instance, params);
    if(err){
      throw err;
    }
    await instance.save().catch(e => err = e);
    if(err){
      throw err;
    }
    await this.postSave(data, instance, params);
    return instance;
  }
  async preSaveImportRelations(data: IDeSerialized, instance: NextContact) {
    const user_id = data.user_id;
    if(user_id && user_id !== "0"){
      const relInfo = {id: user_id};
      await this.importRelation(relInfo, this.user_importer, instance, 'user');
    }
  }
}