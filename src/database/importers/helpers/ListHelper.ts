import { IDeSerialized } from '../../../lib/serializer/interfaces/IDeSerialized';
import { IRelationId } from '../../../lib/serializer/interfaces/IResource';
import List from '../../../models/List';

export class ListHelper {

  async preSaveImportRelations($this: any, data: IDeSerialized, instance: List){
    if (data.relationships && data.relationships.user) {
      const relInfo = data.relationships.user.data;
      await $this.importRelation(relInfo, $this.user_importer, instance, 'owner', false);
    }
  }
  public async postSaveImportRelations($this: any, data: IDeSerialized, instance: List) {
    if (data.filter) {
      const keys = Object.keys(data.filter);
      const filters = [];
      for(let key of keys){
        const int = parseInt(key);
        if(int === 0 || int) {
          if (data.filter[key].hasOwnProperty('operation') && data.filter[key].operation.length) {
            data.filter[key].listId = instance.id;
            filters.push(data.filter[key]);
          }
        }
      }
      await $this.toRelation(filters, $this.list_filter_importer, instance.id, null, true);
    }
    if (data.person_contacts) {
      const contacts = <IRelationId[]> data.person_contacts.map((item:any) => {
        return <IRelationId>{
          id: item,
          type: "person",
        };
      });
      await $this.importRelation(contacts, $this.person_importer, instance, 'people', true);
      const completedContacts = this.getCompletedContacts(data, contacts);
      if (completedContacts.length) {
        await $this.importRelation(completedContacts, $this.person_importer, instance, 'completedPersonContacts', true)
      }
    }

    if (data.company_contacts) {
      const contacts = <IRelationId[]> data.company_contacts.map((item: any) => {
        return <IRelationId>{
          id: item,
          type: "company"
        }
      });
      await $this.importRelation(contacts, $this.company_importer, instance, 'companies', true);
      const completedContacts = this.getCompletedContacts(data, contacts);
      if (completedContacts.length) {
        await $this.importRelation(completedContacts, $this.company_importer, instance, 'completedCompanyContacts', true)
      }
    }
  }

  protected getCompletedContacts(data: IDeSerialized, contacts:IRelationId[]) {
    const completedContacts = [];
    if (data.completed_contacts && data.completed_contacts.length) {
      for (let completed of data.completed_contacts) {
        for(let contact of contacts) {
          if (contact.id === completed) {
            completedContacts.push(contact);
          }
        }
      }
    }
    return completedContacts;
  }
}