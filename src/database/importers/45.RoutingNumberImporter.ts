import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import RoutingNumber from '../../models/RoutingNumber';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';

@injectable()
export class RoutingNumberImporter extends BaseImporter<RoutingNumber>{

  hasData(): boolean {
    return true;
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 45;
  }

  protected getRequiredProperties(): string[] {
    return ['number','accountId'];
  }

  getPath(): string {
    return '/';
  }

  getMapping(): any{
    return {
      number: "number",
      accountId: "accountId"
    };
  }

  async save(data: IDeSerialized, existing?: RoutingNumber | null, params?: any): Promise<RoutingNumber> {
    let instance: RoutingNumber;
    if(existing){
      instance = existing;
    }else{
      instance = new RoutingNumber();
    }
    const values = this.getValues(data);
    for(let attr of Object.keys(values)){
      (instance as any).set(attr, values[attr]);
    }
    await instance.save();
    return instance;
  }

}