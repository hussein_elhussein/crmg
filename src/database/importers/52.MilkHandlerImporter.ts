import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import MilkHandler from '../../models/MilkHandler';

@injectable()
export class MilkHandlerImporter extends BaseImporter<MilkHandler>{
  getOrder(): number {
    return 17;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/milk_handlers';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}