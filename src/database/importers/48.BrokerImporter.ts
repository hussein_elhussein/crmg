import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import Broker from '../../models/Broker';

@injectable()
export class BrokerImporter extends BaseImporter<Broker>{
  getOrder(): number {
    return 17;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/lead_brokers';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}