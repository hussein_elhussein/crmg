import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { SaveOptions } from 'sequelize';
import RelationshipsType from '../../models/RelationshipsType';

@injectable()
export class RelationshipsTypeImporter extends BaseImporter<RelationshipsType>{

  hasData(): boolean {
    return true;
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 26;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/relationships_type';
  }

  getMapping(): any{
    return {
      name: "name",
    };
  }

  public async save(data: IDeSerialized, existing?: RelationshipsType):Promise<RelationshipsType>{
    const values = this.getValues(data);
    let relType: RelationshipsType;
    if (existing) {
      relType = existing;
    }
    else {
      relType = new RelationshipsType();
    }
    for(let attr of Object.keys(values)){
      (relType as any).set(attr, values[attr]);
    }
    let err = null;
    const options: SaveOptions = {validate: false};
    await relType.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return relType;
  }
}