import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import Referral from '../../models/Referral';

@injectable()
export class ReferralImporter extends BaseImporter<Referral>{
  getOrder(): number {
    return 17;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/referred_by';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}