import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import PersonType from '../../models/PersonType';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';

@injectable()
export class PersonTypeImporter extends BaseImporter<PersonType>{

  hasData(): boolean {
    return true;
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 3;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/person_type';
  }

  getMapping(): any{
    return {
      name: "type",
    };
  }

  public async save(data: IDeSerialized, existing?: PersonType):Promise<PersonType>{
    const values = this.getValues(data);
    let personType: PersonType;
    if (existing) {
      personType = existing;
    }
    else {
      personType = new PersonType();
    }
    for(let attr of Object.keys(values)){
      (personType as any).set(attr, values[attr]);
    }
    let err = null;
    await personType.save().catch(e => err = e);
    if(err){
      throw err;
    }
    return personType;
  }
}