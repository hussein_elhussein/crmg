import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import Tag from '../../models/Tag';

@injectable()
export class TagImporter extends BaseImporter<Tag>{
  getOrder(): number {
    return 17;
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/tag';
  }

  getMapping(): any{
    return {
      name: "label"
    };
  }

}