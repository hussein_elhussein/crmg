import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import ContactsActivity from '../../models/ContactsActivity';

@injectable()
export class ContactsActivityImporter extends BaseImporter<ContactsActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 35;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/contacts_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes.value",
      contactType: "contact_type",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:ContactsActivity, params?: any):Promise<ContactsActivity>{
    const values = this.getValues(data);
    values.createdAt = this.parseDate(values.createdAt);
    let contactsActivity: ContactsActivity;
    if (existing) {
      contactsActivity = existing;
    }
    else {
      contactsActivity = new ContactsActivity();
    }
    for(let attr of Object.keys(values)){
      (contactsActivity as any).set(attr, values[attr]);
    }
    contactsActivity.type = "ContactsActivity";
    if(params){
      contactsActivity.activitiableId = params['activitiableId'];
      contactsActivity.activitiableType = params['activitiableType'];
    }
    let err = null;
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, contactsActivity, 'user');
    }
    const options: SaveOptions = {validate: false};
    await contactsActivity.save(options).catch(e => err = e);
    if(err){
      throw err;
    }
    return contactsActivity;
  }
}