import { inject, injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import Commodity from '../../models/Commodity';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { PersonImporter } from './10.PersonImporter';
import { CommodityTypeImporter } from './55.CommodityTypeImporter';

@injectable()
export class CommodityImporter extends BaseImporter<Commodity>{
  constructor(@inject(CommodityTypeImporter) protected commodityTypeImporter: CommodityTypeImporter,
              @inject(PersonImporter) protected personImporter: PersonImporter){
    super();
  }
  getOrder(): number {
    return 56;
  }

  protected getRequiredProperties(): string[] {
    return ['value'];
  }

  getPath(): string {
    return '/commodity';
  }

  getMapping(): any{
    return {
      value: "value",
    };
  }

  async save(data: IDeSerialized, existing?: Commodity | null, params?: any): Promise<Commodity> {
    const values = this.getValues(data);
    let instance: Commodity;
    if (existing) {
      instance = existing;
    }
    else {
      instance = new Commodity();
    }
    for(let attr of Object.keys(values)){
      (instance as any).set(attr, values[attr]);
    }
    await this.preSave(data, instance, params);
    let err = null;
    await instance.save({validate: false}).catch(e => err = e);
    if(err){
      throw err;
    }
    await this.postSave(data, instance, params);
    return instance;
  }

  async preSaveImportRelations(data: IDeSerialized, instance: Commodity){
    if(data.relationships) {
      if (data.relationships.commodity_type) {
        const relData = data.relationships.commodity_type.data;
        await this.importRelation(relData, this.commodityTypeImporter, instance, 'commodityType');
      }
      if (data.relationships.contact_lead) {
        const relData = data.relationships.contact_lead.data;
        await this.importRelation(relData, this.personImporter, instance, 'contactLead');
      }
    }
  }

}