import { injectable, inject } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { SaveOptions } from 'sequelize';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import { UserImporter } from './2.UserImporter'
import AchReportsActivity from '../../models/AchReportsActivity';

@injectable()
export class AchReportsActivityImporter extends BaseImporter<AchReportsActivity>{
    user_importer: UserImporter;
    constructor(@inject(UserImporter) user_importer: UserImporter){
      super();
      this.user_importer = user_importer;
    }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 32;
  }

  protected getRequiredProperties(): string[] {
    return ['title'];
  }

  getPath(): string {
    return '/ach_reports_activity';
  }

  getMapping(): any{
    return {
      title: "title",
      date: "date.value",
      notes: "notes",
      reportType: "report_type",
      reportDate: "report_date",
      transactionType: "transaction_type",
      createdAt: "created"
    };
  }

  public async save(data: IDeSerialized, existing?:AchReportsActivity, params?: any):Promise<AchReportsActivity>{
    const values = this.getValues(data);
    values.createdAt = this.parseDate(values.createdAt);
    values.reportDate = this.parseDate(values.reportDate);
    let achReportsActivity: AchReportsActivity;
    if (existing) {
      achReportsActivity = existing;
    }
    else {
      achReportsActivity = new AchReportsActivity();
    }
    for(let attr of Object.keys(values)){
      (achReportsActivity as any).set(attr, values[attr]);
    }
    achReportsActivity.type = "AchReportsActivity";
    if(params){
      achReportsActivity.activitiableId = params['activitiableId'];
      achReportsActivity.activitiableType = params['activitiableType'];
    }
    let err = null;
    if (data.user_id && data.user_id !== "0"){
      const relInfo = {id: data.user_id};
      await this.importRelation(relInfo, this.user_importer, achReportsActivity, 'user');
    }
    await achReportsActivity.save().catch(e => err = e);
    if(err){
      throw err;
    }
    return achReportsActivity;
  }
}