import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';
import Address from '../../models/Address';

@injectable()
export class AddressImporter extends BaseImporter<Address>{

  shouldRun(): boolean {
    return false;
  }

  hasData(): boolean {
    return true;
  }

  getOrder(): number {
    return 16;
  }

  protected getRequiredProperties(): string[] {
    return ['addressableId', 'addressableType'];
  }

  getPath(): string {
    return '/address';
  }

  getMapping(): any{
    return {
      primary: 'primary_address',
      country: 'country',
      state: 'administrative_area',
      city: 'locality',
      postalCode: 'postal_code',
      address: 'thoroughfare',
      addressTwo: 'premise',
      type: 'address_type',
      verified: 'postal_address_status',
      addressableId: "addressableId",
      addressableType: "addressableType",
    };
  }

  async save(data: IDeSerialized, existing?:Address):Promise<Address>{
    const values = this.getValues(data);
    let address: Address;
    if (existing) {
      address = existing;
    }
    else {
      address = new Address();
    }
    for(let attr of Object.keys(values)){
      if(typeof values[attr] === 'string' && !values[attr].length){
        values[attr] = null;
      }
      (address as any).set(attr, values[attr]);
    }
    let err = null;
    await address.save().catch(e => err = e);
    if(err){
      throw err;
    }
    return address;
  }
}