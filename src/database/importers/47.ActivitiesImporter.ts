import { inject, injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import { RpmActivityImporter } from './34.RpmActivityImporter';
import { ContactsActivityImporter } from './35.ContactsActivityImporter';
import { EmailActivityImporter } from './36.EmailActivityImporter';
import { MeetingActivityImporter } from './37.MeetingActivityImporter';
import { NoteActivityImporter } from './38.NoteActivityImporter';
import { TaskImporter } from './39.TaskImporter';
import { PhoneCallActivityImporter } from './29.PhoneCallActivityImporter';
import { EventsActivityImporter } from './30.EventsActivityImporter';
import { EmailReplyActivityImporter } from './31.EmailReplyActivityImporter';
import { AchReportsActivityImporter } from './32.AchReportsActivityImporter';
import { DeviceActivityImporter } from './33.DeviceActivityImporter';
import { SaleStatusImporter } from './4.SaleStatusImporter';
import { BusinessTypeImporter } from './6.BusinessTypeImporter';
import { LeadSourceImporter } from './8.LeadSourceImporter';
import { AccountImporter } from './15.AccountImporter';
import { AddressImporter } from './16.AddressImporter';
import { SocialLinkImporter } from './28.SocialLinkImporter';
import { EmailImporter } from './19.EmailImporter';
import { NoteImporter } from './26.NoteImporter';
import { PhoneNumberImporter } from './5.PhoneNumberImporter';
import { RelatedPersonImporter } from './27.RelatedPersonImporter';
import { NextContactImporter } from './40.NextContactImporter';
import { IDeSerialized, IRelationIdB } from '../../lib/serializer/interfaces/IDeSerialized';
import { BaseModel } from '../../lib/models/BaseModel';

@injectable()
export class ActivitiesImporter extends BaseImporter<any>{
  protected instance: BaseModel<any>;
  constructor(@inject(RpmActivityImporter) protected rpm_activity_importer: RpmActivityImporter,
              @inject(EmailActivityImporter) protected email_activity_importer: EmailActivityImporter,
              @inject(MeetingActivityImporter) protected meeting_activity_importer: MeetingActivityImporter,
              @inject(NoteActivityImporter) protected note_activity_importer: NoteActivityImporter,
              @inject(TaskImporter) protected task_importer: TaskImporter,
              @inject(PhoneCallActivityImporter) protected phone_call_activity_importer: PhoneCallActivityImporter,
              @inject(EventsActivityImporter) protected events_activity_importer: EventsActivityImporter,
              @inject(EmailReplyActivityImporter) protected email_reply_activity_importer: EmailReplyActivityImporter,
              @inject(AchReportsActivityImporter) protected ach_reports_activity_importer: AchReportsActivityImporter,
              @inject(DeviceActivityImporter) protected device_activity_importer: DeviceActivityImporter,
              @inject(SaleStatusImporter) protected sale_status_importer: SaleStatusImporter,
              @inject(BusinessTypeImporter) protected business_type_importer: BusinessTypeImporter,
              @inject(LeadSourceImporter) protected lead_source_importer: LeadSourceImporter,
              @inject(AccountImporter) protected account_importer: AccountImporter,
              @inject(AddressImporter) protected address_importer: AddressImporter,
              @inject(SocialLinkImporter) protected social_link_importer: SocialLinkImporter,
              @inject(EmailImporter) protected email_importer: EmailImporter,
              @inject(NoteImporter) protected note_importer: NoteImporter,
              @inject(PhoneNumberImporter) protected phone_number_importer: PhoneNumberImporter,
              @inject(RelatedPersonImporter) protected related_person_importer: RelatedPersonImporter,
              @inject(NextContactImporter) protected next_contact_importer: NextContactImporter,
              @inject(ContactsActivityImporter) protected contacts_activity_importer: ContactsActivityImporter,
  ){
    super();
  }

  async init(withModel: boolean = true): Promise<any> {
    return super.init(false);
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 47;
  }

  protected getRequiredProperties(): string[] {
    return [];
  }

  getPath(): string {
    return '';
  }

  getMapping(): any{
    return {};
  }

  public setInstance(instance: BaseModel<any>){
    this.instance = instance;
  }

  async save(data: IDeSerialized, existing?: any | null): Promise<any> {
    if(!data.relationships){
      return;
    }
    const params = {
      activitiableId: this.instance.id,
      activitiableType: (this.instance as any)._modelOptions.name.singular
    };
    if (data.relationships.phone_call_activity) {
      const relInfo = <IRelationIdB> data.relationships.phone_call_activity.data;
      const rel_res = await (this.instance as any).$get('phoneCallActivities');
      await this.importRelation(relInfo, this.phone_call_activity_importer, null, null, true, params);
      const rel_res_t = await (this.instance as any).$get('phoneCallActivities');
      const stop = null;
    }
    if (data.relationships.events_activity) {
      const relInfo = <IRelationIdB> data.relationships.events_activity.data;
      await this.importRelation(relInfo, this.events_activity_importer, null, null, true, params);
    }
    if (data.relationships.email_reply_activity) {
      const relInfo = <IRelationIdB> data.relationships.email_reply_activity.data;
      await this.importRelation(relInfo, this.email_reply_activity_importer, null, null, true, params);
    }
    if (data.relationships.ach_reports_activity) {
      const relInfo = <IRelationIdB> data.relationships.ach_reports_activity.data;
      await this.importRelation(relInfo, this.ach_reports_activity_importer, null, null, true, params);
    }
    if (data.relationships.device_activity) {
      const relInfo = <IRelationIdB> data.relationships.device_activity.data;
      await this.importRelation(relInfo, this.device_activity_importer, null, null, true, params);
    }
    if (data.relationships.rpm_activity) {
      const relInfo = <IRelationIdB> data.relationships.rpm_activity.data;
      await this.importRelation(relInfo, this.rpm_activity_importer, null, null, true, params);
    }
    if (data.relationships.email_activity) {
      const relInfo = <IRelationIdB> data.relationships.email_activity.data;
      await this.importRelation(relInfo, this.email_activity_importer, null, null, true, params);
    }
    if (data.relationships.meeting_activity) {
      const relInfo = <IRelationIdB> data.relationships.meeting_activity.data;
      await this.importRelation(relInfo, this.meeting_activity_importer, null, null, true, params);
    }
    if (data.relationships.note_activity) {
      const relInfo = <IRelationIdB> data.relationships.note_activity.data;
      await this.importRelation(relInfo, this.note_activity_importer, null, null, true, params);
    }
    if (data.relationships.task_activity) {
      const relInfo = <IRelationIdB> data.relationships.task_activity.data;
      const taskParams = {
        taskableId: this.instance.id,
        taskableType: (this.instance as any)._modelOptions.name.singular
      };
      await this.importRelation(relInfo, this.task_importer, null, null, true, taskParams);
    }
    if (data.relationships.next_contact_activity) {
      const nextContactParams = {
        nextContactableId: this.instance.id,
        nextContactableType: (this.instance as any)._modelOptions.name.singular
      };
      const relInfo = <IRelationIdB> data.relationships.next_contact_activity.data;
      await this.importRelation(relInfo, this.next_contact_importer, null, null, true, nextContactParams);
    }
    if (data.relationships.contacts_activity) {
      const relInfo = <IRelationIdB> data.relationships.contacts_activity.data;
      await this.importRelation(relInfo, this.contacts_activity_importer, null, null, true, params);
    }

  }

}