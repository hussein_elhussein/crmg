import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import ListFilter from '../../models/ListFilter';
import { IDeSerialized } from '../../lib/serializer/interfaces/IDeSerialized';

@injectable()
export class ListFilterImporter extends BaseImporter<ListFilter>{

  hasData(): boolean {
    return true;
  }

  shouldRun(): boolean {
    return false;
  }

  getOrder(): number {
    return 22;
  }

  protected getRequiredProperties(): string[] {
    return ['operator', 'listId'];
  }

  getPath(): string {
    return '/list_filter';
  }

  getMapping(): any{
    return {
      type: "field_type",
      property: "property",
      operator: "operation",
      values: "val",
      listId: "listId",
    };
  }

  public async save(data: IDeSerialized, existing?: ListFilter):Promise<ListFilter>{
    const _values = this.getValues(data);
    const values:any = {};
    for(let key of Object.keys(_values)) {
      if(_values[key]?.length){
        values[key] = _values[key];
      }
    }
    const vals = [];
    if (Array.isArray(values.values)) {
      for (let val of values.values) {
        vals.push(val);
      }
    }
    else {
      vals.push(values.values);
    }
    values.values = JSON.stringify(vals);
    let listFilter: ListFilter;
    if (existing) {
      listFilter = existing;
    }
    else {
      listFilter = new ListFilter();
    }
    for(let attr of Object.keys(values)){
      (listFilter as any).set(attr, values[attr]);
    }
    await listFilter.save();
    return listFilter;
  }
}