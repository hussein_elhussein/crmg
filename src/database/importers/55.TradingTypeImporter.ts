import { injectable } from 'inversify';
import { BaseImporter } from '../../lib/database/importers/BaseImporter';
import AccountType from '../../models/AccountType';
import { DatabaseService } from '../../lib/services/DatabaseService';
import { Kernel } from '../../lib/kernel/kernel';
declare var kernel: Kernel;

@injectable()
export class TradingTypeImporter extends BaseImporter<AccountType>{

  async init(withModel: boolean = true): Promise<any> {
    this.sequelize = kernel.getContainer().get(DatabaseService).getDatabase();
    if (withModel) {
      this.model = AccountType;
    }
  }

  getOrder(): number {
    return 17;
  }

  getIdProperty(): string {
    return 'label';
  }

  protected getRequiredProperties(): string[] {
    return ['name'];
  }

  getPath(): string {
    return '/trading_types';
  }

  getMapping(): any{
    return {
      name: "label",
    };
  }

}