import { QueryInterface } from 'sequelize';
import { injectable } from 'inversify';
import { BaseSeeder } from '../../lib/database/seeders/BaseSeeder';
import { StaticModel } from '../../types/model.t';
import { Factory } from '../../lib/database/factories/Factory';
import User from '../../models/User';

@injectable()
export class UserSeeder extends BaseSeeder<User>{
	protected queryInterface: QueryInterface;

	getOrder(): number {
		return 2;
	}

	getModel(): StaticModel<User>{
		return User;
	}

	setQueryInterface(queryInterface: QueryInterface): void {
		this.queryInterface = queryInterface;
	}

	async up(): Promise<User[]> {
		const user = new User();
		user.username = "anonymous";
		return [user];
	}

	async down(): Promise<any> {
		this.queryInterface.delete(null,'users', {
			attribute: "username",
			logic: "=",
			val: "anonymous",
		});
		await this.queryInterface.bulkDelete('users',{});
	}

}
