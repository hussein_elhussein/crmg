import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import { random } from "faker";
import SalesCode from '../../models/SalesCode';
@injectable()
export class SalesCodeFactory extends BaseFactory<SalesCode>{

    async generate(): Promise<SalesCode> {
        const salesCode = new SalesCode();
        salesCode.name = random.number(4).toString();
        return salesCode;
    }
}