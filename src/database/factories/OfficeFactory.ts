import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import { random } from "faker";
import OfficeCode from '../../models/OfficeCode';
@injectable()
export class OfficeFactory extends BaseFactory<OfficeCode>{

    async generate(): Promise<OfficeCode> {
        const office = new OfficeCode();
        office.name = random.words(2);
        return office;
    }
}