import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import BusinessType from '../../models/BusinessType';
@injectable()
export class BusinessTypeFactory extends BaseFactory<BusinessType>{

    async generate(): Promise<BusinessType> {
        const businessType = new BusinessType();
        businessType.name = 'Other';
        return businessType;
    }
}