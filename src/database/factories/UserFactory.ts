import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import User from '../../models/User';
import { injectable } from 'inversify';
import { name, random, internet } from "faker";

@injectable()
export class UserFactory extends BaseFactory<User>{

    async generate(): Promise<User> {
        const user = new User();
        user.firstName = name.firstName();
        user.lastName = name.lastName();
        user.username = internet.userName();
        user.email = internet.email();
        user.active = true;
        user.password = internet.password();
        user.cashAdvisor = random.boolean();
        return user;
    }
}