import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { inject, injectable, named } from 'inversify';
import { random, name, date, internet } from "faker";
import Person from '../../models/Person';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import PersonType from '../../models/PersonType';
import SaleStatus from '../../models/SaleStatus';
import BusinessType from '../../models/BusinessType';
import User from '../../models/User';

@injectable()
export class PersonFactory extends BaseFactory<Person>{
    personTypeRepo: BaseRepository<PersonType>;
    saleStatusRepo: BaseRepository<SaleStatus>;
    businessTypeRepo: BaseRepository<BusinessType>;
    userRepo: BaseRepository<User>;
    constructor(@inject("Repository") @named("PersonType") personTypeRepo: BaseRepository<PersonType>,
                @inject("Repository") @named("SaleStatus") saleStatusRepo: BaseRepository<SaleStatus>,
                @inject("Repository") @named("BusinessType") businessTypeRepo: BaseRepository<BusinessType>,
                @inject("Repository") @named("User") userRepo: BaseRepository<User>,){
        super();
        this.personTypeRepo     = personTypeRepo;
        this.saleStatusRepo     = saleStatusRepo;
        this.businessTypeRepo   = businessTypeRepo;
        this.userRepo           = userRepo;
    }

    async generate(): Promise<Person> {
        const person = new Person();
        person.firstName = "Bill";
        person.lastName = name.lastName();
        person.middleName = name.firstName();
        person.prefix = 'Dr.';
        person.suffix = 'I';
        person.credentials = random.words();
        person.accountOpenedDate = date.past().toISOString();
        person.statementType = 'Paper';
        person.doNotCall = random.boolean();
        person.birthDate = date.past().toISOString();
        person.gender = random.boolean() ? "Male": "Female";
        person.website = internet.url();
        person.jobTitle = name.jobTitle();

        const personType = await this.personTypeRepo.findOneOrFail();
        const saleStatus = await this.saleStatusRepo.findOneOrFail();
        const businessType = await this.businessTypeRepo.findOneOrFail();
        const user = await this.userRepo.findOneOrFail();
        await person.$set("personType", personType);
        await person.$set('saleStatus', saleStatus);
        await person.$set('businessType', businessType);
        await person.$set('owner', user);
        return person;
    }
}