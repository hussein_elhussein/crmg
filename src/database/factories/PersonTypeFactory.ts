import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import {random} from "faker";
import PersonType from '../../models/PersonType';
@injectable()
export class PersonTypeFactory extends BaseFactory<PersonType>{

    async generate(): Promise<PersonType> {
        const personType = new PersonType();
        personType.name = 'Related person';
        return personType;
    }
}