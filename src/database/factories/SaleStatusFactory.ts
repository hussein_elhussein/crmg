import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import {random} from "faker";
import SaleStatus from '../../models/SaleStatus';
@injectable()
export class SaleStatusFactory extends BaseFactory<SaleStatus>{

    async generate(): Promise<SaleStatus> {
        const saleStatus = new SaleStatus();
        saleStatus.name = random.words(3);
        return saleStatus;
    }
}