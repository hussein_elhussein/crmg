import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import Tag from '../../models/Tag';
import {random} from "faker";
@injectable()
export class TagFactory extends BaseFactory<Tag>{

    async generate(): Promise<Tag> {
        const tag = new Tag();
        tag.name = random.words(3);
        return tag;
    }
}