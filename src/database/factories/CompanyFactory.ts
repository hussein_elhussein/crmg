import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { inject, injectable, named } from 'inversify';
import { random, internet, company as companyFaker,date } from "faker";
import Company from '../../models/Company';
import LocationType from '../../models/LocationType';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import Person from '../../models/Person';
import BusinessType from '../../models/BusinessType';

@injectable()
export class CompanyFactory extends BaseFactory<Company>{
    locationTypeRepo: BaseRepository<LocationType>;
    personRepo: BaseRepository<Person>;
    businessTypeRepo: BaseRepository<BusinessType>;
    constructor(@inject("Repository") @named("LocationType") locationTypeRepo: BaseRepository<LocationType>,
                @inject("Repository") @named("Person") personRepo: BaseRepository<Person>,
                @inject("Repository") @named("BusinessType") businessTypeRepo: BaseRepository<BusinessType>){
        super();
        this.locationTypeRepo = locationTypeRepo;
        this.personRepo = personRepo;
        this.businessTypeRepo = businessTypeRepo;
    }

    async generate(): Promise<Company> {
        const company = new Company();
        company.name = companyFaker.companyName();
        company.website = internet.url();
        company.annualRevenue = random.number().toString();
        company.accountOpenedDate = date.past().toISOString();
        company.numberOfEmployees = random.number();
        const locationType = await this.locationTypeRepo.findOneOrFail();
        const person = await this.personRepo.findOneOrFail();
        const businessType = await this.businessTypeRepo.findOneOrFail();
        await company.$set('locationType', locationType);
        await company.$set("primaryContact", person);
        await company.$set("businessType", businessType);
        return company;
    }
}