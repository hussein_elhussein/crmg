import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { inject, injectable, named } from 'inversify';
import { random } from "faker";
import Account from '../../models/Account';
import { BaseRepository } from '../../lib/repositories/BaseRepository';
import Person from '../../models/Person';
import SalesCode from '../../models/SalesCode';
import OfficeCode from '../../models/OfficeCode';

@injectable()
export class AccountFactory extends BaseFactory<Account>{
    personRepo: BaseRepository<Person>;
    salesCodeRepo: BaseRepository<SalesCode>;
    officeRepo: BaseRepository<OfficeCode>;
    constructor(@inject("Repository") @named("Person") personRepo: BaseRepository<Person>,
                @inject("Repository") @named("SalesCode") salesCodeRepo: BaseRepository<SalesCode>,
                @inject("Repository") @named("Office") officeRepo: BaseRepository<OfficeCode>){
        super();
        this.personRepo = personRepo;
        this.salesCodeRepo = salesCodeRepo;
        this.officeRepo = officeRepo;
    }

   async generate(): Promise<Account> {
        const account = new Account();
        account.name = random.words(2);
        account.transactionNote = random.words(4);
        const officeCode = await this.officeRepo.findOneOrFail();
        const salesCode = await this.salesCodeRepo.findOneOrFail();
        const personId = await this.personRepo.findOneOrFail();
        await account.$set('officeCodes', [officeCode]);
        await account.$set('accountableId', personId);
        await account.$set('salesCodes', [salesCode]);
        return account;
    }
}