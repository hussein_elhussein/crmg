import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import LocationType from '../../models/LocationType';

@injectable()
export class LocationTypeFactory extends BaseFactory<LocationType>{

    async generate(): Promise<LocationType> {
        const locationType = new LocationType();
        locationType.name = 'Regional office';
        return locationType;
    }
}