import { BaseFactory } from '../../lib/database/factories/BaseFactory';
import { injectable } from 'inversify';
import Role from '../../models/Role';

@injectable()
export class RoleFactory extends BaseFactory<Role>{

    async generate(): Promise<Role> {
        const role = new Role();
        role.name = "Administrator";
        return role;
    }
}