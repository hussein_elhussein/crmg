import { FactoryManager } from './FactoryManager';
import { BaseFactory } from './BaseFactory';
import { StaticModel } from '../../../types/model.t';
import { BaseModel } from '../../models/BaseModel';

export class Factory {

    public static async generate<T extends BaseModel>(model: StaticModel<T>, count: number, save?: boolean):Promise<T[]>{
        let factory: BaseFactory<any>|null = null;
        try {
            factory = FactoryManager.getFactory(model);
        } catch (e) {
            throw e;
        }

        const instances = <T[]> await factory.run(count);

        if(save){
            for(let instance of instances){
                let err = null;
                await instance.save().catch(e => err = e);
                const sss = null;
                if(err){
                    throw err;
                }
            }
        }
        return instances;
    }
}