import { FindOptions } from 'sequelize';
import { SaveOptions } from 'sequelize/types/lib/model';
import { add } from 'winston';
import { BaseModel, BaseModel as Model } from '../../models/BaseModel';
import { Op, Transaction } from 'Sequelize';
import { BaseRepository } from '../../repositories/BaseRepository';
import { Kernel } from '../../kernel/kernel';
import { StaticModel } from '../../../types/model.t';
import { ModelManager } from '../../models/ModelManager';
import { Sequelize } from 'sequelize-typescript';
import { DatabaseService } from '../../services/DatabaseService';
import { RepositoryManager } from '../../repositories/RepositoryManager';
import { IDeSerialized } from '../../serializer/interfaces/IDeSerialized';
import { ImportedItem } from './interfaces/ImportedItem';
import { ImporterManager } from './ImporterManager';
import * as moment from 'moment';
import { ReportService } from './ReportService';
import { SharedImporterData } from './SharedImporterData';

declare var kernel: Kernel;
export type ExistingDataStrategy = "skip"|"update";
export abstract class BaseImporter<T extends Model> {
  model: StaticModel<T>;
  importedItemRepo: BaseRepository<ImportedItem>;
  sequelize: Sequelize;
  sharedImporterData: SharedImporterData;
  protected data: any = null;
  validateInstance: boolean = true;
  protected transaction: Transaction;
  constructor(){
    this.init();
  }
  public async init(withModel: boolean = true): Promise<any>{
    this.sequelize = kernel.getContainer().get(DatabaseService).getDatabase();
    this.sharedImporterData = kernel.getContainer().get(SharedImporterData);
    if (withModel) {
      const name = this.constructor.name.replace('Importer','');
      this.model = ModelManager.resolveModel(name);
    }
  }

  public shouldRun():boolean{
    return true;
  }
  public hasData(): boolean {
    return false;
  }
  public getData(){
    return this.data;
  }

  public setData(data: any) {
    this.data = data;
  }

  /**
   * Returns the resource path.
   */
  public abstract getPath():string;

  public abstract getOrder():number;

  /**
   * Returns the unique properties names
   */
  public getIdProperty():string{
    return "id"
  }

  /**
   * Returns the unique properties names
   */
  public getAdditionalIds(): string[]|null{
    return null;
  }


  /**
   * Gets the required properties in the imported data.
   */
  protected abstract getRequiredProperties():string[];

  public getExistingDataStrategy():ExistingDataStrategy{
    return "update";
  }

  /**
   * Determines whether data item is unique.
   *
   * @param data
   */
  public async isUnique(data: IDeSerialized):Promise<boolean>{
    let err = null;
    const res = await this.findExisting(data, this.model).catch(e => err = e);
    if(err){
      throw err;
    }
    return !res;
  }

  public getUniqueId(data: IDeSerialized): string|null{
    const idProperty = this.getIdProperty();
    const id = data[idProperty];
    if(id === undefined){
      return null;
    }
    return id;
  }
  /**
   * Gets existing data based on unique identifiers.
   *
   * @param data
   * @param model
   */
  public async findExisting<T extends Model>(data: IDeSerialized, model: StaticModel<T>):Promise<T|false>{
    const repo = RepositoryManager.findRepository(model);
    const uniqueAttrs = this.getAdditionalIds();
    let err = null;
    let found = null;

    // look in the imported items:
    const id = this.getUniqueId(data);
    if (id) {
      const item = await this.findImportedItem(id, data, model).catch(e => err = e);
      if(item && !err){
        return item;
      }
    }
    if(err){
      throw err;
    }
    let options = this.loadQueryOptions(data);
    if(options){
      found = await repo.findOne(options).catch(e => err = e);
      if(found && !err){
        return found;
      }
    }
    if(uniqueAttrs && !options){
      let filters = [];
      for(let attr of Object.keys(data)){
        if(uniqueAttrs.includes(attr) && data[attr]){
          const filter:any = {};
          filter[attr] = data[attr];
          filters.push(filter);
        }
      }

      let err = null;
      const options:FindOptions = {
        where: {
          [Op.or]: filters
        },
      };
      let found = null;
      if(filters.length){
        found = await repo.findOne(options).catch(e => err = e);
        if(found && !err){
          return found;
        }
      }
    }
    return false;
  }
  protected loadQueryOptions(data: IDeSerialized): FindOptions|null {
    return null;
  }

  public async countImported(): Promise<number|null> {
    return null;
  }

  public async findImportedItem<T extends Model>(id: string, data: IDeSerialized,  model: StaticModel<T>):Promise<T|null> {
    const repository = <BaseRepository<ImportedItem>> kernel.getContainer().get('ImportedItemRepository');
    this.importedItemRepo = repository;
    const repo = RepositoryManager.findRepository(model);
    const table_name = model.tableName;
    if (parseInt(id)) {
      id = id.toString();
    }
    let options:FindOptions|null = null;
    if (parseInt(id)){
      options = {
        where: {
          unique_id: id,
          table_name: table_name,
        }
      };
    }
    else {
      options = {
        where: {
          unique_id: {
            [Op.like]: id,
          },
          table_name: table_name,
        },
      };
    }

    let err = null;
    let item = <ImportedItem> await this.importedItemRepo.findOne(options).catch(e => err = e);
    if(!item && !Array.isArray(data)) {
      options = await this.loadQueryOptions(data);
      if (options) {
        let xxxx =  await repo.findOne(options).catch(e => err = e);
        if(err) {
          throw err;
        }
        return xxxx;
      }
    }
    if (err){
      throw err;
    }
    if(!item){
      return null;
    }
    return <T> await repo.findByPk(item.itemId,{rejectOnEmpty: true});
  }

  /**
   * Validates the imported data
   *
   * @param data
   */
  public async validate(data: any):Promise<boolean>{
    const requiredProperties = this.getRequiredProperties();
    if(requiredProperties.length){
      for(let required of requiredProperties){
        if(!data.hasOwnProperty(required) || data[required] === undefined || data[required] === null || (typeof data[required] === 'string' && !data[required].length)){
          const addValRes =  await this.additionalValidation(data, required);
          if(!addValRes){
            return false;
          }
        }
      }
    }
    return true;
  }

  protected async additionalValidation(data: any, attr: string):Promise<boolean>{
    return false;
  }

  protected getRepository():BaseRepository<any>{
    const name = this.constructor.name.replace("Importer","Repository");
    return <BaseRepository<any>>kernel.getContainer().get(name);
  }

  /**
   * Mapping blueprint of keys(right-hand side) to a model properties.
   * model property => object key:
   *
   */
  public abstract getMapping(): any;

  /**
   * Returns an object with known properties based on the mapping.
   *
   * @param data
   */
  public getValues(data: IDeSerialized): any{
    const mapping = this.getMapping();
    const obj:any = {};
    for(let objKey of Object.keys(mapping)){
      const props = mapping[objKey].split('.');
      let val = null;
      for(let prop of props){
        val = val? val[prop]: data[prop];
      }
      obj[objKey] = val;
    }
    for(let prop of Object.keys(obj)) {
      obj[prop] = this.valueFor(prop, obj[prop]);
    }
    return obj;
  }

  public valueFor(property: string, value: any): any {
    return value;
  }

  protected parseDate(epoch: number):any{
    if (typeof epoch === 'object') {
      return epoch;
    }
    const mm = moment.unix(epoch).toDate();
    return mm;
  }

  /**
   * A hook that runs before starting importing an item.
   *
   * @param data
   * @param instance
   * @param params
   * @param transaction
   */
  public async preSave(data: IDeSerialized, instance:T, params?: any, transaction?: Transaction){
    await this.preSaveImportRelations(data, instance, params);
  }

  public async preSaveImportRelations(data: IDeSerialized, instance: BaseModel<any>, params?: any):Promise<any>{}

  /**
   * creates and saves a model from object.
   *
   * @param data
   * @param existing
   * @param params any parameters to pass
   * @param transaction
   */
  public async save(data: IDeSerialized, existing?:T|null, params?: any, transaction?: Transaction):Promise<T>{
    let instance: T;
    const values = this.getValues(data);
    if (existing) {
      instance = existing;
      for(let attr of Object.keys(values)){
        (instance as any).set(attr, values[attr]);
      }
    }
    else {
      instance = new this.model(values);
    }
    let err = null;
    const op:SaveOptions = {validate: this.validateInstance};
    await this.preSave(data, instance, params);
    await instance.save(op).catch(e => err = e);
    if(err){
      throw err;
    }
    await this.postSave(data, instance, params);
    return instance;
  }

  public async postSave(data: IDeSerialized, instance: BaseModel<any>, params?: any):Promise<any>{
    await this.postSaveImportRelations(data, instance, params);
  }

  public async postSaveImportRelations(data: IDeSerialized, instance: BaseModel<any>, params?: any): Promise<any>{}

  protected async toRelation(
      data: any,
      importer: BaseImporter<any>,
      parentModelId?: string|null,
      additionalAttr: any = {},
      multiple = true,
      autoId = true,
      params?: any,
  ): Promise<BaseModel<any>[]>{
    const items:any = [];
    const ids:any = [];
    const importedItems = [];
    const initAttr = (_item: any, key: string| number) => {
      const name = importer.model.name.toLowerCase();
      let id_prefix = null;
      let id = null;
      if(parentModelId){
        const pName = this.model.name.toLowerCase();
        id_prefix = pName + "_" + parentModelId;
        id = id_prefix + `_${name}_` + key;
      }
      if(!autoId){
        id = _item.id;
      }
      const item = {
        data: {
          id: id,
          attributes: {
            id: id,
            ..._item,
            ...additionalAttr,
          }
        }
      };
      items.push(item);
      ids.push({id: id});
    };
    if (multiple && Array.isArray(data)) {
      for(let i = 0; i < data.length; i++ ){
        initAttr(data[i], i);
      }
    }
    else {
      initAttr(data, 0);
    }
    for(let i = 0; i < items.length; i++ ){
      importer.setData(items[i]);
      const res = await this.importRelation(ids[i], importer, null, null, multiple, params);
      if(res){
        if(Array.isArray(res)){
          for(let imported of res){
            importedItems.push(imported);
          }
        }
        else {
          importedItems.push(res);
        }
      }
    }
    return importedItems;
  }
  protected async importRelation(
      data: any,
      importer: BaseImporter<any>,
      instance?: any,
      instanceProp?: string| null,
      multiple?: boolean,
      params?:any
  ){
    const items:Array<BaseModel<any>> = [];
    const doImport = async (item: any) => {
      const importedItem = await importer.findImportedItem(item.id, data, importer.model);
      if (importedItem) {
        // @todo: find a way to run the item's importer again in case the sub relations didn't import previously:
        // const relInstance = await ImporterManager.importItem(importer, item.id, instance, params);
        items.push(importedItem);
      }
      else if (!this.sharedImporterData.isRunning(importer, item.id)) {
        const relInstance = await ImporterManager.importItem(importer, item.id, instance, params);
        if (relInstance) {
          items.push(relInstance);
        }
      }
    };
    const report = async (item: any, err: any) => {
      // @todo: decide whether to throw the error based on the config
      const report_ser = kernel.getContainer().get(ReportService);
      let item_id = null;
      if (Object.keys(item).includes('id')) {
        item_id = item.id;
      }
      else if(Object.keys(item).includes('unique_id')) {
        item_id = item.unique_id;
      }
      await report_ser.updateReport(importer, null, err, false, item_id);
    }
    if (Array.isArray(data)){
      for (let item of data) {
        let err = null;
        await doImport(item).catch(e => err = e);
        if(err){
          await report(item, err);
        }
      }
    }
    else {
      let err = null;
      await doImport(data).catch(e => err = e);
      if(err){
        await report(data, err);
      }
    }
    if (!instance) {
      return items;
    }
    if (!items.length) {
      return null;
    }
    if (multiple) {
      await instance.$set(instanceProp, items);
    }
    else {
      await instance.$set(instanceProp, items[0]);
    }
  }

}