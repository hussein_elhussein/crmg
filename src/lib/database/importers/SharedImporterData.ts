import { injectable } from 'inversify';
import { BaseImporter } from './BaseImporter';
import { IDeSerialized } from '../../serializer/interfaces/IDeSerialized';
import { IRunningImporter } from './interfaces/IRunningImporter';

@injectable()
export class SharedImporterData{
  runningNow: IRunningImporter[] = [];




  public markRunning(importer: BaseImporter<any>, data: IDeSerialized){
    if(data.name === "Company a"){
      const stop = null;
    }
    if(!data.hasOwnProperty('id') || !data.id){
      return;
    }
    if(this.isRunning(importer, data.id)){
      return;
    }
    const item: IRunningImporter = {
      name: importer.model.name,
      id: data.id,
      afterImportCallbacks: [],
    };
    this.runningNow.push(item);
  }

  public markFinished(importer: BaseImporter<any>, data: IDeSerialized){
    if(!data.hasOwnProperty('id') || !data.id){
      return;
    }
    this.runningNow = this.runningNow.filter(item => {
      return !(item.id === data.id && item.name === importer.model.name);
    });
  }

  public isRunning(importer: BaseImporter<any>, id: any):boolean{
    let running = this.runningNow.filter(item => {
      return item.id === id && item.name === importer.model.name;
    })
    return running.length > 0;
  }
}