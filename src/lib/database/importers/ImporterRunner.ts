import { inject, injectable, named, tagged } from 'inversify';
import { BaseImporter, ExistingDataStrategy } from './BaseImporter';
import { IDeSerialized } from '../../serializer/interfaces/IDeSerialized';
import { BaseModel } from '../../models/BaseModel';
import { CurrentJob } from './types/CurrentJob';
import { ReportService } from './ReportService';
import { ModelManager } from '../../models/ModelManager';
import { BaseRepository } from '../../repositories/BaseRepository';
import ImportedItem from '../../../models/ImportedItem';
import { SharedImporterData } from './SharedImporterData';

@injectable()
export class ImporterRunner{
  protected existing: boolean;
  protected saved: boolean;
  public constructor(@inject(ReportService) protected report_ser: ReportService,
                     @inject('Repository') @named('ImportedItem') protected importedItemRepo: BaseRepository<ImportedItem>,
                     @inject(SharedImporterData) protected sharedImporterData: SharedImporterData)
  {

  }

  public async run<T extends BaseModel>(importer: BaseImporter<T>, data: IDeSerialized, params?: any, strategy?: ExistingDataStrategy, return_job?: boolean): Promise<T|null|CurrentJob> {
    const obj = importer.getValues(data);
    // 1. validate the data:
    const valid = await importer.validate(obj);
    let current_job:CurrentJob = "skipping";
    // todo: instead of calling getExisting in the manager and the importer, add the existing item to @unique
    const id = importer.getUniqueId(data);
    if(id === "82107") {
      const stop = null;
    }
    if (!valid) {
      await this.report_ser.updateProgress(importer.constructor.name, 1,0, id, "skipping");
      const err_data = {
        importer: importer.constructor.name,
        unique_id: id,
        data: data,
        params: params
      };
      const err = new Error('Item is not valid to import: ' + JSON.stringify(err_data));
      err.name = "Invalid item";
      throw err;
    }
    this.sharedImporterData.markRunning(importer, data);
    // 2. save the data:
    let instance: T|null = null;
    if (!strategy) {
      strategy = importer.getExistingDataStrategy();
    }
    let err = null;
    switch (strategy) {
      case 'skip':
        // import only new items:
        instance = await this.importUnique(importer, data, params).catch(e => err = e);
        break;
      case 'update':
        // import new or update existing:
        instance = await this.importAndUpdate(importer, data, params).catch(e => err = e);
        break;
    }
    if(err){
      throw err;
    }
    this.sharedImporterData.markFinished(importer, data)
    if (this.saved) {
      if (this.hasRemoteData(importer)) {
        current_job = this.existing ? "updating": "inserting";
      }
      else {
        current_job = "inserting";
      }
    }
    if (!importer.hasData() && !importer.shouldRun()) {
      await this.report_ser.updateReport(importer, null, null, true,);
    }
    if (this.hasRemoteData(importer)) {
      await this.report_ser.updateProgress(importer.constructor.name, 1, 0, id, current_job, this.existing);
    }
    if (instance) {
      await this.upsertImportedItem(instance, importer, id, data, current_job);
    }
    if (return_job) {
      return current_job;
    }
    return instance;
  }

  protected async importUnique<T extends BaseModel>(importer: BaseImporter<T>, data: IDeSerialized, params?: any, id?: string|null ): Promise<T|null> {
    let unique: any = await importer.isUnique(data);
    let instance: T;
    if(!unique){
      let _id:any = id;
      if(!_id){
        _id = importer.getUniqueId(data);
      }
      instance = <T> await importer.findImportedItem(_id, data, importer.model);
      if(!instance){
        const stop = null;
      }
      await importer.postSaveImportRelations(data, instance, params);
      return instance;
    }
    instance = await importer.save(data, null, params);
    this.saved = true;
    this.existing = unique === false;
    return instance;
  }

  protected async importAndUpdate<T extends BaseModel>(importer: BaseImporter<T>, data: IDeSerialized, params?: any, id?: string|null ): Promise<T> {
    let instance: T;
    let existing: T|null = null;
    let unique = await importer.isUnique(data);
    this.existing = unique === false;
    if (this.existing) {
      existing = <T> await importer.findExisting(data, importer.model);
      this.existing = !!existing;
    }
    else {
      const stop = null;
    }
    let err = null;
    instance = await importer.save(data, existing, params).catch(e => err = e);
    if(err){
      throw err;
    }
    this.saved = true;
    return instance;
  }

  protected hasRemoteData<T extends BaseModel>(importer: BaseImporter<T>): boolean{
    if (importer.shouldRun() || !importer.hasData()) {
      return true;
    }
    return false;
  }


  /**
   * Creates or updates an imported_item entry.
   *
   * @param instance
   * @param importer
   * @param id
   * @param data
   * @param currentJob
   * @protected
   */
  protected async upsertImportedItem(instance: BaseModel<any>, importer: BaseImporter<any>, id: string|null, data:IDeSerialized,  currentJob: CurrentJob){
    const importedItemModel = ModelManager.resolveModel('ImportedItem');
    const table_name = (instance as any)._modelOptions.tableName;
    let item;
    if(currentJob === "inserting") {
      item = new importedItemModel();
    }
    else {
      item = await this.importedItemRepo.findOne({
        where: {
          table_name: table_name,
          unique_id: id,
        }
      })
      if (!item) {
        item = new importedItemModel();
      }
    }
    if (item.isNewRecord) {
      item.itemId = instance.id;
      item.tableName = importer.model.tableName;
      item.uniqueId = id;
      item.imported = true;
      item.updated = false;
    }
    else {
      item.updated = true;
    }

    let err = null;
    await item.save().catch((e: any) => err = e);
    if(err){
      throw err;
    }
  }
}