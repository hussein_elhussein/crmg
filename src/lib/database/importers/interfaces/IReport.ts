export interface IFailedItem {
  path: string,
  message: string,
  remote_id?: any;
  error: any,
}
export interface IReportEntry {
  importedItems: number,
  failed: IFailedItem[],
  completed: boolean,
}
export interface IReport {
  [name: string]: IReportEntry;
}