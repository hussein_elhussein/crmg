import ImporterFailedItem from '../../../../models/ImporterFailedItem';
import { BaseImporter } from '../BaseImporter';

export interface IFailedImport {
  importer: BaseImporter<any>;
  id?: any;
  path?: string;
  failedItem: ImporterFailedItem;
}