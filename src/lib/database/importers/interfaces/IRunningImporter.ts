export interface IRunningImporter{
  name: string,
  id: string,
  afterImportCallbacks: Array<any>,
}