import ImporterReport from '../../../../models/ImporterReport';

export interface IReportSaveQueue{
  reports: Array<ImporterReport>;
  name: string;
}