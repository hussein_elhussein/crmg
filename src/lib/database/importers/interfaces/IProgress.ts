export interface IProgressItem {
  name: string,
  total: number,
  fetched: number;
  imported: number,
  updated: number,
}
export interface IProgress {
  items: IProgressItem[],
  current: string,
  total: number;
  fetched: number;
  imported: number;
  updated: number;
  processed: number,
  percentage: number;
  fetch_percentage: number;
}