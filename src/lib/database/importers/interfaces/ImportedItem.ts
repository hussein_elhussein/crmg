import { BaseModel } from '../../../models/BaseModel';

export interface ImportedItem extends BaseModel{
  itemId:string;
}