export interface ISkippableRelation{
  type: string,
  id: string,
  afterImportCallbacks?: Array<any>;
}