import { Transaction } from 'sequelize';

export interface ITransaction {
  importer: string;
  transaction?: Transaction;
}