export interface IImportSettingsCache {
  last_importer: string|null;
  last_error?: string|null;
  items_per_importer: number;
  imported_items: number;
  last_page?: string;
  completed: boolean;
}