import { message } from 'gulp-typescript/release/utils';
import { Op } from 'Sequelize';
import { IProgress, IProgressItem } from './interfaces/IProgress';
import { inject, injectable, named } from 'inversify';
import { Logger } from 'winston';
import { BaseImporter } from './BaseImporter';
import { IImportSettingsCache } from './interfaces/IImportSettingsCache';
import { ImporterManager } from './ImporterManager';
import { Kernel } from '../../kernel/kernel';
import { access, writeFile } from 'fs';
import ImporterReport from '../../../models/ImporterReport';
import ImporterFailedItem from '../../../models/ImporterFailedItem';
import { IReportSaveQueue } from './interfaces/IReportSaveQueue';
import { BaseRepository } from '../../repositories/BaseRepository';
import ImportedItem from '../../../models/ImportedItem';
import { CurrentJob } from './types/CurrentJob';
declare var kernel: Kernel;

@injectable()
export class ReportService{
  public progress: IProgress;
  progressCount = 0;
  reportSaveQueue: Array<IReportSaveQueue>;
  importers: Array<BaseImporter<any>> = [];

  constructor(@inject("LoggerService") protected loggerService: Logger,
              @inject('Repository') @named('ImportedItem') protected importedItemRepo: BaseRepository<ImportedItem>,
              @inject('Repository') @named('ImporterFailedItem') protected importerFailedItemRepo: BaseRepository<ImportedItem>)
  {

  }

  async updateCache(settings: IImportSettingsCache,
                           imported_items?: number|null,
                           last_error?: any,
                           last_importer?:string|null,
                           last_page?:string|null,
                           completed?:boolean|null):Promise<any>{
    return new Promise<any>((resolve, reject) => {
      if(imported_items){
        settings.imported_items = imported_items;
      }
      const err_message = last_error && last_error.hasOwnProperty('message')? last_error.message: null;
      if(last_error){
        settings.last_error = err_message;
      }
      if(last_importer){
        settings.last_importer = last_importer;
      }
      if(last_page){
        settings.last_page = last_page;
      }
      if(completed !== undefined && completed !== null){
        settings.completed = completed;
      }
      const file_name = "importer_cache.json";
      const path = kernel.getConfig().temp_path + "/" + file_name;
      const str = JSON.stringify(settings);
      writeFile(path,str, (err) => {
        if (err){
          reject(err);
          return;
        }
        else {
          resolve(true);
          return;
        }
      });
    });
  }

  getReports():Promise<ImporterReport[]>{
    return ImporterReport.findAll({include: [ImporterFailedItem]});
  }
  async getReport(importer?: string, remote_id?: string): Promise<ImporterReport|null>{
    let report: ImporterReport|null = null;
    if (importer) {
      if(this.reportSaveQueue){
        // search in the queue:
        for(let item of this.reportSaveQueue){
          if(item.name === importer){
            return item.reports[item.reports.length -1];
          }
        }
      }
      report = await ImporterReport.findOne({where: {name: importer}, include: [ImporterFailedItem]});
    }
    else if(remote_id) {
      const failed_item = await ImporterFailedItem.findOne({where: {remoteId: remote_id}, include: [ImporterReport]});
      if (failed_item) {
        report = failed_item.importerReport;
      }
    }
    return report;
  }
  async updateReport(importer: BaseImporter<any>,
                     failed_item_path?: string|null,
                     error?: any,
                     completed: boolean = false,
                     item_id: string| null = null): Promise<ImporterReport|null> {
    const hasError = error != undefined && error;
    if(hasError){
      const stop = null;
    }
    let err = null;
    const importer_name = importer.constructor.name;
    let report = await this.getReport(importer_name);
    if(!report){
      report = new ImporterReport();
    }
    report.name = importer_name;
    report.completed = completed;
    let failed = <ImporterFailedItem[]> [];
    if (report.failed && report.failed.length) {
      report.completed = false;
    }
    if (failed_item_path) {
      if(report.failed) {
        failed = report.failed;
      }
      let where = null;
      if (report.isNewRecord) {
        where = {
          remoteId: item_id,
        };
      }
      else {
        where =  {
          [Op.and]: [
            {
              remoteId: item_id,
            },
            {
              importerReportId: report.id,
            }
          ]
        }
      }
      let failed_item:ImporterFailedItem = await this.importerFailedItemRepo.findOne({where: where}).catch(e => err = e);
      if(!failed_item) {
        failed_item = new ImporterFailedItem();
      }
      failed_item.path = failed_item_path;
      failed_item.message = error?.message;
      failed_item.error = hasError? JSON.stringify(error): null;
      failed_item.remoteId = item_id? item_id: "";
      failed.push(failed_item);
      report.completed = false;
    }
    report.updatedItems = await this.countUpdated(importer);
    report.importedItems = await this.countImported(importer);
    if (failed.length || completed) {
      await report.save().catch(e => err = e);
      this.clearQueueItem(report.name);
    }
    else {
      const queueItem = this.addReportToQueue(report);
      await this.checkAndSaveReportQueue(queueItem).catch(e => err = e);
    }
    for(let item of failed){
      if (item.isNewRecord) {
        item.importerReportId = report.id;
      }
      await item.save().catch(e => err = e);
    }
    return report;
  }

  addReportToQueue(report: ImporterReport): IReportSaveQueue{
    if(!this.reportSaveQueue) {
      this.reportSaveQueue = [];
    }
    let foundReports = this.reportSaveQueue.filter(item => item.name === report.name);
    let queueItem: IReportSaveQueue = {
      reports: [report],
      name: report.name
    };
    if(foundReports.length){
      queueItem.reports = foundReports[0].reports;
      queueItem.reports.push(report);
    }
    const reports = this.reportSaveQueue.filter(item => item.name !== report.name);
    reports.push(queueItem);
    this.reportSaveQueue = reports;
    return queueItem;
  }

  async checkAndSaveReportQueue(queueItem: IReportSaveQueue){
    // skip if the reports in a queue item is less than 10:
    if(queueItem.reports.length < 10){
      return;
    }
    // remove the item from the queue:
    this.clearQueueItem(queueItem.name);

    // save it:
    const last_report = queueItem.reports[queueItem.reports.length -1];
    await last_report.save();
    const stop = null;
  }

  async saveAllQueueItems(){
    if(!this.reportSaveQueue){
      return;
    }
    for(let item of this.reportSaveQueue){
      const last_report = item.reports[item.reports.length -1];
      await last_report.save();
    }
    this.reportSaveQueue = [];
  }

  clearQueueItem(name: string){
    if(!this.reportSaveQueue){
      return;
    }
    this.reportSaveQueue = this.reportSaveQueue.filter(item => item.name !== name);
  }

  async prepareProgress(importers: BaseImporter<any>[],
                        settings: IImportSettingsCache,
                        exist_on_fail:boolean = false,
                        token?: string):Promise<IProgress> {
    this.importers = importers;
    const conf = kernel.getConfig();
    const base_path = conf.import_api;
    const progressItems: IProgressItem[] = [];
    let total: number = 0;
    let imported: number = 0;

    // reset the updated items count:
    await this.importedItemRepo.update({updated: false}, {where: {updated: true}}).catch(e => {});
    // calculate the progress:
    for(let importer of importers) {
      this.loggerService.info('Initializing ' + importer.constructor.name);
      let path = base_path + importer.getPath();
      if (path.includes('?')){
        path += "&range=1";
      }
      else {
        path += "?range=1";
      }
      let err:any = null;
      let data: any = null;
      if (token && importer.getPath() && importer.getPath().length) {
        data = await ImporterManager.getData(path, token, importer).catch(e => err = e);
        if (err) {
          throw err;
        }
      }
      const count = data?.meta?.count? data.meta.count: 0;
      let sub_total: number = count;
      if (settings.items_per_importer && settings.items_per_importer != -1) {
        if (settings.items_per_importer <= count) {
          sub_total = settings.items_per_importer;
        }
      }
      // get number of imported items:
      const sub_imported_count = await this.countImported(importer);
      if (sub_imported_count > sub_total) {
        sub_total = sub_imported_count;
      }
      await this.prepareReport(importer, sub_total, sub_imported_count);
      const progressItem:IProgressItem = {
        imported: sub_imported_count,
        fetched: 0,
        updated: 0,
        total: sub_total,
        name: importer.constructor.name,
      };
      progressItems.push(progressItem);
      total += sub_total;
      imported += sub_imported_count;
    }
    const progress: IProgress = {
      items: progressItems,
      current: "",
      total: total,
      fetched: 0,
      imported: imported,
      updated: 0,
      processed: 0,
      percentage: 0,
      fetch_percentage: 0,
    };
    if (conf.existing_data_strategy === "skip") {
      progress.percentage =  parseFloat(((progress.imported/progress.total) * 100).toFixed(2));
    }
    this.progress = progress;
    return progress;
  }

  private async prepareReport(importer: BaseImporter<any>, total: number, imported: number){
    const name = importer.constructor.name;
    const strategy = kernel.getExistingDataStrategy();
    let report = await this.getReport(name);
    if(!report){
      report = new ImporterReport();
    }
    report.total = total;
    report.importedItems = imported;
    report.updatedItems = 0;
    report.name = name;
    let completed = false;
    if (imported >= total){
      completed = true;
      report.total = imported;
    }
    report.completed = strategy === "update"? false: completed;
    await report.save();
  }

  private async countImported(importer: BaseImporter<any>):Promise<number>{
    if(!importer.model){
      return 0;
    }
    let count = await importer.countImported();
    if(count){
      return count;
    }
    count = await this.importedItemRepo.count({
      where: {
        table_name: importer.model.tableName,
        imported: true,
      }
    });
    return count;
  }

  private async countUpdated(importer: BaseImporter<any>):Promise<number>{
    if(!importer.model){
      return 0;
    }
    const count = await this.importedItemRepo.count({
      where: {
        table_name: importer.model.tableName,
        updated: true,
      }
    });
    return count;
  }

  async updateProgress(importer: string, processed: number, fetched: number, remote_id?: any, current_job?: CurrentJob, existing?: boolean): Promise<IProgress>{
    const progress = this.progress;
    const found_importer = this.importers.filter(item => {
      return item.constructor.name === importer;
    });
    if(!found_importer || !found_importer.length) {
      return progress;
    }
    let subImported = 0;
    let found = false;
    if(importer === "MapListImporter") {
      const stop = null;
    }
    const ex_str = kernel.getConfig().existing_data_strategy;
    // collect the total of each importer data items to be imported:
    for (let item of progress.items ) {
      if (item.name == importer) {
        found = true;
        if (fetched) {
          if (!item.fetched) {
            item.fetched = 0;
          }
          item.fetched += fetched;
        }
        if (current_job === "inserting" && !existing) {
          item.imported += processed;
        }
        else if(current_job === "updating"){
          item.updated += processed;
        }
        subImported = item.imported;
        if (item.imported > item.total) {
          item.total = item.imported;
        }
        if (item.updated > item.total) {
          item.total = item.updated;
        }
        break;
      }
    }
    if (!found) {
      subImported = processed;
      const progItem:IProgressItem = {
        imported: current_job === "inserting"? processed: 0,
        updated: current_job === "updating"? processed: 0,
        name: importer,
        total: processed,
        fetched: fetched,
      };
      progress.items.push(progItem);
    }
    progress.current = importer;
    if (current_job === "inserting") {
      progress.imported += processed;
    }
    else if (current_job === "updating" ) {
      progress.updated += processed;
    }
    if (current_job === "inserting" || current_job === "updating") {
      progress.processed += processed;
    }
    // fix percentage bugs caused by processed/updated/imported items being larger than the total:
    if (progress.imported > progress.total) {
      progress.total = progress.imported;
    }
    if (progress.updated > progress.total) {
      progress.total = progress.updated;
    }
    //  update percentage:
    if (ex_str === "update") {
      progress.percentage =  parseFloat(((progress.processed/progress.total) * 100).toFixed(2));
    }
    else if (ex_str === "skip") {
      progress.percentage =  parseFloat(((progress.imported/progress.total) * 100).toFixed(2));
    }

    if (fetched) {
      progress.fetched += fetched;
    }
    progress.fetch_percentage =  parseFloat(((progress.fetched/progress.total) * 100).toFixed(2));

    this.progress = progress;
    const log = {
      total: progress.total,
      fetched: progress.fetched,
      processed: progress.processed,
      percentage: "%" + progress.percentage,
      fetch_percentage: '%' + progress.fetch_percentage,
      imported: progress.imported,
      updated: progress.updated,
      current_job: current_job,
      importer: progress.current,
      importer_count: subImported,
      remote_id: remote_id,
    };
    await this.logProgress(log).catch(e => {});
    return progress;
  }

  async logProgress(progress: any):Promise<any>{
    this.progressCount++;
    const file_name = "importer_progress.json";
    const path = kernel.getConfig().temp_path + "/" + file_name;
    const str = JSON.stringify(progress);
    if(this.progressCount < 10){
      return;
    }
    this.progressCount = 0;
    const write = () => {
      return new Promise<any>((resolve, reject) => {
        writeFile(path,str, (err) => {
          if (err){
            reject(err);
            return;
          }
          else {
            resolve(true);
            return;
          }
        });
      })
    };
    await write();
    return;
  }

  async deleteFailedItem(failedItem: ImporterFailedItem) {
    let err = null;
    const res = await failedItem.destroy({force: true}).catch(e => err = e);
    return res && !err;
  }
}