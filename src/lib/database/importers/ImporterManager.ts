import { Container } from 'inversify';
import { Kernel } from '../../kernel/kernel';
import { BaseImporter, ExistingDataStrategy } from './BaseImporter';
import Axios from "axios";
import { AccessToken } from '../../console/app-commands/importers/AccessToken';
import { BaseModel } from '../../models/BaseModel';
import { Manager } from '../../container/Manager';
import { Serializer } from '../../serializer/Serializer';
import { IDeSerialized } from '../../serializer/interfaces/IDeSerialized';
import { IImportSettingsCache } from './interfaces/IImportSettingsCache';
import { access } from "fs";
import { ReportService } from './ReportService';
import { IFailedImport } from './interfaces/IFailedImport';
import ImporterReport from '../../../models/ImporterReport';
import { CurrentJob } from './types/CurrentJob';
import { Logger } from 'winston';
import { ImporterRunner } from './ImporterRunner';
import { SharedImporterData } from './SharedImporterData';

declare var kernel: Kernel;

export class ImporterManager extends Manager{
  protected static getPath():string{
    return kernel.getConfig().importers_path;
  }
  public static async register(container: Container){
    container.bind(SharedImporterData).toSelf().inSingletonScope();
    const importers = await super.register(container,false);
    container.bind('AllImporters').toConstantValue(importers);
    container.bind(ReportService).toSelf().inSingletonScope();
    container.bind(ImporterRunner).toSelf();
    return importers;
  }

  public static getImporters():BaseImporter<any>[]{
    const container: Container = kernel.getContainer();
    const importers_names:any[] =  container.get('AllImporters');
    const importers:BaseImporter<any>[] = [];
    for(let im of importers_names){
      const importer = container.resolve<BaseImporter<any>>(im);
      importers.push(importer);
    }
    return importers;
  }

  public static filterImporters(importers: BaseImporter<any>[], settings: IImportSettingsCache, restart: boolean):BaseImporter<any>[] {
// get all importers that should run
    let _importers = importers.filter((item) => {
      return item.shouldRun();
    });

    // sort them:
    importers = _importers.sort((a,b) => {
      return a.getOrder() - b.getOrder();
    });

    // filter out all importers that we ran before:
    if(settings.last_importer || (!settings.completed && !restart)){
      let last_importer_index = 0;
      for(let i = 0; i < importers.length; i++){
        if(importers[i].constructor.name === settings.last_importer){
          last_importer_index = i;
        }
      }
      if(last_importer_index){
        importers = importers.filter((item, index) => {
          return index >= last_importer_index;
        });
      }
    }

    // filter out importers based on the configuration:
    const toInclude = kernel.getConfig().importers_to_run;
    if (toInclude && toInclude.length) {
      if (!toInclude.includes("*all*")) {
        importers = importers.filter(item => {
          return toInclude.includes(item.constructor.name);
        });
      }
    }
    return importers;
  }
  public static async getFailed(importers: BaseImporter<any>[]): Promise<IFailedImport[]> {
    let failedImports:IFailedImport[] = [];
    const container = kernel.getContainer();
    const report_ser:ReportService = container.get(ReportService);
    let err = null;
    const reports = <ImporterReport[]> await report_ser.getReports().catch(e => err = e);
    if (err) {
      return failedImports;
    }
    if (!reports) {
      return failedImports;
    }
    for(let report of reports) {
      const failed = report.failed;
      if(!failed.length) {
        continue;
      }
      for(let failedItem of failed) {
        if(!failedItem.remoteId && !failedItem.path) {
          continue;
        }
        let importer = importers.filter((item) => {
          return item.constructor.name === report.name;
        });
        if (importer.length) {
          let item = <IFailedImport> {
            importer: importer[0],
            id: failedItem.remoteId,
            path: failedItem.path,
            failedItem: failedItem,
          };
          failedImports.push(item);
        }
      }
    }
    return failedImports;
  }

  static importEnabled(): boolean{
    return kernel.getConfig().import_enabled;
  }
  static getToken(): AccessToken{
    const path = kernel.getConfig().temp_path;
    return require(path + '/token.json');
  }
  static getImportSettingsCache():Promise<IImportSettingsCache|null>{
    const file_name = "importer_cache.json";
    const path = kernel.getConfig().temp_path + "/" + file_name;
    return new Promise((resolve, reject) => {
      access(path, (err) => {
        if(err){
          reject(err);
          return;
        }
        const settings = require(path);
        resolve(settings);
      });
    });
  }

  static async getImportSettings(restart: boolean, items_per_importer: number): Promise<IImportSettingsCache >{
    let settings = <IImportSettingsCache> await this.getImportSettingsCache().catch(e => null);
    if(!settings || restart){
      settings = <IImportSettingsCache>{
        imported_items: 0,
        items_per_importer: items_per_importer,
        last_error: null,
        last_importer: null,
        last_page: '',
        completed: false,
      }
    }
    return settings;
  }

  public static async import(items_per_importer: number = -1, restart = false, exit_on_fail:boolean = false): Promise<any>{
    if(!this.importEnabled()){
      throw Error("Importer is disabled.");
    }
    // get all importers:
    let importers = this.getImporters();
    // get settings:
    const settings = await this.getImportSettings(restart, items_per_importer);

    // register report service:
    const container = kernel.getContainer();

    const report_ser = container.get(ReportService);
    const token = this.getToken().access_token;

    // filter importers based on user configuration:
    importers = this.filterImporters(importers, settings, restart);
    await report_ser.prepareProgress(importers, settings, exit_on_fail, token);
    if (settings.last_importer || (!settings.completed && !restart)) {
      await this.start(importers, settings, false, exit_on_fail);
    }
    else {
      await this.start(importers, settings, restart, exit_on_fail);
    }
  }

  public static async importFailed(exit_on_fail: boolean = false):Promise<any> {
    const container = kernel.getContainer();
    // register report service:
    const report_ser = container.get(ReportService);
    // get settings:
    const settings = await this.getImportSettings(true, -1);
    // get all importers:
    let importers = this.getImporters();
    // filter out all importers that we ran before:
    importers = this.filterImporters(importers, settings, true);
    let failedImports = await this.getFailed(importers);
    const conf = kernel.getConfig();
    const base_path = conf.import_api;
    importers = failedImports.map(item => item.importer);
    await report_ser.prepareProgress(importers, settings, exit_on_fail);
    for (let failed of failedImports) {
      let err = null;
      await this.importItem(failed.importer, failed.id, null, null, failed.path).catch(e => err = e);
      if (err) {
        if (exit_on_fail) {
          throw err;
        }
      } else {
        await report_ser.deleteFailedItem(failed.failedItem);
      }
    }
  }
  public static async start(importers: BaseImporter<any>[], settings: IImportSettingsCache, restart:boolean = true, exit_on_fail:boolean = false):Promise<any>{
    const conf = kernel.getConfig();
    const token = this.getToken().access_token;
    const items_per_importer = settings.items_per_importer;
    const base_path = conf.import_api;
    const container = kernel.getContainer();
    const report_ser = container.get(ReportService);
    const serializer = container.get(Serializer);
    const logger:Logger = container.get("LoggerService");
    logger.info("Importing started");
    for (let importer of importers) {
      const importer_name = importer.constructor.name;
      logger.info("Running " + importer_name + "...");
      let path = base_path + importer.getPath();
      let last_imported_item = null;
      if (!restart) {
        if (settings.last_error && settings.imported_items) {
          last_imported_item = settings.imported_items;
          if (settings.last_page && settings.last_page.length) {
            path = settings.last_page;
          }
        }
      }
      // 1. get the data from api:
      let err:any = null;
      let firstPage = true;
      let hasNext = false;
      let total_items_imported = 0;
      let data = await this.getData(path, token, importer, total_items_imported, restart, exit_on_fail, items_per_importer)
          .catch(e => err = e);
      if (err) {
        if(exit_on_fail){
          throw err;
        }
        continue;
      }
      await report_ser.updateCache(settings, total_items_imported, null, importer_name, path);
      const unlimited = items_per_importer == -1;
      while ((firstPage || hasNext) && (total_items_imported < items_per_importer || unlimited)) {
        const deserialized = <IDeSerialized[]>serializer.deSerialize(data);
        let imported_items_in_page = 0;
        await report_ser.updateProgress(importer_name, 0, deserialized.length);
        for (let i = 0; i < deserialized.length; i++) {
          const item = deserialized[i];
          let remote_id = null;
          if(last_imported_item && i < last_imported_item){
            remote_id = importer.getUniqueId(item);
            await report_ser.updateProgress(importer_name, imported_items_in_page,0 ,remote_id, "skipping", false);
            continue;
          }
          if (total_items_imported < items_per_importer || unlimited) {
            const import_res = await this.runImporter(importer,item, null, kernel.getExistingDataStrategy(), true)
                .catch(e => err = e);
            if (err) {
              let rem_id = remote_id? remote_id: importer.getUniqueId(item);
              await report_ser.updateCache(settings, total_items_imported, err, importer_name, path);
              await report_ser.updateReport(importer, path, err, false, rem_id);
              if (exit_on_fail) {
                throw err;
              }
              err = null;
            }
            else if(import_res && import_res !== 'updating' && import_res !== "skipping"){
              total_items_imported++;
              imported_items_in_page++;
            }
          }
          else {
            break;
          }
        }
        firstPage = false;
        hasNext = !!data?.links?.next;
        if (hasNext && (total_items_imported < items_per_importer || unlimited)) {
          let next_link:string = data.links.next;
          data = await this.getData(next_link, token, importer, total_items_imported, restart, exit_on_fail, items_per_importer)
              .catch(e => err = e);
          if (err) {
            if (exit_on_fail) {
              throw err;
            }
            continue;
          }
        }
        if (!err) {
          await report_ser.updateReport(importer);
        }
        err = null;
      }
      await report_ser.updateReport(importer, null, null, true);
    }
    await report_ser.updateCache(settings, null, null, null, null, true);
    let err = null;
    await report_ser.saveAllQueueItems().catch(e => err = e);
  }

  public static async importItem<T extends BaseModel>(importer: BaseImporter<T>, id: string, instance?: any, params?: any, path?: string):Promise<T|T[]>{
    const token = this.getToken().access_token;
    const conf = kernel.getConfig();
    const base_path = conf.import_api;
    const strategy = kernel.getExistingDataStrategy();
    const importer_name = importer.constructor.name;
    // 1. get the data from api:
    if(id) {
      path = base_path + importer.getPath() + '/' + id;
    }
    else if(!path){
      path = base_path + importer.getPath() + '/' + id;
    }
    let data = null;
    if (importer.shouldRun() || !importer.hasData()) {
      data = await this.getData(path, token, importer, 0);
    }
    else {
      data = await importer.getData();
      importer.setData(null);
    }
    const serializer = kernel.getContainer().get(Serializer);
    let deserialized = <IDeSerialized[]>serializer.deSerialize(data);
    const instances:T[] = [];
    if (Array.isArray(deserialized)) {
      for(let item of deserialized){
        const instance = <T> await this.runImporter(importer, item, params, strategy);
        if(instance){
          instances.push(instance);
        }
      }
      return instances;
    }
    else {
      return <T> await this.runImporter(importer, deserialized, params, strategy);
    }
  }

  protected static async runImporter<T extends BaseModel>(importer: BaseImporter<T>,
                                                          data: IDeSerialized,
                                                          params?: any,
                                                          strategy?: ExistingDataStrategy,
                                                          return_job?: boolean):Promise<T|null|CurrentJob>
  {
    const runner = kernel.getContainer().get(ImporterRunner);
    return await runner.run(importer, data, params, strategy, return_job);
  }

  public static async getData(path: string,
                              token: string,
                              importer: BaseImporter<any>,
                              total_imported: number = 0,
                              restart:boolean = true,
                              exit_on_fail:boolean = false,
                              per_importer: number = -1):Promise<any>{
    const container = kernel.getContainer();
    const report_ser = container.get(ReportService);
    const max_tries = 100;
    let err:any = null;
    let data = null;
    let wait_seconds = 0;
    let tries = 0;
    const importer_name: string = importer? importer.constructor.name: "";
    const settings = await this.getImportSettings(restart, per_importer);
    while(tries < max_tries && !data){
      tries++;
      data = await this.makeRequest(path, token).catch(e => err = e);
      wait_seconds = 10;
      if(err){
        if (this.isConnectionErr(err)) {
          const connected = await this.waitForConnection();
          if(!connected){
            throw err;
          }
        }
        else {
          await report_ser.updateCache(settings, total_imported, err, importer_name, path);
          await report_ser.updateReport(importer, path, err);
          if (exit_on_fail) {
            throw err;
          }
          if (err.code === "ECONNRESET" || err.code === "ECONNABORTED" || err?.response?.status === 500) {
            // connection error,
            const next_page = this.increasePageNumber(path);
            return await this.getData(next_page, token, importer, total_imported, restart, exit_on_fail, per_importer);
          }
          if(data?.response?.status === 404){
            throw err;
          }
          throw err;
        }
        data = null;
        err = null;
      }
    }
    if(err){
      throw err;
    }
    return data;
  }
  public static isConnectionErr(err: any): boolean{
    return err && err.hasOwnProperty('code') && err.code === "ENOTFOUND";
  }

  /**
   * waits for seconds.
   *
   * @param seconds
   */
  public static wait(seconds: number): Promise<any>{
    return new Promise((resolve, reject) => {
      const secs = seconds * 1000;
      setTimeout(() => {
        resolve(true);
      }, secs);
    });
  }

  /**
   *
   * @param max_wait maximum seconds to wait for connection, default is 1 week
   */
  public static async waitForConnection(max_wait: number = 604800): Promise<any>{
    let seconds = 0;
    let waited = 0;
    while(waited < max_wait){
      if(seconds){
        waited++;
      }
      let err = null;
      await this.wait(seconds);
      let res = await Axios.get('https://google.com').catch(e => err = e);
      seconds = 1;
      if(res && !err){
        return true;
      }
    }
    return false;
  }
  public static async makeRequest(path: string, token: string):Promise<any>{
    const headers = {
      "access-token": token,
      "Accept": 'application/vnd.api+json; charset=utf-8',
    };
    let err = null;
    const timeout = 120000; // 2 minutes
    let res = await Axios.get(path, {headers: headers, timeout: timeout}).catch(e => err = e);
    if (err && res?.response?.status === 422) {
      if(res.response.data.hasOwnProperty('title') && res.response.data.title.includes('does not exist.')){
        return null;
      }
      throw err;
    }

    if(err){
      throw err;
    }
    return res.data;
  }
  static increasePageNumber(page_link: string): string{
    // skip the next page, and move to the one after:
    page_link = decodeURI(page_link);
    let matches = page_link.match(/page\[number\]=\d+/gm);
    let next_page_number: any = null;
    matches = matches![0].match(/\d+/gm);
    next_page_number = parseInt(matches![0]);
    next_page_number++;
    let next_page_param = 'page[number]=' + next_page_number;
    page_link = page_link.replace(/page\[number\]=\d+/gm, next_page_param);
    return page_link;
  }
}