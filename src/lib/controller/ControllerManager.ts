import { Kernel } from '../kernel/kernel';
import { Manager } from '../container/Manager';

declare var kernel: Kernel;
export class ControllerManager extends Manager{
    protected static getPath():string{
        return kernel.getConfig().controllers_path;
    }
}