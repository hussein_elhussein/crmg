import { ICommandConfig } from '../../interfaces/ICommandConfig';
import { inject, injectable } from 'inversify';
import { BaseCommand } from '../../BaseCommand';
import Axios from "axios";
import { Kernel } from '../../../kernel/kernel';
import { AccessToken } from './AccessToken';
import { writeFile } from "fs";
import { Logger } from 'winston';
declare var kernel: Kernel;

@injectable()
export class AuthCommand extends BaseCommand{
  constructor(@inject("LoggerService") protected loggerService: Logger){
    super()
  }
  getName(): string {
    return "auth";
  }

  init(): ICommandConfig {
    return {
      description: "authenticates and stores the token",
      allowUnknownOptions: true,
      passOptionsAsObject: true,
      usage: "",
      options: [
        {
          name: "user",
          type: "string",
          description: "The direction of the migration (up/down)",
          alias: "u",
        },
        {
          name: "password",
          type: "string",
          description: "The direction of the migration (up/down)",
          alias: "p",
        },
      ],
    };
  }

  async run(args:any|string[],options:any, called_command?:string): Promise<any> {
    const config = kernel.getConfig();
    if(!options.user){
      options.user = config.api_username;
      if (!options.user || !options.user.length) {
        throw Error('"user" is required');
      }
    }
    if(!options.password){
      options.password = config.api_password;
      if (!options.password || !options.password.length) {
        throw Error('"password" is required');
      }
    }
    const path = kernel.getConfig().import_api + "/login-token";
    const auth = "Basic " + Buffer.from(options.user + ":" + options.password).toString('base64');
    let err = null;
    const res = await Axios.get(path,{headers: {Authorization:auth}}).catch(e => err = e);
    if(err){
      throw err;
    }
    await this.writeToken(res.data);
    this.loggerService.info('Successfully generated token.');
  }

  private async writeToken(token: AccessToken):Promise<any>{
    return new Promise<any>((resolve, reject) => {
      const path = kernel.getConfig().temp_path + "/token.json";
      const str = JSON.stringify(token);
      writeFile(path,str,{},(err) => {
        if(err){
          reject(err);
        }
        resolve(true);
      });
    });
  }
}