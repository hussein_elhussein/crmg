import { ICommandConfig } from '../../interfaces/ICommandConfig';
import { inject, injectable } from 'inversify';
import { BaseCommand } from '../../BaseCommand';
import { Kernel } from '../../../kernel/kernel';
import { Logger } from 'winston';
import { ImporterManager } from '../../../database/importers/ImporterManager';
declare var kernel: Kernel;

@injectable()
export class ImportCommand extends BaseCommand{
  constructor(@inject("LoggerService") protected loggerService: Logger){
    super()
  }
  getName(): string {
    return "import";
  }

  init(): ICommandConfig {
    return {
      description: "Imports the data from the api",
      allowUnknownOptions: true,
      passOptionsAsObject: true,
      usage: "",
      options: [
        {
          name: "failed_only",
          type: "boolean",
          description: "import only failed items",
          alias: "f",
        },
        {
          name: "restart",
          type: "boolean",
          description: "whether to continue or restart importing",
          alias: "u",
        },
        {
          name: "exit_on_fail",
          type: "boolean",
          description: "whether to stop importing when an error occurs",
          alias: "p",
        },
      ],
    };
  }

  async run(args:any|string[],options:any, called_command?:string): Promise<any> {
    let restart = true;
    let exit = false;
    if (options.restart) {
      restart = options.restart;
    }
    if (options.exit) {
      exit = options.exit;
    }
    if (options.failed_only) {
      await ImporterManager.importFailed(exit);
      this.loggerService.info('Finished imported data.');
      return;
    }
    await ImporterManager.import(-1, restart, exit);
    this.loggerService.info('Finished imported data.');
  }
}