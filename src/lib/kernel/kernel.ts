import corsMiddleware from '../../middleware/cors';
import { formBodyParserMiddleware, jsonBodyParserMiddleware } from '../../middleware/bodyParser';
import morganMiddleware from '../../middleware/morgan';
import {ContainerLoader} from '../container/ContainerLoader';
import { Security } from '../security';
import { Router } from '../router';
import { HttpError } from '../../helpers/HttpError';
import errorMiddleware from '../../middleware/error';
import {Express, Request, Response } from 'express';
import * as App from 'express';
import { Container } from 'inversify';
import { AppConfig } from './AppConfig';
import { IAppConfig } from './interfaces/IAppConfig';
import { ExistingDataStrategy } from '../database/importers/BaseImporter';

declare var kernel: Kernel;
export class Kernel {
    protected app: Express;
    protected container: Container;
    protected config: IAppConfig;
    public async boot(env?:string):Promise<Express>{
        this.config = new AppConfig(env);
        const gb: any = global;
        gb.kernel = this;
        const app = App();
        app.use(corsMiddleware);
        app.use(formBodyParserMiddleware);
        app.use(jsonBodyParserMiddleware);
        app.use(morganMiddleware);

        /** Register services,repositories..etc: */
        const con = await ContainerLoader.register();

        /** Load Security middlewares */
        const security = new Security(app);
        await security.init();

        /** Load application routes */
        const router = new Router(con);
        const routes = await router.init().catch(console.log);
        if(routes){
            app.use(routes);
        }else{
            throw new Error("Failed to load routes");
        }

        /** Return 404 for requested resources not found */
        app.use((req:Request, res:Response) => {
            throw new HttpError(404);
        });
        /** Load error middleware */
        app.use(errorMiddleware);
        this.app = app;
        this.container = con;
        gb.kernel = this;
        return app;
    }
    public static getRoodDir():string{
        let path = __dirname.replace(/(?:\\|\/)src(?:\\|\/)lib(?:\\|\/)kernel/g,'');
        path = path.replace(/(?:\\|\/)built(?:\\|\/)lib(?:\\|\/)kernel/g,'');
        return path;
    }

    public getApp():Express{
        return this.app;
    }

    public getContainer():Container{
        return this.container;
    }

    public getConfig():IAppConfig{
        return this.config;
    }

    public getExistingDataStrategy():ExistingDataStrategy{
        return this.config.existing_data_strategy? this.config.existing_data_strategy: "skip";
    }

    public static getKernel():Kernel{
        return kernel;
    }
}