import { IAppConfig } from './interfaces/IAppConfig';
import { Kernel } from './kernel';
import { ISequelizeOptions } from './interfaces/ISequelizeOptions';
import { ExistingDataStrategy } from '../database/importers/BaseImporter';
export class AppConfig implements IAppConfig{
  // App config:
  env: string = '';
  log_successful_requests: boolean = false;
  app_host: string = "";
  app_port: number = 0;

  // JWT
  app_key: string = "";
  app_key_issuer: string = "";
  app_key_audience: string = "";
  app_token_expires_in: string = "";

  db: ISequelizeOptions = {};
  // App directories paths:
  models_path: string[] = [];
  seeders_path: string = '';
  seeders_compiled_path: string = '';
  migrations_path: string = '';
  migrations_compiled_path: string = '';
  migrate_from_ts: boolean = false;
  controllers_path: string = '';
  repositories_path: string = '';
  factories_path: string = '';
  services_path: string = '';
  resources_path: string = '';
  commands_path: string = '';
  temp_path: string = "";
  importers_path: string = '';

  // Importer config:
  import_api: string = '';
  import_enabled: boolean = false;
  existing_data_strategy: ExistingDataStrategy = "skip";
  api_username: string = "";
  api_password: string = "";
  importers_to_run: string[] = [];
  items_per_importer: number = 0;
  constructor(env?: string){
    this.load(env);
  }
  load(env?: string):IAppConfig{
    const env_dir = Kernel.getRoodDir() + "/env/";
    // Load the default environment variables:
    const defaultConf = require(env_dir + "default.config");
    // Load the environment from a file based on the key app_env from "default.config":
    const env_name = env? env: defaultConf.env;
    const overrides = require(env_dir + env_name + ".config");
    process.env.APP_ENV = env_name;
    const conf = Object.assign(defaultConf,overrides);
    const properties = Object.keys(this);
    for(let key of Object.keys(conf)){
      if(properties.includes(key)){
        (this as any)[key] = conf[key];
      }
    }
    return conf;
  }
}