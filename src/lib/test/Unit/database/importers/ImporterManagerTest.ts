import { BaseUnitTest } from '../../BaseUnitTest';
import { suite, test } from "@testdeck/mocha";
import { expect, } from "chai";
import { resolve } from "path";
import { spy, when,verify,anything } from "ts-mockito";
import { ImporterManager } from '../../../../database/importers/ImporterManager';
import { ModelAImporter } from '../../../assets/database/importers/ModelAImporter';
import { ModelARepository } from '../../../assets/repositories/ModelARepository';
import { ExistingDataStrategy } from '../../../../database/importers/BaseImporter';
import ModelB from '../../../assets/models/ModelB';
import ModelA from '../../../assets/models/ModelA';
import { IDeSerialized } from '../../../../serializer/interfaces/IDeSerialized';
import { IMultiPagination } from '../../../../repositories/interfaces/IPagination';
import { Serializer } from '../../../../serializer/Serializer';

@suite
export class ImporterManagerTest extends BaseUnitTest{
  async before(): Promise<any>{
    await this.init(false, true, true, 'lib_test');
  }

  @test
  async getToken(){
    const res = ImporterManager.getToken();
    expect(res).haveOwnProperty('access_token');
    expect(res).haveOwnProperty('expires_in');
    expect(res).haveOwnProperty('refresh_token');
    expect(res).haveOwnProperty('type');
  }

  getDataDump(path: string):any{
    path = resolve(__dirname, "../../../assets/database/importers/dump_response_" + path + ".json");
    return require(path);
  }

  @test
  async import() {
    const manager = ImporterManager;
    const modelAImporter = this.container.resolve(ModelAImporter);
    const repo = this.container.get(ModelARepository);
    let dump:IMultiPagination<any> = this.getDataDump('1');
    const model_b  = new ModelB();
    model_b.address_a = "my address";
    const mbi = await model_b.save();
    let enabled = false;
    let strategy:ExistingDataStrategy = "skip";
    let dataReturned = false;

    const spied_manager = spy(manager);
    const spied_importer = spy(modelAImporter);
    when(spied_manager.importEnabled()).thenCall(() => {
      return enabled;
    });
    when(spied_manager.getImporters()).thenCall(() => {
      modelAImporter.model = ModelA;
      return [modelAImporter];
    });

    when((spied_manager as any).getData(anything(), anything())).thenCall(async (path: string, headers: any) => {
      if(dataReturned){
        let dump_path:any = path.split('/');
        dump_path = dump_path[dump_path.length -1];
        dump = this.getDataDump(dump_path);
      }
      for(let item of dump.data){
        item.attributes.model_b_id = mbi.id;
      }
      dataReturned = true;
      return dump;
    });
    when(spied_importer.getExistingDataStrategy()).thenCall(() => {
      return strategy;
    });
    // assert that importing doesn't run when it's disabled:
    let err = null;
    await manager.import().catch(e => err = e);
    expect(err).be.instanceOf(Error);
    err = null;
    // assert that importing runs normally when it's enabled:
    enabled = true;
    await manager.import().catch(e => err = e);
    expect(err).to.be.null;
    verify(spied_importer.validate(anything())).times(3);
    verify(spied_importer.getMapping()).atLeast(1);
    verify(spied_importer.save(anything(),anything(), anything())).times(2);
    err = null;
    const instances = await repo.findAll().catch(e => err = e);
    expect(instances).to.length(2);

    // assert that duplicate date doesn't get saved:
    dataReturned = false;
    await manager.import().catch(e => err = e);
    const instancesT = await repo.findAll().catch(e => err = e);
    expect(err).to.be.null;
    expect(instancesT).to.length(2);

    // assert that unique data get updated:
    strategy = "update";
    dump = this.getDataDump('1');
    dump.data[0].attributes.first_name = "Danny";
    dataReturned = false;
    await manager.import().catch(e => err = e);
    expect(err).to.be.null;
    const data = dump.data[0];
    const serializer = this.container.get(Serializer);
    const deSerialized:IDeSerialized = serializer.deSerialize(data);
    const updatedInstance = <ModelA> await modelAImporter.findExisting(deSerialized, modelAImporter, ModelA).catch(e => err = e);
    expect(err).to.be.null;
    expect(updatedInstance).to.not.be.null;
    expect(updatedInstance?.get('firstName')).to.equal("Danny");
  }

}