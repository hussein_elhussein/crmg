import { Op } from 'Sequelize';
import { FindOptions } from 'sequelize';
import { BaseUnitTest } from '../../BaseUnitTest';
import { suite, test } from "@testdeck/mocha";
import { expect, } from "chai";
import { spy, when,verify,anything } from "ts-mockito";
import { ModelAImporter } from '../../../assets/database/importers/ModelAImporter';

@suite
export class ImporterManagerTest extends BaseUnitTest{
  async before(): Promise<any>{
    await this.init(false, false, false, 'lib_test');
  }

  @test
  getValues() {
    const modelAImporter = this.container.resolve(ModelAImporter);
    const spied_improter = spy(modelAImporter);
    const mapping = {
      firstName: "a.b.name",
      lastName: 'last_name',
    };
    const data = {
      a: {
        b:{
          name: "John",
        }
      },
      last_name: "Doe",
    };
    when(spied_improter.getMapping()).thenReturn(mapping);
    const result = modelAImporter.getValues(data);
    expect(result).to.haveOwnProperty('firstName');
    expect(result).to.haveOwnProperty('lastName');
    expect(result.firstName).to.equal('John');
    expect(result.lastName).to.equal('Doe');
  }

  @test
  loadQueryOptions(){
    const modelAImporter = this.container.resolve(ModelAImporter);
    const spied_improter = spy(modelAImporter);
    const data = {
      label: "John Doe",
    };
    const fields = ['firstName', 'lastName'];
    when(spied_improter.getAdditionalIds()).thenReturn(fields);
    const result = (modelAImporter as any).loadQueryOptions(data);
    expect(result).to.haveOwnProperty('firstName');
    expect(result).to.haveOwnProperty('lastName');
    expect(result.firstName).to.equal('John');
    expect(result.lastName).to.equal('Doe');
  }

}