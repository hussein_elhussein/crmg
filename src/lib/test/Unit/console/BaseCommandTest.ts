import { BaseUnitTest } from '../BaseUnitTest';
import { resolve } from "path";
import { Kernel } from '../../../kernel/kernel';
import { ForkOptions } from "child_process";
import * as cp from "child_process";
import * as commander from "./../../../console/utils/commander"

export class BaseCommandTest extends BaseUnitTest{
  async before(){
    await this.init(false, false);
  }

  /**
   * Calls a console command.
   * @param args
   */
  callCommand(args: string[]): Promise<any>{
    return new Promise(async (res, reject) => {
      const path = resolve(Kernel.getRoodDir(), './src/lib/console/index.ts');
      const options: ForkOptions = {
        silent: false,
        execArgv: ['-r', 'ts-node/register','--inspect'],
        env: process.env,
      };

      const child = cp.fork(path, args, options);
      let result:any = null;
      child.on('exit', (code: any, signal: any) => {
        res(result);
      });
      child.once('message', (message:any, sendHandle) => {
        const isBoolean = typeof message === 'boolean';
        if(isBoolean && message){
          res(message);
        }
        else {
          reject(message);
        }
      });

    });
  }

  initCommander():commander.Command{
    const command = new commander.Command();
    command.version('0.0.1');
    return command;
  }
}