import { BaseUnitTest } from '../BaseUnitTest';
import { suite,test } from "@testdeck/mocha";
import { expect } from "chai";
import { DatabaseService } from '../../../services/DatabaseService';
import seeder from '../../../database/listener/SeedListener';
import { RepositoryManager } from '../../../repositories/RepositoryManager';
import ModelC from '../../assets/models/ModelC';
import { BaseRepository } from '../../../repositories/BaseRepository';

@suite
export class RepositoryManagerTest  extends BaseUnitTest{
  async before(): Promise<any>{
    await this.init(false, true, true, "lib_test");
  }

  @test
  async findRepository(){
    await this.seed();
    // assert getting the repository passing model object:
    let repo;
    repo = RepositoryManager.findRepository(ModelC);
    expect(repo).to.be.instanceOf(BaseRepository);
    // assert getting the repository passing model name:
    repo = RepositoryManager.findRepository("ModelC");
    expect(repo).to.be.instanceOf(BaseRepository);

    // assert that running methods on the repository works:
    const result = await repo.findAll();
    expect(result).to.have.lengthOf(1);
    const modelC = result[0];
    expect(modelC).to.be.instanceOf(ModelC);
  }
}