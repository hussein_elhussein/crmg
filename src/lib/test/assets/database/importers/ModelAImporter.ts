import { BaseImporter } from '../../../../database/importers/BaseImporter';
import { injectable } from 'inversify';
import ModelA from '../../models/ModelA';
import { IDeSerialized } from '../../../../serializer/interfaces/IDeSerialized';

@injectable()
export class ModelAImporter extends BaseImporter<ModelA>{
  getOrder(): number {
    return 1;
  }

  protected getRequiredProperties(): string[] {
    return ['modelBId'];
  }
  getPath(): string {
    return '/model_a';
  }

  getMapping(): any{
    return {
      firstName: "first_name",
      lastName: "last_name",
      modelBId: "model_b_id",
    };
  }

}