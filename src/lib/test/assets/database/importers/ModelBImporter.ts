import { BaseImporter } from '../../../../database/importers/BaseImporter';
import { injectable } from 'inversify';
import ModelB from '../../models/ModelB';

@injectable()
export class ModelBImporter extends BaseImporter<ModelB>{
  getOrder(): number {
    return 2;
  }

  protected getRequiredProperties(): string[] {
    return ['address_a'];
  }
  getExistingDataStrategy(): 'skip' | 'update' {
    return 'update';
  }

  getPath(): string {
    return '/model_b';
  }

  getMapping(): any{
    return {
      address_a: "address_a",
      address_b: "address_b"
    };
  }

}