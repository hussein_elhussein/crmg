import { BaseCommand } from '../../../console/BaseCommand';
import { ICommandConfig } from '../../../console/interfaces/ICommandConfig';
import { injectable } from 'inversify';

@injectable()
export class TestCommand extends BaseCommand{
  getName(): string {
    return 'my_command';
  }

  init(): ICommandConfig {
    return {
      description: "A command for testing",
      usage: "For testing",
      options: [
        {
          name: "option_a",
          type: "string",
          description: "Option A description",
          alias: "a",
        }
      ]
    }
  }

  async run(args:any|string[],options:any|string[], called_command?:string): Promise<any> {
    console.log('this is a test command');
    const stop = null;
  }

}