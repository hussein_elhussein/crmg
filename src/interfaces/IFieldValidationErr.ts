import { IHttpError } from './IHttpError';

export interface IFieldValidationErr extends IHttpError{
    field: string;
}