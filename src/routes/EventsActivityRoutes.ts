import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { EventsActivityController } from '../controllers/EventsActivityController';
@Router.route({
    controller: EventsActivityController,
    resource: "/api/events-activities",
    middleWares: [ApiMiddleware]
})
export class EventsActivityRoutes extends BaseRouter{};