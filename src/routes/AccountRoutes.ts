import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { AccountController } from '../controllers/AccountController';
@Router.route({
    controller: AccountController,
    resource: "/api/accounts",
    middleWares: [ApiMiddleware]
})
export class AccountRoutes extends BaseRouter{}
