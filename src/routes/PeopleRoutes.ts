import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { PersonController } from '../controllers/PersonController';
@Router.route({
  controller: PersonController,
  resource: '/api/people',
  middleWares: [ApiMiddleware],
})
export class PeopleRoutes extends BaseRouter{}
