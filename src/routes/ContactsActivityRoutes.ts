import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { ContactsActivityController } from '../controllers/ContactsActivityController';
@Router.route({
    controller: ContactsActivityController,
    resource: "/api/contacts-activities",
    middleWares: [ApiMiddleware]
})
export class ContactsActivityRoutes extends BaseRouter{};