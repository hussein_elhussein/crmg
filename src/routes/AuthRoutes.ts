import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { AuthController } from '../controllers/AuthController';
@Router.route({
    controller: AuthController,
    groups: [
        {
            base: '/',
            routes: [
                {
                    path: "/login",
                    method: "post",
                    handler: "login",
                },
                {
                    path: "/register",
                    method: "post",
                    handler: "register",
                }
            ]
        }
    ],
})
export class AuthRoutes extends BaseRouter{}
