import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { AddressController } from '../controllers/AddressController';

@Router.route({
  controller: AddressController,
  resource: '/api/addresses',
  middleWares: [ApiMiddleware],
})
export class AddressRoutes extends BaseRouter{}
