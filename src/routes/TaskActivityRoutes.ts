import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { TaskActivityController } from '../controllers/TaskActivityController';
@Router.route({
    controller: TaskActivityController,
    resource: "/api/task-activities",
    middleWares: [ApiMiddleware]
})
export class TaskActivityRoutes extends BaseRouter{};