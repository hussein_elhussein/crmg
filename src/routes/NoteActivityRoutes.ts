import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { NoteActivityController } from '../controllers/NoteActivityController';
@Router.route({
    controller: NoteActivityController,
    resource: "/api/note-activities",
    middleWares: [ApiMiddleware]
})
export class NoteActivityRoutes extends BaseRouter{};