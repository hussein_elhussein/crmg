import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { DeviceActivityController } from '../controllers/DeviceActivityController';
@Router.route({
    controller: DeviceActivityController,
    resource: "/api/device-activities",
    middleWares: [ApiMiddleware]
})
export class DeviceActivityRoutes extends BaseRouter{};