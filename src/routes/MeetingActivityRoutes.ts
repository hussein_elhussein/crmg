import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { MeetingActivityController } from '../controllers/MeetingActivityController';
@Router.route({
    controller: MeetingActivityController,
    resource: "/api/meeting-activities",
    middleWares: [ApiMiddleware]
})
export class MeetingActivityRoutes extends BaseRouter{};