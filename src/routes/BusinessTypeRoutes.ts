import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import {BusinessTypeController} from "../controllers/BusinessTypeController"
@Router.route({
    controller: BusinessTypeController,
    resource: "/api/business-types",
    middleWares: [ApiMiddleware]
})
export class BusinessTypeRoutes extends BaseRouter{}
