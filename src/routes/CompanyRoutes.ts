import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { CompanyController } from '../controllers/CompanyController';
@Router.route({
    controller: CompanyController,
    resource: "/api/companies",
    middleWares: [ApiMiddleware]
})
export class CompanyRoutes extends BaseRouter{}
