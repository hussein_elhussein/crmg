import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { AchReportsActivityController } from '../controllers/AchReportsActivityController';
@Router.route({
    controller: AchReportsActivityController,
    resource: "/api/ach-reports-activities",
    middleWares: [ApiMiddleware]
})
export class AchReportsActivityRoutes extends BaseRouter{};