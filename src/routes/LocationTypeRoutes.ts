import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { LocationTypeController } from '../controllers/LocationTypeController';
@Router.route({
  controller: LocationTypeController,
  resource: "/api/location-types",
  middleWares: [ApiMiddleware],
})
export class LocationTypeRoutes extends BaseRouter{}
