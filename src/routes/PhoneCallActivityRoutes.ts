import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { PhoneCallActivityController } from '../controllers/PhoneCallActivityController';
@Router.route({
    controller: PhoneCallActivityController,
    resource: "/api/phone-call-activities",
    middleWares: [ApiMiddleware]
})
export class PhoneCallActivityRoutes extends BaseRouter{};