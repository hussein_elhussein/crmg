import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { EmailActivityController } from '../controllers/EmailActivityController';
@Router.route({
    controller: EmailActivityController,
    resource: "/api/email-activities",
    middleWares: [ApiMiddleware]
})
export class EmailActivityRoutes extends BaseRouter{};