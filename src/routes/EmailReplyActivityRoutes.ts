import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { EmailReplyActivityController } from '../controllers/EmailReplyActivityController';
@Router.route({
    controller: EmailReplyActivityController,
    resource: "/api/email-reply-activities",
    middleWares: [ApiMiddleware]
})
export class EmailReplyActivityRoutes extends BaseRouter{};