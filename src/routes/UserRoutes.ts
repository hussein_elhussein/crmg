import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { UserController } from '../controllers/UserController';
@Router.route({
    controller: UserController,
    resource: "/api/users",
    middleWares: [ApiMiddleware]
})
export class AccountRoutes extends BaseRouter{}
