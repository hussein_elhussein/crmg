import { Router } from '../lib/router';
import { BaseRouter } from '../lib/router/BaseRouter';
import { ApiMiddleware } from '../lib/security/middlewares/ApiMiddleware';
import { RpmActivityController } from '../controllers/RpmActivityController';
@Router.route({
    controller: RpmActivityController,
    resource: "/api/rpm-activities",
    middleWares: [ApiMiddleware]
})
export class RpmActivityRoutes extends BaseRouter{};